package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.ChangesetMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.IssueToChangesetMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryToChangesetMapping;
import com.atlassian.jira.plugins.dvcs.dao.impl.ChangesetDaoImpl;
import com.atlassian.jira.plugins.dvcs.dao.impl.DAOConstants;
import com.atlassian.jira.plugins.dvcs.dao.impl.QueryDslFeatureHelper;
import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessor;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessorUtils.dropTable;
import static com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessorUtils.setUpDatabaseUsingSQLFile;
import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ChangesetDaoQueryDslByIssueKeyFoldingTest {
    private static final String ISSUE_1 = "TEST-1";
    private static final String ISSUE_2 = "TEST-2";
    private static final String ISSUE_3 = "TEST-3";
    private static final List<String> issueKeys = ImmutableList.of("TST-1", "TST-2", "TST-3", "TST-4", ISSUE_1, ISSUE_2, ISSUE_3);
    private static final String AO_TABLE_NAME_PREFIX = "AO_E8B6CC";
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private ChangesetDaoImpl changesetDao;
    @Mock
    private QueryDslFeatureHelper queryDslFeatureHelper;
    private ChangesetDaoQueryDsl classUnderTest;
    private StandaloneDatabaseAccessor databaseAccessor;

    @Before
    public void setup() throws Exception {
        try (InputStream inputStream = MessageQueueItemDaoQueryDslTest.class.getResourceAsStream("/com.atlassian.jira.plugins.dvcs.dao.impl.querydsl/ChangesetDaoQueryDslByIssueKeyFoldingTest.sql")) {
            databaseAccessor = setUpDatabaseUsingSQLFile(AO_TABLE_NAME_PREFIX, inputStream,
                    ChangesetMapping.class,
                    OrganizationMapping.class,
                    IssueToChangesetMapping.class,
                    RepositoryToChangesetMapping.class,
                    RepositoryMapping.class);
        }

        classUnderTest = new ChangesetDaoQueryDsl(databaseAccessor, changesetDao, queryDslFeatureHelper);
    }

    @After
    public void cleanUp() {
        dropTable("AO_E8B6CC_CHANGESET_MAPPING", databaseAccessor);
        dropTable("AO_E8B6CC_ORGANIZATION_MAPPING", databaseAccessor);
        dropTable("AO_E8B6CC_ISSUE_TO_CHANGESET", databaseAccessor);
        dropTable("AO_E8B6CC_REPOSITORY_MAPPING", databaseAccessor);
        dropTable("AO_E8B6CC_REPO_TO_CHANGESET", databaseAccessor);
    }

    @Test
    public void issueOneFindsExpectedResults() {
        List<Changeset> results = classUnderTest.getByIssueKey(ImmutableList.of(ISSUE_1), "bitbucket", false);

        assertThat(results.size(), is(85));
        results.forEach(changeset -> assertThat(changeset.getIssueKeys().contains(ISSUE_1), is(true)));
    }

    @Test
    public void resultsAreLimitedToMax() {
        List<Changeset> results = classUnderTest.getByIssueKey(issueKeys, "", false);

        assertThat(results.size(), is(DAOConstants.MAXIMUM_ENTITIES_PER_ISSUE_KEY));
        Set<String> issueKeySet = new HashSet<>(issueKeys);

        results.forEach(changeset -> changeset.getIssueKeys().forEach(issueKey ->
                assertThat(issueKeySet.contains(issueKey), is(true))));
    }

    @Test
    public void testOrgTypeFilterWorks() {
        List<Changeset> results = classUnderTest.getByIssueKey(issueKeys, "github", false);

        assertThat(results.size(), is(0));
    }


    @Test
    public void testEmptyQueryOldestFirst() {
        List<Changeset> results = classUnderTest.getByIssueKey(ImmutableList.of("NONISSUE-1"), "github", false);

        assertThat(results, is(emptyList()));
    }

    @Test
    public void testEmptyQueryNewestFirst() {
        List<Changeset> results = classUnderTest.getByIssueKey(ImmutableList.of("NONISSUE-1"), "github", true);

        assertThat(results, is(emptyList()));
    }
}
