package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.ChangesetMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryMapping;
import com.atlassian.jira.plugins.dvcs.activity.PullRequestParticipantMapping;
import com.atlassian.jira.plugins.dvcs.activity.RepositoryPullRequestIssueKeyMapping;
import com.atlassian.jira.plugins.dvcs.activity.RepositoryPullRequestMapping;
import com.atlassian.jira.plugins.dvcs.dao.impl.DAOConstants;
import com.atlassian.jira.plugins.dvcs.model.PullRequest;
import com.atlassian.jira.plugins.dvcs.model.PullRequestStatus;
import com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessor;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.io.InputStream;
import java.util.List;

import static com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessorUtils.dropTable;
import static com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessorUtils.setUpDatabaseUsingSQLFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class PullRequestDaoQueryDslByIssueKeyFoldingTest {
    private static final String AO_TABLE_NAME_PREFIX = "AO_E8B6CC";
    private static final Integer PR_MAPPING_ID = 2;
    private static final String AUTHOR_NAME = "GTTaylor";
    private static final String ISSUE_1 = "TEST-1";
    private static final String ISSUE_2 = "TEST-2";
    private static final String ISSUE_3 = "TEST-3";

    private PullRequestDaoQueryDsl classUnderTest;
    private StandaloneDatabaseAccessor databaseAccessor;


    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        try (InputStream inputStream = PullRequestDaoQueryDslByIssueKeyFoldingTest.class.getResourceAsStream(
                "/com.atlassian.jira.plugins.dvcs.dao.impl.querydsl/PullRequestDaoQueryDslByIssueKeyFoldingTest.sql")) {
            databaseAccessor = setUpDatabaseUsingSQLFile(AO_TABLE_NAME_PREFIX, inputStream,
                    ChangesetMapping.class,
                    OrganizationMapping.class,
                    RepositoryMapping.class,
                    PullRequestParticipantMapping.class,
                    RepositoryPullRequestMapping.class,
                    RepositoryPullRequestIssueKeyMapping.class);
        }

        classUnderTest = new PullRequestDaoQueryDsl(databaseAccessor);
    }

    @After
    public void cleanUp() {
        dropTable("AO_E8B6CC_CHANGESET_MAPPING", databaseAccessor);
        dropTable("AO_E8B6CC_ORGANIZATION_MAPPING", databaseAccessor);
        dropTable("AO_E8B6CC_REPOSITORY_MAPPING", databaseAccessor);
        dropTable("AO_E8B6CC_PR_PARTICIPANT", databaseAccessor);
        dropTable("AO_E8B6CC_PULL_REQUEST", databaseAccessor);
        dropTable("AO_E8B6CC_PR_ISSUE_KEY", databaseAccessor);
    }

    @Test
    public void getSinglePR() {
        List<PullRequest> results = classUnderTest.getByIssueKeys(ImmutableList.of(ISSUE_1), "");
        assertThat(results.size(), is(1));
        PullRequest result = results.get(0);

        assertThat(result.getId(), is(PR_MAPPING_ID));
        assertThat(result.getIssueKeys().get(0), is(ISSUE_1));
        assertThat(result.getRemoteId(), is(1L));
        assertThat(result.getRepositoryId(), is(7));
        assertThat(result.getAuthor(), is(AUTHOR_NAME));
        assertThat(result.getParticipants().size(), is(1));
        assertThat(result.getStatus(), is(PullRequestStatus.OPEN));
    }

    @Test
    public void testReturnStopsAtLimit() {
        List<PullRequest> results = classUnderTest.getByIssueKeys(ImmutableList.of(ISSUE_2), "");

        assertThat(results.size(), is(DAOConstants.MAXIMUM_ENTITIES_PER_ISSUE_KEY));
        results.stream().forEach(pr -> assertThat(pr.getIssueKeys(), is(ImmutableList.of(ISSUE_2))));
    }

    @Test
    public void testDoesNotUpdateOnDuplicateParticipant() {
        List<PullRequest> results = classUnderTest.getByIssueKeys(ImmutableList.of(ISSUE_3), "bitbucket");

        assertThat(results.size(), is(1));
        PullRequest result = results.get(0);

        assertThat(result.getId(), is(PR_MAPPING_ID));
        assertThat(result.getRemoteId(), is(1L));
        assertThat(result.getRepositoryId(), is(7));
        assertThat(result.getAuthor(), is(AUTHOR_NAME));
        assertThat(result.getParticipants().size(), is(1));
        assertThat(result.getStatus(), is(PullRequestStatus.OPEN));

        results.stream().forEach(pr -> assertThat(pr.getIssueKeys(), is(ImmutableList.of(ISSUE_3))));
    }
}
