package com.atlassian.jira.plugins.dvcs.dao.impl.common;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageQueueItemMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageTagMapping;
import com.atlassian.jira.plugins.dvcs.dao.impl.querydsl.MockPayload;
import com.atlassian.jira.plugins.dvcs.model.Message;
import com.atlassian.jira.plugins.dvcs.model.MessageState;
import com.atlassian.jira.plugins.dvcs.service.message.HasProgress;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddress;
import net.java.ao.DBParam;
import net.java.ao.EntityManager;
import net.java.ao.test.ActiveObjectsIntegrationTest;
import org.junit.Before;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.beans.PropertyChangeListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Base class that does the data setup for the Message related DB tests
 */
public class MessageDaoBaseTest extends ActiveObjectsIntegrationTest {

    protected static final List<Integer> idNums = new ArrayList<>();
    protected static final MessageAddress address = new MockAddress();
    protected static final int ID = 1;
    protected static final int NUMBER_OF_SEEDED_MESSAGES = 2000;
    protected static final double STATES_WE_CARE_ABOUT_RATIO = 3 / 5.0;
    protected static final double TAG_RATIO = 1 / 2.0;
    protected static final String TAG_ONE = "tag";
    protected static final String TAG_TWO = "tag2";
    protected static final List<Message> messages_populated_in_DB = populateEntitityList();
    protected static final String QUEUE = "TestQueue";
    protected static final String STATE_INFO = "STATE INFO";

    protected static final Date NOW = new Date();

    protected static List<Message> populateEntitityList() {
        List<Message> events = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_SEEDED_MESSAGES; i++) {
            Message message = new Message<>();
            message.setId(i);
            message.setAddress(new MockAddress());
            message.setPayload(MockPayload.class.getCanonicalName());
            message.setPayloadType(MockPayload.class);
            message.setPriority(0);
            message.setRetriesCount(0);
            message.setTags(new String[]{i % 2 == 0 ? TAG_ONE : TAG_TWO});
            events.add(message);
        }
        return events;
    }

    protected static String getState(int id) {
        return MessageState.values()[id % MessageState.values().length].name();
    }

    protected <P extends HasProgress> Map<String, Object> toMessageMap(Message<P> source) {
        final Map<String, Object> result = new HashMap<>();

        result.put(MessageMapping.ADDRESS, source.getAddress().getId());
        result.put(MessageMapping.PRIORITY, source.getPriority());
        result.put(MessageMapping.PAYLOAD_TYPE, source.getPayloadType().getCanonicalName());
        result.put(MessageMapping.PAYLOAD, source.getPayload());

        return result;
    }

    @Before
    public void setupData(EntityManager entityManager) throws Exception {
        entityManager.migrateDestructively();
        entityManager.migrate(MessageTagMapping.class, MessageMapping.class, MessageQueueItemMapping.class);

        entityManager.deleteWithSQL(MessageTagMapping.class, "1=1");
        entityManager.deleteWithSQL(MessageQueueItemMapping.class, "1=1");
        entityManager.deleteWithSQL(MessageMapping.class, "1=1");

        if (entityManager.find(MessageMapping.class).length != messages_populated_in_DB.size()) {
            messages_populated_in_DB.stream().forEach(message -> {
                try {
                    MessageMapping mp = entityManager.create(MessageMapping.class,
                            new DBParam(MessageMapping.ADDRESS, message.getAddress().getId()),
                            new DBParam(MessageMapping.PAYLOAD, message.getPayload()),
                            new DBParam(MessageMapping.PAYLOAD_TYPE, message.getPayloadType().getCanonicalName()),
                            new DBParam(MessageMapping.PRIORITY, message.getPriority()));
                    idNums.add(mp.getID());
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });
        }

        if (entityManager.find(MessageTagMapping.class).length != messages_populated_in_DB.size()) {
            idNums.stream().forEach(id -> {
                try {
                    entityManager.create(MessageTagMapping.class,
                            new DBParam(MessageTagMapping.MESSAGE, id),
                            new DBParam(MessageTagMapping.TAG, id % 2 == 0 ? TAG_ONE : TAG_TWO)
                    );
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });
        }

        if (entityManager.find(MessageQueueItemMapping.class).length != messages_populated_in_DB.size()) {
            idNums.stream().forEach(id -> {

                try {
                    entityManager.create(MessageQueueItemMapping.class,
                            new DBParam(MessageQueueItemMapping.MESSAGE, id),
                            new DBParam(MessageQueueItemMapping.QUEUE, QUEUE),
                            new DBParam(MessageQueueItemMapping.STATE, getState(id)),
                            new DBParam(MessageQueueItemMapping.LAST_FAILED, NOW),
                            new DBParam(MessageQueueItemMapping.RETRIES_COUNT, 0),
                            new DBParam(MessageQueueItemMapping.STATE_INFO, STATE_INFO)
                    );
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }

    protected static class MockAddress implements MessageAddress<MockPayload> {
        @Override
        public String getId() {
            return "test-id";
        }

        @Override
        public Class<MockPayload> getPayloadType() {
            return MockPayload.class;
        }

        @Override
        public boolean equals(Object o) {
            return o.getClass().equals(this.getClass());
        }
    }

    protected class TestTagMapping implements MessageTagMapping {

        private String tag;
        private int id;

        public TestTagMapping(String tag, int id) {
            this.tag = tag;
            this.id = id;
        }

        @Override
        public String getTag() {
            return tag;
        }

        @Override
        public int getID() {
            return 1;
        }

        @Override
        public MessageMapping getMessage() {
            throw new NotImplementedException();
        }

        @Override
        public void init() {
            throw new NotImplementedException();
        }

        @Override
        public void save() {
            throw new NotImplementedException();
        }

        @Override
        public EntityManager getEntityManager() {
            throw new NotImplementedException();
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            throw new NotImplementedException();
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            throw new NotImplementedException();
        }

        @Override
        public Class getEntityType() {
            throw new NotImplementedException();
        }
    }
}
