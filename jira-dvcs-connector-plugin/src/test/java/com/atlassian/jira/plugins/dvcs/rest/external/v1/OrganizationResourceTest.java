package com.atlassian.jira.plugins.dvcs.rest.external.v1;

import com.atlassian.jira.exception.NotFoundException;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService;
import com.atlassian.jira.plugins.dvcs.model.Group;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.service.RepositoryService;
import com.atlassian.jira.plugins.dvcs.util.JsonResourceLoader;
import com.atlassian.jira.plugins.dvcs.util.TestRestResourceInstance;
import com.atlassian.jira.plugins.dvcs.util.UnitTestRestServerRule;
import com.atlassian.jira.util.I18nHelper;
import com.beust.jcommander.internal.Lists;
import com.beust.jcommander.internal.Sets;
import com.google.common.collect.ImmutableList;
import com.google.gson.JsonObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.skyscreamer.jsonassert.JSONCompareMode;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService.OrganizationApprovalFailedReason.ORGANIZATION_ALREADY_REMOVED;
import static com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService.OrganizationApprovalFailedReason.UNKNOWN_ERROR;
import static com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory.create2LOCredential;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.isEmptyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class OrganizationResourceTest {
    @Rule
    public UnitTestRestServerRule serverRule = new UnitTestRestServerRule(this);

    @Mock
    I18nHelper i18nHelper;
    @Mock
    AnalyticsService analyticsService;
    @Mock
    OrganizationService organizationService;
    @Mock
    RepositoryService repositoryService;

    @TestRestResourceInstance
    @InjectMocks
    OrganizationResource resource;

    @Test
    public void approveOrganization_returnsNoContent_forExistingOrg() {
        // setup
        final Organization org = createSampleOrganization();
        final int orgId = org.getId();
        Mockito.when(organizationService.changeOrganizationApprovalState(orgId, Organization.ApprovalState.APPROVED))
                .thenReturn(org);

        // test
        given()
                .contentType("application/json")
                .pathParam("id", orgId)
                .queryParam("approvalLocation", "ON_ADMIN_SCREEN")
                .post("/organization/{id}/approve")
                .then()
                // check
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        verify(analyticsService).publishOrganizationApproved(
                org, AnalyticsService.OrganizationApprovalLocation.ON_ADMIN_SCREEN);
    }

    @Test
    public void approveOrganization_returnsNotFound_whenOrgIdDoesNotExist() {
        // setup
        final int orgId = 3;
        Mockito.when(organizationService.changeOrganizationApprovalState(orgId, Organization.ApprovalState.APPROVED))
                .thenThrow(new NotFoundException());

        // test
        given()
                .contentType("application/json")
                .pathParam("id", orgId)
                .queryParam("approvalLocation", "DURING_INSTALLATION_FLOW")
                .post("/organization/{id}/approve")
                .then()
                // check
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());

        verify(analyticsService).publishOrganizationApprovalFailed(
                orgId,
                AnalyticsService.OrganizationApprovalLocation.DURING_INSTALLATION_FLOW,
                ORGANIZATION_ALREADY_REMOVED);
    }

    @Test
    public void approveOrganization_returnsNotFound_and_usesUnknownApprovalLocation_when_approvalLocationNotProvided() {
        // setup
        final int orgId = 3;
        Mockito.when(organizationService.changeOrganizationApprovalState(orgId, Organization.ApprovalState.APPROVED))
                .thenThrow(new NotFoundException());

        // test
        given()
                .contentType("application/json")
                .pathParam("id", orgId)
                .post("/organization/{id}/approve")
                .then()
                // check
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());

        verify(analyticsService).publishOrganizationApprovalFailed(
                orgId,
                AnalyticsService.OrganizationApprovalLocation.UNKNOWN,
                ORGANIZATION_ALREADY_REMOVED);
    }

    @Test
    public void approveOrganization_returnsInternalServerError_whenOrgApprovalFails() {
        // setup
        final int orgId = 3;
        Mockito.when(organizationService.changeOrganizationApprovalState(orgId, Organization.ApprovalState.APPROVED))
                .thenThrow(new RuntimeException("some runtime exception"));

        // test
        given()
                .contentType("application/json")
                .pathParam("id", orgId)
                .queryParam("approvalLocation", "DURING_INSTALLATION_FLOW")
                .post("/organization/{id}/approve")
                .then()
                // check
                .statusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());

        verify(analyticsService).publishOrganizationApprovalFailed(
                orgId,
                AnalyticsService.OrganizationApprovalLocation.DURING_INSTALLATION_FLOW,
                UNKNOWN_ERROR);
    }

    @Test
    public void syncRepoList() {
        // setup
        final Organization org = createSampleOrganization();
        final int orgId = org.getId();
        Mockito.when(organizationService.get(orgId, false)).thenReturn(org);

        // test
        given()
                .contentType("application/json")
                .pathParam("id", orgId)
                .get("/organization/{id}/syncRepoList")
                .then()
                // check
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        verify(organizationService).get(orgId, false);
        verify(repositoryService).syncRepositoryList(org);
    }

    @Test
    public void findOrganization_findsOrganizationCaseSensitivelyWhenItExists() throws Exception {
        // setup
        final String name = "NAME1";
        final String type = "orgType";
        final Organization org1 = createSampleOrganization();
        org1.setId(1);
        org1.setName(name.toLowerCase());
        final Organization org2 = createSampleOrganization();
        org2.setId(2);
        org2.setName(name);
        when(organizationService.getAll(false, type)).thenReturn(ImmutableList.of(org1, org2));
        final JsonObject expected = JsonResourceLoader.getTestJson("SampleOrganization.json", this);
        expected.getAsJsonObject("organization").addProperty("id", 2);

        // test
        final String actualJson = given()
                .contentType("application/json")
                .queryParam("name", "NAME1")
                .queryParam("type", type)
                .get("/organization/find")
                .then()
                // check
                .statusCode(Response.Status.OK.getStatusCode())
                .extract().response().asString();

        assertEquals(expected.toString(), actualJson, JSONCompareMode.NON_EXTENSIBLE);
    }

    @Test
    public void findOrganization_shouldNotFindOrganizationWhenItDoesNotExist() {
        // setup
        final String name = "NAME1";
        final String type = "orgType";
        final Organization org1 = createSampleOrganization();
        org1.setId(1);
        org1.setName(name.toLowerCase()); // Assigning lowercase name and will use UPPER CASE name for the lookup
        when(organizationService.getAll(false, type)).thenReturn(ImmutableList.of(org1));

        // test
        given()
                .contentType("application/json")
                .queryParam("name", "NAME1")
                .queryParam("type", type)
                .get("/organization/find")
                .then()
                // check
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .body(isEmptyString());
    }

    /**
     * Creates an Organization instance with no repositories and with all other fields populated
     */
    private Organization createSampleOrganization() {
        final Organization organization = new Organization();
        organization.setId(6);
        organization.setDvcsType("bitbucket");
        organization.setHostUrl("https://bitbucket.org");
        organization.setOrganizationUrl("https://bitbucket.org/x/orgname");
        organization.setName("doesnotmatter");
        organization.setApprovalState(Organization.ApprovalState.APPROVED);
        organization.setAutolinkNewRepos(true);
        organization.setSmartcommitsOnNewRepos(true);

        // Adding Credential, Groups and DefaultGroups on purpose as they are transient
        // and should not appear in REST call JSON response while fetching Orgs
        organization.setCredential(create2LOCredential("doesnotmatter_u", "doesnotmatter_p"));

        Set<Group> defaultGroups = Sets.newHashSet();
        Group dg1 = new Group("slugText1", "niceNameText1");
        Group dg2 = new Group("slugText2", "niceNameText2");
        defaultGroups.add(dg1);
        defaultGroups.add(dg2);
        organization.setDefaultGroups(defaultGroups);

        List<Group> groups = Lists.newArrayList();
        Group g1 = new Group("slugText1x", "niceNameText1x");
        Group g2 = new Group("slugText2x", "niceNameText2x");
        groups.add(g1);
        groups.add(g2);
        organization.setGroups(groups);

        return organization;
    }
}
