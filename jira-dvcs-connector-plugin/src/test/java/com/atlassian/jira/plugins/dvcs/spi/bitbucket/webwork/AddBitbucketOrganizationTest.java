package com.atlassian.jira.plugins.dvcs.spi.bitbucket.webwork;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsConfigAddEndedAnalyticsEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsConfigAddLifecycleAnalyticsEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsConfigAddStartedAnalyticsEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsType;
import com.atlassian.jira.plugins.dvcs.analytics.event.FailureReason;
import com.atlassian.jira.plugins.dvcs.analytics.event.Source;
import com.atlassian.jira.plugins.dvcs.analytics.smartcommits.SmartCommitsAnalyticsService;
import com.atlassian.jira.plugins.dvcs.auth.OAuthStore;
import com.atlassian.jira.plugins.dvcs.bbrebrand.BitbucketRebrandDarkFeature;
import com.atlassian.jira.plugins.dvcs.exception.SourceControlException;
import com.atlassian.jira.plugins.dvcs.exception.SourceControlException.UnauthorisedException;
import com.atlassian.jira.plugins.dvcs.model.AccountInfo;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.BitbucketCommunicator;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.HttpClientProvider;
import com.atlassian.jira.plugins.dvcs.util.SystemUtils;
import com.atlassian.jira.plugins.dvcs.util.TestNGMockComponentContainer;
import com.atlassian.jira.plugins.dvcs.util.TestNGMockHttp;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.HttpServletVariables;
import com.atlassian.jira.web.action.RedirectSanitiser;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.web.servlet.api.ServletForwarder;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.scribe.exceptions.OAuthException;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import webwork.action.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.http.HTTPException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class AddBitbucketOrganizationTest {
    private static final String SAMPLE_SOURCE = "devtools";
    private static final String SAMPLE_XSRF_TOKEN = "xsrfToken";
    private static final String SAMPLE_AUTH_URL = "http://authurl.com";

    private static final String ADD_STARTED_EVENT_NAME = "jira.dvcsconnector.config.add.bitbucket.started";
    private static final String ADD_SUCCEEDED_EVENT_NAME = "jira.dvcsconnector.config.add.bitbucket.ended.succeeded";
    private static final String ADD_FAILED_EVENT_NAME = "jira.dvcsconnector.config.add.bitbucket.ended.failed";
    private static final String testingURL = "localhost:8890";
    private final TestNGMockComponentContainer mockComponentContainer = new TestNGMockComponentContainer(this);
    private final TestNGMockHttp mockHttp = TestNGMockHttp.withMockitoMocks();
    @Mock
    @AvailableInContainer
    private XsrfTokenGenerator xsrfTokenGenerator;
    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    @AvailableInContainer
    private RedirectSanitiser redirectSanitiser;
    private HttpServletResponse response;
    @Mock
    private ApplicationProperties ap;
    @Mock
    private BitbucketRebrandDarkFeature bitbucketRebrandDarkFeature;
    @Mock
    private EventPublisher eventPublisher;
    @Captor
    private ArgumentCaptor<DvcsConfigAddLifecycleAnalyticsEvent> analyticsEventCaptor;
    @Captor
    private ArgumentCaptor<DvcsConfigAddEndedAnalyticsEvent> addEndedCaptor;
    @Mock
    private OrganizationService organizationService;
    @Mock
    private OAuthStore oAuthStore;
    @Mock
    private OAuthService oAuthService;
    @Mock
    private HttpClientProvider httpClientProvider;
    @Mock
    private AccountInfo accountInfo;
    @Mock
    private SmartCommitsAnalyticsService smartCommitsAnalyticsService;
    @Mock
    @AvailableInContainer
    private HttpServletVariables httpServletVariables;
    private AddBitbucketOrganization addBitbucketOrganization;
    @Mock
    @AvailableInContainer
    private ServletForwarder servletForwarder;

    @BeforeMethod(alwaysRun = true)
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mockComponentContainer.beforeMethod();
        mockHttp.beforeMethod();
        final HttpServletRequest request = mockHttp.mockRequest();
        response = mockHttp.mockResponse();

        when(xsrfTokenGenerator.generateToken(request)).thenReturn(SAMPLE_XSRF_TOKEN);
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(redirectSanitiser.makeSafeRedirectUrl(anyString())).then(returnsFirstArg()); // returns the same url

        when(oAuthStore.getClientId(anyString())).thenReturn("apiKey");
        when(oAuthStore.getSecret(anyString())).thenReturn("apiSecret");

        when(request.getParameter("oauth_verifier")).thenReturn("verifier");
        HttpSession session = mock(HttpSession.class);
        when(httpServletVariables.getHttpRequest()).thenReturn(request);
        when(httpServletVariables.getHttpSession()).thenReturn(session);
        when(request.getSession()).thenReturn(session);
        Token requestToken = mock(Token.class);
        when(session.getAttribute(AddBitbucketOrganization.SESSION_KEY_REQUEST_TOKEN)).thenReturn(requestToken);

        final Token accessToken = mock(Token.class);
        when(accessToken.getToken()).thenReturn("accessToken");
        when(accessToken.getSecret()).thenReturn("accessSecret");
        when(oAuthService.getAccessToken(eq(requestToken), any(Verifier.class))).thenReturn(accessToken);
        when(oAuthService.getRequestToken()).thenReturn(requestToken);
        when(oAuthService.getAuthorizationUrl(eq(requestToken))).thenReturn(SAMPLE_AUTH_URL);

        when(organizationService.save(any(Organization.class))).then(i -> i.getArguments()[0]);

        addBitbucketOrganization = new AddBitbucketOrganization(ap, bitbucketRebrandDarkFeature, eventPublisher,
                httpClientProvider, oAuthStore, organizationService, smartCommitsAnalyticsService) {
            @Override
            OAuthService createOAuthScribeService() {
                return oAuthService;
            }
        };
        addBitbucketOrganization.setUrl("http://url.com");
    }

    @AfterMethod
    public void tearDown() {
        ComponentAccessor.initialiseWorker(null); // reset
        mockComponentContainer.afterMethod();
        mockHttp.afterMethod();
        BitbucketDetails.resetHostUrl();
    }

    @Test
    public void testDoExecuteAnalytics() throws Exception {
        addBitbucketOrganization.setSource(SAMPLE_SOURCE);
        String ret = addBitbucketOrganization.doExecute();
        assertThat(ret, equalTo(Action.NONE));
        verify(eventPublisher).publish(new DvcsConfigAddStartedAnalyticsEvent(Source.DEVTOOLS, DvcsType.BITBUCKET));
        verifyNoMoreInteractions(eventPublisher);
        verify(response).sendRedirect(eq(SAMPLE_AUTH_URL));
        verifyNoMoreInteractions(response);
    }

    @Test
    public void testDoExecuteAnalyticsDefaultSource() throws Exception {
        addBitbucketOrganization.setSource(null);
        String ret = addBitbucketOrganization.doExecute();
        assertThat(ret, equalTo(Action.NONE));
        verify(eventPublisher).publish(new DvcsConfigAddStartedAnalyticsEvent(Source.UNKNOWN, DvcsType.BITBUCKET));
        verifyNoMoreInteractions(eventPublisher);
        verify(response).sendRedirect(eq(SAMPLE_AUTH_URL));
        verifyNoMoreInteractions(response);
    }

    @Test
    public void testDoExecuteAnalyticsError() throws Exception {
        addBitbucketOrganization.setSource(SAMPLE_SOURCE);
        reset(oAuthService);
        when(oAuthService.getRequestToken()).thenThrow(new OAuthException(""));

        String ret = addBitbucketOrganization.doExecute();

        assertThat(ret, equalTo(Action.INPUT));

        verify(eventPublisher, times(2)).publish(analyticsEventCaptor.capture());

        final DvcsConfigAddStartedAnalyticsEvent addStartedEvent = (DvcsConfigAddStartedAnalyticsEvent) analyticsEventCaptor.getAllValues().get(0);
        assertThat(addStartedEvent.determineEventName(), is(ADD_STARTED_EVENT_NAME));
        assertThat(addStartedEvent.getSource(), is(Source.DEVTOOLS.toString()));

        final DvcsConfigAddEndedAnalyticsEvent addEndedEvent = (DvcsConfigAddEndedAnalyticsEvent) analyticsEventCaptor.getAllValues().get(1);
        assertThat(addEndedEvent.determineEventName(), is(ADD_FAILED_EVENT_NAME));
        assertThat(addEndedEvent.getSource(), is(Source.DEVTOOLS.toString()));
        assertThat(addEndedEvent.getReason(), is(FailureReason.OAUTH_TOKEN.toString()));

        verifyNoMoreInteractions(response);
    }

    @Test
    public void testDoFinishAnalytics() throws Exception {
        addBitbucketOrganization.setSource(SAMPLE_SOURCE);

        String ret = addBitbucketOrganization.doFinish();

        assertThat(ret, equalTo(Action.NONE));

        verify(eventPublisher).publish(addEndedCaptor.capture());
        assertThat(addEndedCaptor.getValue().determineEventName(), is(ADD_SUCCEEDED_EVENT_NAME));
        assertThat(addEndedCaptor.getValue().getSource(), is(Source.DEVTOOLS.toString()));
        assertThat(addEndedCaptor.getValue().getReason(), is(nullValue()));
        verifyNoMoreInteractions(eventPublisher);

        verify(response).sendRedirect(
                "ConfigureDvcsOrganizations.jspa?atl_token=" + SAMPLE_XSRF_TOKEN + "&source=" + SAMPLE_SOURCE);
        verifyNoMoreInteractions(response);
    }

    @Test
    public void testDoFinishAnalyticsDefaultSource() throws Exception {
        addBitbucketOrganization.setSource(null);

        String ret = addBitbucketOrganization.doFinish();

        assertThat(ret, equalTo(Action.NONE));

        verify(eventPublisher).publish(addEndedCaptor.capture());
        assertThat(addEndedCaptor.getValue().determineEventName(), is(ADD_SUCCEEDED_EVENT_NAME));
        assertThat(addEndedCaptor.getValue().getSource(), is(Source.UNKNOWN.toString()));
        assertThat(addEndedCaptor.getValue().getReason(), is(nullValue()));
        verifyNoMoreInteractions(eventPublisher);

        verify(response).sendRedirect(eq("ConfigureDvcsOrganizations.jspa?atl_token=" + SAMPLE_XSRF_TOKEN)); // source parameter skipped
        verifyNoMoreInteractions(response);
    }

    @Test
    public void testDoFinishAnalyticsErrorUnauth() throws Exception {
        addBitbucketOrganization.setSource(SAMPLE_SOURCE);
        doThrow(UnauthorisedException.class).when(organizationService).save(any(Organization.class));

        String ret = addBitbucketOrganization.doFinish();

        assertThat(ret, equalTo(Action.INPUT));

        verify(eventPublisher).publish(addEndedCaptor.capture());
        assertThat(addEndedCaptor.getValue().determineEventName(), is(ADD_FAILED_EVENT_NAME));
        assertThat(addEndedCaptor.getValue().getSource(), is(Source.DEVTOOLS.toString()));
        assertThat(addEndedCaptor.getValue().getReason(), is(FailureReason.OAUTH_UNAUTH.toString()));
        verifyNoMoreInteractions(eventPublisher);

        verifyNoMoreInteractions(response);
    }

    @Test
    public void testDoFinishAnalyticsErrorSourceControl() throws Exception {
        addBitbucketOrganization.setSource(SAMPLE_SOURCE);
        reset(organizationService);
        doThrow(SourceControlException.class).when(organizationService).save(any(Organization.class));

        String ret = addBitbucketOrganization.doFinish();

        assertThat(ret, equalTo(Action.INPUT));

        verify(eventPublisher).publish(addEndedCaptor.capture());
        assertThat(addEndedCaptor.getValue().determineEventName(), is(ADD_FAILED_EVENT_NAME));
        assertThat(addEndedCaptor.getValue().getSource(), is(Source.DEVTOOLS.toString()));
        assertThat(addEndedCaptor.getValue().getReason(), is(FailureReason.OAUTH_SOURCECONTROL.toString()));
        verifyNoMoreInteractions(eventPublisher);

        verifyNoMoreInteractions(response);
    }

    @Test
    public void testDoValidationAnalyticsError() throws Exception {
        addBitbucketOrganization.setSource(SAMPLE_SOURCE);
        addBitbucketOrganization.setOrganization(null); // cause validation error

        addBitbucketOrganization.doValidation();

        verify(eventPublisher).publish(addEndedCaptor.capture());
        assertThat(addEndedCaptor.getValue().determineEventName(), is(ADD_FAILED_EVENT_NAME));
        assertThat(addEndedCaptor.getValue().getSource(), is(Source.DEVTOOLS.toString()));
        assertThat(addEndedCaptor.getValue().getReason(), is(FailureReason.VALIDATION.toString()));
        verifyNoMoreInteractions(eventPublisher);
    }

    @Test
    public void testDoValidationAnalyticsNoError() throws Exception {
        addBitbucketOrganization.setSource(SAMPLE_SOURCE);
        addBitbucketOrganization.setOrganization("org");
        final AccountInfo accountInfo = mock(AccountInfo.class);
        when(organizationService.getAccountInfo(anyString(), anyString(), eq(BitbucketCommunicator.BITBUCKET)))
                .thenReturn(accountInfo);
        addBitbucketOrganization.doValidation();
        verifyNoMoreInteractions(eventPublisher);
    }

    @Test
    public void testAddAccountWithSmartCommitsEnabledFiresEvent() throws Exception {
        addBitbucketOrganization.setAutoSmartCommits("true");
        addBitbucketOrganization.setSource(SAMPLE_SOURCE);
        String ret = addBitbucketOrganization.doFinish();
        assertThat(ret, equalTo(Action.NONE));
        verify(smartCommitsAnalyticsService).fireNewOrganizationAddedWithSmartCommits(DvcsType.BITBUCKET, true);
        verify(response).sendRedirect(
                "ConfigureDvcsOrganizations.jspa?atl_token=" + SAMPLE_XSRF_TOKEN + "&source=" + SAMPLE_SOURCE);
        verifyNoMoreInteractions(response);
    }

    @Test
    public void testExpectedAnalyticsWhenCtkState() throws Exception {
        setupForCtkStateTest();
        String response = addBitbucketOrganization.doExecute();
        assertThat(response, equalTo(Action.NONE));

        verify(eventPublisher, times(2)).publish(analyticsEventCaptor.capture());

        final DvcsConfigAddStartedAnalyticsEvent addStartedEvent = (DvcsConfigAddStartedAnalyticsEvent) analyticsEventCaptor.getAllValues().get(0);
        assertThat(addStartedEvent.determineEventName(), is(ADD_STARTED_EVENT_NAME));
        assertThat(addStartedEvent.getSource(), is(Source.UNKNOWN.toString()));

        final DvcsConfigAddEndedAnalyticsEvent addEndedEvent = (DvcsConfigAddEndedAnalyticsEvent) analyticsEventCaptor.getAllValues().get(1);
        assertThat(addEndedEvent.determineEventName(), is(ADD_SUCCEEDED_EVENT_NAME));
        assertThat(addEndedEvent.getSource(), is(Source.UNKNOWN.toString()));
        assertThat(addEndedEvent.getReason(), is(nullValue()));

        verifyNoMoreInteractions(eventPublisher);
    }

    @Test
    public void testValidationWhenCtkState() throws Exception {
        setupForCtkStateTest();
        addBitbucketOrganization.doValidation();
        assertThat(addBitbucketOrganization.getUrl(), equalTo(testingURL));
    }

    @Test
    public void addInvalidAccount() {
        String BAD_ACCOUNT_NAME = "BAD ACCOUNT NAME";
        addBitbucketOrganization.setOrganization(BAD_ACCOUNT_NAME);

        when(organizationService.getAccountInfo(testingURL, BAD_ACCOUNT_NAME, BitbucketCommunicator.BITBUCKET))
                .thenReturn(null);

        addBitbucketOrganization.doValidation();

        assertThat(addBitbucketOrganization.getErrorMessages(), hasSize(1));
        assertThat(addBitbucketOrganization.getErrorMessages().iterator().next(), is("Invalid user/team account."));
    }

    @Test
    public void addAccountWithInvalidOauthToken() throws Exception {
        when(SystemUtils.getRedirect(addBitbucketOrganization, SAMPLE_AUTH_URL, true))
                .thenThrow(new HTTPException(401));

        addBitbucketOrganization.doExecute();

        assertThat(addBitbucketOrganization.getErrorMessages(), hasSize(1));
        assertThat(addBitbucketOrganization.getErrorMessages().iterator().next(),
                is("The authentication with Bitbucket has failed. Please check your OAuth settings."));
    }

    private void setupForCtkStateTest() {
        BitbucketDetails.setHostUrl(testingURL);
        addBitbucketOrganization = new AddBitbucketOrganization(ap, bitbucketRebrandDarkFeature, eventPublisher,
                httpClientProvider, oAuthStore, organizationService, smartCommitsAnalyticsService) {
            @Override
            OAuthService createOAuthScribeService() {
                return oAuthService;
            }
        };
        addBitbucketOrganization.setOrganization("org");
        when(organizationService.getAccountInfo(testingURL, "org", BitbucketCommunicator.BITBUCKET))
                .thenReturn(accountInfo);
    }
}
