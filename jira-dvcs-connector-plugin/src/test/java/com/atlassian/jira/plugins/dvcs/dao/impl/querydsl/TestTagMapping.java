package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageTagMapping;
import net.java.ao.EntityManager;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.beans.PropertyChangeListener;

class TestTagMapping implements MessageTagMapping {

    private int id;
    private String tag;

    public TestTagMapping(String tag, int id) {
        this.tag = tag;
        this.id = id;
    }

    @Override
    public String getTag() {
        return tag;
    }

    @Override
    public int getID() {
        return 1;
    }

    @Override
    public MessageMapping getMessage() {
        throw new NotImplementedException();
    }

    @Override
    public void init() {
        throw new NotImplementedException();
    }

    @Override
    public void save() {
        throw new NotImplementedException();
    }

    @Override
    public EntityManager getEntityManager() {
        throw new NotImplementedException();
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        throw new NotImplementedException();
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        throw new NotImplementedException();
    }

    @Override
    public Class getEntityType() {
        throw new NotImplementedException();
    }
}
