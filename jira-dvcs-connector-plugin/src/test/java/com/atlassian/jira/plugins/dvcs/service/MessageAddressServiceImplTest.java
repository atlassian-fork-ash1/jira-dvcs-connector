package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettings;
import com.atlassian.jira.plugins.dvcs.service.MessageAddressServiceImpl.IdKey;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddress;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.message.BitbucketSynchronizeActivityMessage;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.message.BitbucketSynchronizeChangesetMessage;
import com.atlassian.jira.plugins.dvcs.sync.BitbucketSynchronizeActivityMessageConsumer;
import com.atlassian.jira.plugins.dvcs.util.MockitoTestNgListener;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.when;

@Listeners(MockitoTestNgListener.class)
public class MessageAddressServiceImplTest {
    private static final String OTHER_KEY = "abc";

    @Mock
    private CacheManager cacheManager;

    @Mock
    private Cache cache;

    @Mock
    private MessageAddress address1;

    @Mock
    private MessageAddress address2;

    private MessageAddressServiceImpl messageAddressService;

    @BeforeMethod
    public void setUpMessagingService() throws Exception {
        MockitoAnnotations.initMocks(this);

        //Mockito
        when(cacheManager.getCache(anyString(), any(), any(CacheSettings.class)))
                .thenReturn(cache);

        when(cache.get(argThat(new matchesKey(
                BitbucketSynchronizeActivityMessage.class,
                BitbucketSynchronizeActivityMessageConsumer.KEY))))
                .thenReturn(address1);

        when(cache.get(argThat(new matchesKey(
                BitbucketSynchronizeActivityMessage.class,
                OTHER_KEY))))
                .thenReturn(address2);

        messageAddressService = new MessageAddressServiceImpl(cacheManager);
    }

    @Test
    public void getReturnsDifferentInstancesGivenDifferentKeys() throws Exception {
        final MessageAddress<BitbucketSynchronizeActivityMessage> address =
                messageAddressService.get(
                        BitbucketSynchronizeActivityMessage.class,
                        BitbucketSynchronizeActivityMessageConsumer.KEY);
        final MessageAddress<BitbucketSynchronizeActivityMessage> address2 =
                messageAddressService.get(BitbucketSynchronizeActivityMessage.class, OTHER_KEY);
        assertThat(address, not(sameInstance(address2)));
    }

    @Test
    public void getReturnsSameInstanceGivenSameKey() throws Exception {
        final MessageAddress<BitbucketSynchronizeActivityMessage> address =
                messageAddressService.get(
                        BitbucketSynchronizeActivityMessage.class,
                        BitbucketSynchronizeActivityMessageConsumer.KEY);
        final MessageAddress<BitbucketSynchronizeActivityMessage> address2 =
                messageAddressService.get(
                        BitbucketSynchronizeActivityMessage.class,
                        BitbucketSynchronizeActivityMessageConsumer.KEY);
        assertThat(address, sameInstance(address2));
    }

    @Test
    public void getReturnsSameInstanceGivenSameKeyButDifferentPayloadType() throws Exception {
        final MessageAddress<BitbucketSynchronizeActivityMessage> address =
                messageAddressService.get(
                        BitbucketSynchronizeActivityMessage.class,
                        BitbucketSynchronizeActivityMessageConsumer.KEY);
        final MessageAddress<?> address2 =
                messageAddressService.get(BitbucketSynchronizeChangesetMessage.class,
                        BitbucketSynchronizeActivityMessageConsumer.KEY);
        //noinspection unchecked
        assertThat(address, sameInstance((MessageAddress<BitbucketSynchronizeActivityMessage>) address2));
    }

    private class matchesKey extends ArgumentMatcher {

        final String id;
        final Class payloadType;

        public matchesKey(final Class payloadType, final String id) {
            this.id = id;
            this.payloadType = payloadType;
        }

        @Override
        public boolean matches(Object argument) {

            IdKey key = new IdKey(id, payloadType);
            return key.equals(argument);
        }
    }

}
