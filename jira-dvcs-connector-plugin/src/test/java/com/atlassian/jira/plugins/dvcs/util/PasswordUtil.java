package com.atlassian.jira.plugins.dvcs.util;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Password-related utility methods.
 */
public final class PasswordUtil {

    /**
     * Returns the given user's password.
     *
     * @param username the username for which to get the password
     * @return <code>null</code> if the password is unknown
     */
    @Nullable
    public static String getPassword(@Nonnull final String username) {
        return System.getProperty(username + ".password");
    }

    private PasswordUtil() {}
}
