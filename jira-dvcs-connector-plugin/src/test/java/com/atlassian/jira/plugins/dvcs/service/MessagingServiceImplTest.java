package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.jira.plugins.dvcs.dao.MessageDao;
import com.atlassian.jira.plugins.dvcs.dao.SyncAuditLogDao;
import com.atlassian.jira.plugins.dvcs.event.CarefulEventService;
import com.atlassian.jira.plugins.dvcs.model.DefaultProgress;
import com.atlassian.jira.plugins.dvcs.model.Progress;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.message.HasProgress;
import com.atlassian.jira.plugins.dvcs.service.message.MessageConsumer;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.HttpClientProvider;
import com.atlassian.jira.plugins.dvcs.sync.Synchronizer;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MessagingServiceImplTest {
    private static final int REPO_ID = 123;

    private Progress progress;

    @Mock
    private CarefulEventService eventService;

    @Mock
    private ChangesetService changesetService;

    @Mock
    private HttpClientProvider httpClientProvider;

    @Mock
    private MessageConsumer<? extends HasProgress> consumer;

    @Mock
    private LinkerService linkerService;

    @Mock
    private MessageDao messageDao;

    @Mock
    private Repository repository;

    @Mock
    private Synchronizer synchronizer;

    @Mock
    private SyncAuditLogDao syncAudit;

    @InjectMocks
    private MessagingServiceImpl messagingService;

    @BeforeMethod
    public void setUpMessagingService() throws Exception {
        messagingService = new MessagingServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    @BeforeMethod
    public void setUpOtherMocks() throws Exception {
        progress = new DefaultProgress();

        when(repository.getId()).thenReturn(REPO_ID);
        when(messageDao.getMessagesForConsumingCount(MessagingServiceImpl.SYNCHRONIZATION_REPO_TAG_PREFIX + REPO_ID)).thenReturn(0);
    }

    @Test
    public void tryEndProgressShouldDispatchEventsWhenSmartCommitsProcessingSucceeds() throws Exception {
        messagingService.tryEndProgress(repository, progress, consumer, 456);
        verify(eventService).dispatchEvents(repository);
    }

    @Test
    public void tryEndProgressShouldDispatchEventsAfterSmartCommitsProcessingIsDone() throws Exception {
        messagingService.tryEndProgress(repository, progress, consumer, 456);
        verify(eventService).dispatchEvents(repository);
    }

    @Test
    public void tryEndProgressShouldNotDispatchEventsWhenThereIsASyncError() throws Exception {
        progress.setError("bad juju");

        messagingService.tryEndProgress(repository, progress, consumer, 456);
        verify(eventService, never()).dispatchEvents(repository);
    }

    @Test
    public void auditIdShouldBeZeroIfTagsContainNoValidId() {
        // Set up
        final String[] tags = {"foo", "audit-id-bar", "audit-id-"};

        // Invoke
        final int auditId = messagingService.getSynchronizationAuditIdFromTags(tags);

        // Check
        assertThat(auditId, is(0));
    }

    @Test
    public void auditIdShouldBeReturnedIfTagsContainValidId() {
        // Set up
        final String[] tags = {"foo", "audit-id-bar", "audit-id-666"};

        // Invoke
        final int auditId = messagingService.getSynchronizationAuditIdFromTags(tags);

        // Check
        assertThat(auditId, is(666));
    }
}
