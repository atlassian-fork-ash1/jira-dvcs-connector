package it.com.atlassian.jira.plugins;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.plugins.dvcs.util.DvcsConstants;
import com.atlassian.jira.testkit.client.PluginsControl;
import com.atlassian.plugin.PluginState;

import javax.annotation.Nonnull;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Test helper methods relating to plugins.
 */
public final class PluginUtil {
    /**
     * The complete plugin system key of the DVCS plugin.
     */
    public static final String DVCS_PLUGIN_KEY = DvcsConstants.PLUGIN_KEY;

    private PluginUtil() {
    }

    /**
     * Asserts that the plugin with the given key is in the given state.
     *
     * @param backdoor      the backdoor for the JIRA instance to check
     * @param pluginKey     the complete plugin key
     * @param expectedState the expected state
     */
    public static void assertPluginState(@Nonnull final Backdoor backdoor,
                                         @Nonnull final String pluginKey, @Nonnull final PluginState expectedState) {
        assertPluginState(backdoor.plugins(), pluginKey, expectedState);
    }

    /**
     * Exactly like {@link #assertPluginState} but uses {@link com.atlassian.jira.testkit.client.Backdoor}
     * instead of {@link Backdoor}.
     */
    public static void assertPluginState(@Nonnull final com.atlassian.jira.testkit.client.Backdoor backdoor,
                                         @Nonnull final String pluginKey, @Nonnull final PluginState expectedState) {
        assertPluginState(backdoor.plugins(), pluginKey, expectedState);
    }

    private static void assertPluginState(@Nonnull final PluginsControl pluginsControl,
                                          @Nonnull final String pluginKey, @Nonnull final PluginState expectedState) {
        assertThat("Plugin state validation failed for " + pluginKey,
                pluginsControl.getPluginState(pluginKey), is(expectedState.name()));
    }
}
