package it.com.atlassian.jira.plugins.dvcs;

import com.atlassian.httpclient.api.HttpStatus;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.pageobjects.config.EnvironmentBasedProductInstance;
import com.atlassian.jira.pageobjects.config.ProductInstanceBasedEnvironmentData;
import com.atlassian.jira.plugins.dvcs.base.resource.TimestampNameTestResource;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Progress;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.rest.json.CredentialJson;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.BitbucketRemoteClient;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConsumer;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketRepository;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketRepositoryLink;
import com.atlassian.jira.plugins.dvcs.util.PasswordUtil;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.beust.jcommander.internal.Lists;
import it.util.LocalDevOnlyPropertiesLoader;
import it.util.LocalGitRepo;
import it.util.TestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static it.util.TestAccounts.DVCS_CONNECTOR_TEST_ACCOUNT;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * Tests how Dvcs Connector sets up Bitbucket Linkers for repos that are linked in Jira.
 * <br><br>
 * No webdriver used in these tests.
 */
public class BitbucketLinkersTest {
    private static final String BB_REPO_NAME_PREFIX = "linkingtestrepo_";
    private static final String DVCS_REPO_OWNER = DVCS_CONNECTOR_TEST_ACCOUNT;
    private static final String PROJECT_KEY_PREFIX = "TPL";

    private static final JIRAEnvironmentData ENVIRONMENT_DATA =
            new ProductInstanceBasedEnvironmentData(new EnvironmentBasedProductInstance());
    private static final Backdoor BACKDOOR = new Backdoor(ENVIRONMENT_DATA);
    private static final DvcsConnectorRestClient DVCS_CONNECTOR_REST_CLIENT =
            new DvcsConnectorRestClient(ENVIRONMENT_DATA);
    private static final TimestampNameTestResource TIMESTAMP_NAME_TEST_RESOURCE = new TimestampNameTestResource();
    /**
     * Keys of Jira issues created by test setup
     **/
    private final List<String> issueKeys = Lists.newArrayList();
    /**
     * Keys of Jira projects created by test setup
     **/
    private final List<String> projectKeys = Lists.newArrayList();
    private BitbucketConsumer bitbucketConsumer;
    private BitbucketRemoteClient bitbucketRemoteClient;
    private String dvcsRepoPassword;
    private Git localGitRepo;
    private Organization testDvcsOrganization;
    private BitbucketRepository testBitbucketRepo;

    @BeforeClass
    public void setup() throws IOException {
        // Load local dev properties
        LocalDevOnlyPropertiesLoader.loadLocalDevOnlyProperties();
        dvcsRepoPassword = PasswordUtil.getPassword(DVCS_REPO_OWNER);

        // Setup Bitbucket remote client
        bitbucketRemoteClient = new BitbucketRemoteClient(dvcsRepoOwner(), dvcsRepoPassword());

        // Remove all Projects that might have been previously created by this test class
        removeJiraProjectsCreatedByThisTestClass();

        dvcsConnectorRestClient().deleteAllOrganizations();

        // Configure BB account OAuth consumer
        bitbucketConsumer = bitbucketRemoteClient().getConsumerRemoteRestpoint().createConsumer(dvcsRepoOwner());

        // Create Organization in DVCS & update OAuth credentials
        testDvcsOrganization =
                createDvcsConnectorOrganization(bitbucketConsumer.getKey(), bitbucketConsumer.getSecret());

        // Create new BB repo
        testBitbucketRepo = createTestBitbucketRepo(BB_REPO_NAME_PREFIX);

        // Create local GIT repo & configure GIT repo remote to point to BB repo
        localGitRepo = LocalGitRepo.createLocalTempGitRepo();
        LocalGitRepo.configureRemoteOriginToBitbucket(localGitRepo, dvcsRepoOwner(), testBitbucketRepo.getSlug());
    }

    /**
     * Tests scenario when DVCS Connector attempts to configure Linkers in Bitbucket for BB repo
     * where commit messages refer issues from many different projects. This was previously broken
     * because Linkers are configured via regex that contains keys of all projects and that regex
     * is sent to Bitbucket. However, Bitbucket only allows 255 char limit on that regex.
     * <br><br>
     * In response to issue: FUSE-2836
     */
    @Test
    public void testBitbucketLinkersWorkWhenLargeNumberOfJiraProjectsExist() throws GitAPIException {
        // PREPARE

        // Create Test projects in JIRA. 60 Projects, 1 Issue each. We want enough projects such that
        // it will make Dvcs Connector create two Linker definitions in Bitbucket.
        createProjectsAndIssuesInJira("TPL", "Test Linkers Project ", 60, 1);

        // Commit to local GIT repo with proper commit message and push to Bitbucket remote
        final String commitMessage = createCommitMessageWithIssueKeys("Message ", issueKeys);
        makeDummyGitCommitWithMessage(localGitRepo, commitMessage);
        // Git PUSH
        localGitRepo.push().setCredentialsProvider(
                new UsernamePasswordCredentialsProvider(dvcsRepoOwner(), dvcsRepoPassword()))
                .call();

        // Trigger repo list sync in DVCS connector
        dvcsConnectorRestClient().syncRepoList(testDvcsOrganization.getId());

        // Retrieve the organization again to get synced repo list
        testDvcsOrganization = retrieveDvcsOrganization(testDvcsOrganization.getId());

        // Make test repo linked (enabled)
        final Optional<Repository> testRepoOptional = testDvcsOrganization.getRepositories().stream()
                .filter(r -> testBitbucketRepo.getName().equals(r.getName()))
                .findFirst();
        if (!testRepoOptional.isPresent()) {
            fail("Cannot find testing Bitbucket repo in DVCS connector synced repos. Repo name: "
                    + testBitbucketRepo.getName());
        }
        final Repository testRepo = testRepoOptional.get();
        final Response enableRepositoryResponse = dvcsConnectorRestClient().enableRepository(testRepo.getId());
        assertEquals(enableRepositoryResponse.statusCode, 204);

        addBitbucketLinksPointingToOtherJiraInstances();

        // Precondition - there should be 4 pre-existing links in BB (pointing to fake JIRA instances)
        final List<BitbucketRepositoryLink> repositoryLinksInBitbucketBefore = bitbucketRemoteClient()
                .getRepositoryLinksRest().getRepositoryLinks(dvcsRepoOwner(), testRepo.getSlug());
        assertEquals(repositoryLinksInBitbucketBefore.size(), 4);

        // TEST PHASE 1 - Trigger sync on test repo, this will invoke BB Link creation
        dvcsConnectorRestClient().startRepoFullSync(testRepo.getId());

        // Wait for sync result by observing repository information
        final boolean repoSyncCompleted = TestUtils.checkInLoop(
                TimeUnit.SECONDS.toMillis(15), TimeUnit.SECONDS.toMillis(1), () -> {
                    final Repository repository = retrieveDvcsRepository(testRepo.getId());
                    final Progress sync = repository.getSync();
                    return sync != null && sync.isFinished();
                });
        assertTrue(repoSyncCompleted, "It looks like sync for repo id: " + testRepo.getId()
                + " did not finish in time.");

        // VERIFY PHASE 1
        // Retrieve Linkers from Bitbucket and see if they look like what we expected
        final List<BitbucketRepositoryLink> repositoryLinksInBitbucketAfter = bitbucketRemoteClient()
                .getRepositoryLinksRest().getRepositoryLinks(dvcsRepoOwner(), testRepo.getSlug());
        // We expect to see 6 links after sync - 4 pre-existing + 2 new created by repo sync
        assertEquals(repositoryLinksInBitbucketAfter.size(), 6);

        // TEST PHASE 2 - unlink test DCSR repo - this will invoke relevant BB Link removal. Here
        // we are testing that the BB Links that existed previously are not removed by accident.
        dvcsConnectorRestClient().disableRepository(testRepo.getId());

        // VERIFY PHASE 2
        // Retrieve Linkers from Bitbucket and see if they look like what we expected
        final List<BitbucketRepositoryLink> repositoryLinksInBitbucketAfterRepoDisable = bitbucketRemoteClient()
                .getRepositoryLinksRest().getRepositoryLinks(dvcsRepoOwner(), testRepo.getSlug());
        // We expect to see 6 links after sync - 4 pre-existing + 2 new created by repo sync
        assertEquals(repositoryLinksInBitbucketAfterRepoDisable.size(), 4);
    }

    @AfterClass
    public void tearDown() {
        // Delete temp local GIT repo
        LocalGitRepo.removeLocalGitRepo(localGitRepo);

        // Remove DVCS Organization in Jira
        Response deleteOrganizationResponse =
                dvcsConnectorRestClient().deleteOrganization(testDvcsOrganization.getId(), true);
        assertEquals(deleteOrganizationResponse.statusCode, 204);

        // Delete Bitbucket repo
        bitbucketRemoteClient().getRepositoriesRest().removeRepository(dvcsRepoOwner(), testBitbucketRepo.getName());

        // Remove OAuth consumer in BB
        bitbucketRemoteClient().getConsumerRemoteRestpoint()
                .deleteConsumer(dvcsRepoOwner(), bitbucketConsumer.getId().toString());

        removeJiraProjectsCreatedByThisTestClass();
    }

    private void addBitbucketLinksPointingToOtherJiraInstances() {
        bitbucketRemoteClient().getRepositoryLinksRest().addCustomRepositoryLink(
                dvcsRepoOwner(), testBitbucketRepo.getSlug(), "https://someurl1.com/", "someurl1 regex");
        bitbucketRemoteClient().getRepositoryLinksRest().addCustomRepositoryLink(
                dvcsRepoOwner(), testBitbucketRepo.getSlug(), "https://someurl2.com/", "someurl1 regex");
        bitbucketRemoteClient().getRepositoryLinksRest().addCustomRepositoryLink(
                dvcsRepoOwner(), testBitbucketRepo.getSlug(), "https://someurl3.com/", "someurl1 regex");
        bitbucketRemoteClient().getRepositoryLinksRest().addRepositoryLink(
                dvcsRepoOwner(), testBitbucketRepo.getSlug(), "jira", "https://someurl3.com/", "PROJKEY");
    }

    private Repository retrieveDvcsRepository(final int id) {
        final Response<Repository> repositoryResponse = dvcsConnectorRestClient().getRepository(id);
        assertEquals(repositoryResponse.statusCode, HttpStatus.OK.code,
                "Wrong status code while retrieving Dvcs Repository id: " + id);
        assertNotNull(repositoryResponse.body,
                "Received null while retrieving Dvcs Repository id: " + id);
        return repositoryResponse.body;
    }

    private Organization retrieveDvcsOrganization(final int id) {
        final Response<Organization> organizationResponse =
                dvcsConnectorRestClient().getOrganization(testDvcsOrganization.getId());
        assertEquals(organizationResponse.statusCode, HttpStatus.OK.code,
                "Wrong status code while retrieving Dvcs Organization id: " + id);
        assertNotNull(organizationResponse.body,
                "Received null while retrieving Dvcs Organization id: " + id);
        return organizationResponse.body;
    }

    private void removeJiraProjectsCreatedByThisTestClass() {
        backdoor().project().getProjects().stream()
                .map(p -> p.key)
                .filter(Objects::nonNull)
                .filter(key -> key.startsWith(PROJECT_KEY_PREFIX))
                .forEach(key -> backdoor().project().deleteProject(key));
    }

    private String createCommitMessageWithIssueKeys(final String messagePrefix, final List<String> issueKeys) {
        final StringBuilder sb = new StringBuilder(messagePrefix);
        for (final String issueKey : issueKeys) {
            sb.append(" | ");
            sb.append(issueKey);
            sb.append(" | ");
        }
        return sb.toString();
    }

    /**
     * Creates specified number of projects and issues in each project. Project keys and issue keys
     * of created Entities will be added in projectKeys and issueKeys lists.
     */
    private void createProjectsAndIssuesInJira(
            final String projectKeyPrefix, final String projectNamePrefix,
            final int projectCount, final int issuePerProjectCount) {
        for (int i = 1; i <= projectCount; i++) {
            final String projectKey = projectKeyPrefix + i;
            final long projectId = backdoor().project()
                    .addProject(projectNamePrefix + i, projectKey, "admin");
            projectKeys.add(projectKey);
            for (int x = 1; x <= issuePerProjectCount; x++) {
                final IssueCreateResponse issueCreateResponse = backdoor().issues().createIssue(projectId,
                        String.format("TPL%s Issue %s", i, x));
                final String newIssueKey = issueCreateResponse.key();
                assertFalse(StringUtils.isBlank(newIssueKey));
                issueKeys.add(newIssueKey);
            }
        }
    }

    private void makeDummyGitCommitWithMessage(final Git localGitRepo, final String commitMessage)
            throws GitAPIException {
        final String fileName = "test.txt";
        try {
            final File testFile = new File(localGitRepo.getRepository().getDirectory(), fileName);
            testFile.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException("Failed to create dummy file in temp git repo for commit.", e);
        }
        localGitRepo.add().addFilepattern(fileName).call();
        localGitRepo.commit().setMessage(commitMessage).call();
    }

    private BitbucketRepository createTestBitbucketRepo(final String repoNamePrefix) {
        final String repoNamePrefixWithRandom =
                (repoNamePrefix + RandomStringUtils.random(6, true, true)).toLowerCase();
        final String repoName = timestampNameTestResource().randomName(repoNamePrefixWithRandom, 30 * 60 * 1000);
        bitbucketRemoteClient().getRepositoriesRest().createGitRepository(repoName);
        return bitbucketRemoteClient().getRepositoriesRest().getRepository(dvcsRepoOwner(), repoName);
    }

    private Organization createDvcsConnectorOrganization(final String oauthKey, final String oauthSecret) {
        // Setup Org data
        final Organization newOrg = new Organization();
        newOrg.setAutolinkNewRepos(false);
        newOrg.setHostUrl(BitbucketDetails.getHostUrl());
        newOrg.setName(dvcsRepoOwner());
        newOrg.setDvcsType("bitbucket");
        newOrg.setSmartcommitsOnNewRepos(true);
        newOrg.setOrganizationUrl(BitbucketDetails.getHostUrl() + "/" + dvcsRepoOwner());

        // POST to DVCS Connector
        final Response<Organization> postResponse = dvcsConnectorRestClient().postOrganization(newOrg);
        assertEquals(postResponse.statusCode, 200);
        final Organization savedOrg = postResponse.body;
        assertNotNull(savedOrg);
        assertNotEquals(savedOrg.getId(), 0); // Make sure the ID was set and returned by the POST REST call

        // Update OAuth details for new Org in DVCS Connector
        final CredentialJson credentialRaw = new CredentialJson().setKey(oauthKey).setSecret(oauthSecret);
        final Response updateOAuthResponse = dvcsConnectorRestClient().updateOrganizationCredential(
                savedOrg.getId(), credentialRaw);
        assertEquals(updateOAuthResponse.statusCode, 200);
        return savedOrg;
    }

    private BitbucketRemoteClient bitbucketRemoteClient() {
        return bitbucketRemoteClient;
    }

    private DvcsConnectorRestClient dvcsConnectorRestClient() {
        return DVCS_CONNECTOR_REST_CLIENT;
    }

    private String dvcsRepoOwner() {
        return DVCS_REPO_OWNER;
    }

    private String dvcsRepoPassword() {
        return dvcsRepoPassword;
    }

    private Backdoor backdoor() {
        return BACKDOOR;
    }

    private TimestampNameTestResource timestampNameTestResource() {
        return TIMESTAMP_NAME_TEST_RESOURCE;
    }
}
