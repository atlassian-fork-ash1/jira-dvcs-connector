package it.com.atlassian.jira.plugins.dvcs.navlinks;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.plugins.navlink.consumer.menu.rest.MenuNavigationLinkEntity;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;

/**
 * A client for nav links operations.
 */
@ParametersAreNonnullByDefault
public class NavLinksClient extends RestApiClient<NavLinksClient> {

    private static final GenericType<List<MenuNavigationLinkEntity>> NAV_LINK_LIST =
            new GenericType<List<MenuNavigationLinkEntity>>() {};

    /**
     * Returns a client that connects to the given JIRA instance.
     *
     * @param jira the JIRA instance
     * @return see above
     */
    @Nonnull
    public static NavLinksClient forJira(final JiraTestedProduct jira) {
        return new NavLinksClient(jira.environmentData());
    }

    public NavLinksClient(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    @Override
    protected WebResource createResource() {
        return resourceRoot(getEnvironmentData().getBaseUrl().toExternalForm()).path("rest").path("menu").path("1.0");
    }

    /**
     * Returns the nav links exposed via the app switcher (a.k.a. the "hamburger menu").
     *
     * @return the same information as calls to the app links plugin's MenuResource#getMenuByKey resource for the "home"
     * and "custom-apps" keys, but with an order defined in the menuService across both sets
     * @see com.atlassian.plugins.navlink.consumer.menu.rest.MenuResource#getAppSwitcherMenu in the nav links plugin
     */
    @Nonnull
    public List<MenuNavigationLinkEntity> getAppSwitcherLinks() {
        return createResource().path("appswitcher").get(NAV_LINK_LIST);
    }
}
