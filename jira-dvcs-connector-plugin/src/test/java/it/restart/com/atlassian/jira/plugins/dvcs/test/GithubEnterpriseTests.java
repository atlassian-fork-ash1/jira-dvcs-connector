package it.restart.com.atlassian.jira.plugins.dvcs.test;

import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFileDetail;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.MagicVisitor;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.OAuth;
import com.atlassian.jira.plugins.dvcs.pageobjects.component.OrganizationDiv;
import com.atlassian.jira.plugins.dvcs.pageobjects.component.RepositoryDiv;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.AccountType;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.OAuthCredentials;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.Account;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.AccountRepository;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.DvcsAccountsPage;
import com.atlassian.jira.plugins.dvcs.pageobjects.remoterestpoint.ChangesetLocalRestpoint;
import it.com.atlassian.jira.plugins.dvcs.DvcsWebDriverTestCase;
import it.restart.com.atlassian.jira.plugins.dvcs.github.GithubLoginPage;
import it.restart.com.atlassian.jira.plugins.dvcs.github.GithubOAuthApplicationPage;
import it.restart.com.atlassian.jira.plugins.dvcs.github.GithubOAuthPage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static com.atlassian.jira.plugins.dvcs.model.ChangesetFileAction.ADDED;
import static com.atlassian.jira.plugins.dvcs.pageobjects.page.AccountType.getGHEAccountType;
import static com.atlassian.jira.plugins.dvcs.pageobjects.page.account.Account.AccountType.GIT_HUB_ENTERPRISE;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static it.restart.com.atlassian.jira.plugins.dvcs.test.GithubTestHelper.REPOSITORY_NAME;
import static it.restart.com.atlassian.jira.plugins.dvcs.test.IntegrationTestUserDetails.ACCOUNT_NAME;
import static it.util.TestAccounts.DVCS_CONNECTOR_TEST_ACCOUNT;
import static it.util.TestAccounts.JIRA_BB_CONNECTOR_ACCOUNT;
import static org.fest.assertions.api.Assertions.assertThat;

public class GithubEnterpriseTests extends DvcsWebDriverTestCase implements BasicTests {
    private static final String ATLASSIAN_TEAM_ACCOUNT = "atlassian";
    private static final String GITHUB_ENTERPRISE_URL = System.getProperty("githubenterprise.url", "http://172.26.26.78");
    private static final List<String> BASE_REPOSITORY_NAMES = Arrays.asList("missingcommits", "repo1", "test",
            "test-project", "noauthor");

    private OAuth oAuth;

    @BeforeClass
    public void beforeClass() {
        JIRA.quickLoginAsAdmin();

        // log in to github enterprise
        new MagicVisitor(JIRA).visit(GithubLoginPage.class, GITHUB_ENTERPRISE_URL).doLogin();

        // setup up OAuth from github
        oAuth = new MagicVisitor(JIRA).visit(GithubOAuthPage.class, GITHUB_ENTERPRISE_URL)
                .addConsumer(JIRA.getProductInstance().getBaseUrl());
    }

    @AfterClass
    public void afterClass() {
        // delete all organizations
        deleteAllOrganizations();
        // remove OAuth in github enterprise
        new MagicVisitor(JIRA).visit(GithubOAuthApplicationPage.class, GITHUB_ENTERPRISE_URL).removeConsumer(oAuth);
        // log out from github enterprise
        new MagicVisitor(JIRA).visit(GithubLoginPage.class, GITHUB_ENTERPRISE_URL).doLogout();
    }

    @BeforeMethod
    public void beforeMethod() {
        deleteAllOrganizations();
    }

    @Override
    @Test
    public void shouldBeAbleToSeePrivateRepositoriesFromTeamAccount() {
        // Invoke
        final Organization organization = addOrganization(getGHEAccountType(GITHUB_ENTERPRISE_URL),
                ATLASSIAN_TEAM_ACCOUNT, new OAuthCredentials(oAuth.key, oAuth.secret), false);

        // Check
        final List<String> repositoryNames = getRepositoryNamesByOrgId(organization.getId());
        assertThat(repositoryNames).contains("private-dvcs-connector-test");
    }

    @Test
    @Override
    public void addOrganization() {
        final Organization org = addOrganization(getGHEAccountType(GITHUB_ENTERPRISE_URL), ACCOUNT_NAME,
                new OAuthCredentials(oAuth.key, oAuth.secret), false);

        assertThat(org).isNotNull();
        assertThat(getRepositoryNamesByOrgId(org.getId())).containsAll(BASE_REPOSITORY_NAMES);
    }

    @Test
    @Override
    public void addOrganizationWaitForSync() {
        final Organization org = addOrganization(getGHEAccountType(GITHUB_ENTERPRISE_URL), ACCOUNT_NAME,
                new OAuthCredentials(oAuth.key, oAuth.secret), true);

        assertThat(org).isNotNull();
        assertThat(getRepositoryNamesByOrgId(org.getId())).containsAll(BASE_REPOSITORY_NAMES);

        final ChangesetLocalRestpoint changesetLocalRestpoint = new ChangesetLocalRestpoint();
        final List<String> commitsForQA2 = changesetLocalRestpoint.getCommitMessages("QA-2", 6);
        assertThat(commitsForQA2).contains("BB modified 1 file to QA-2 and QA-3 from TestRepo-QA");
        final List<String> commitsForQA3 = changesetLocalRestpoint.getCommitMessages("QA-3", 1);
        assertThat(commitsForQA3).contains("BB modified 1 file to QA-2 and QA-3 from TestRepo-QA");
    }

    @Test(expectedExceptions = AssertionError.class,
            expectedExceptionsMessageRegExp = ".*Error!\\n.*Error retrieving list of repositories.*")
    public void addOrganizationInvalidAccount() {
        addOrganization(getGHEAccountType(GITHUB_ENTERPRISE_URL), "I_AM_SURE_THIS_ACCOUNT_IS_INVALID",
                getOAuthCredentials(), false, true);
    }

    @Test(expectedExceptions = AssertionError.class, expectedExceptionsMessageRegExp =
            ".*Error!\\nThe url \\[https://nonexisting.org\\] is incorrect or the server is not responding.*")
    public void addOrganizationInvalidUrl() {
        addOrganization(getGHEAccountType(GITHUB_ENTERPRISE_URL), "https://nonexisting.org/someaccount",
                getOAuthCredentials(), false, true);
    }

    @Override
    @Test(expectedExceptions = AssertionError.class, expectedExceptionsMessageRegExp = "Invalid OAuth")
    public void addOrganizationInvalidOAuth() {
        addOrganization(getGHEAccountType(GITHUB_ENTERPRISE_URL), ACCOUNT_NAME,
                new OAuthCredentials("xxx", "yyy"), true, true);
    }

    @Test
    @Override
    public void testCommitStatistics() {
        final OrganizationDiv organizationDiv = addOrganizationOld(getGHEAccountType(GITHUB_ENTERPRISE_URL),
                ACCOUNT_NAME, new OAuthCredentials(oAuth.key, oAuth.secret), false);
        final RepositoryDiv repositoryDiv = organizationDiv.enableRepoAndGetRepoDiv(REPOSITORY_NAME);
        repositoryDiv.sync();

        // QA-3
        final List<Changeset> qa3Commits = getCommitsForIssue("QA-3");
        assertThat(qa3Commits).hasSize(1);
        final Changeset qa3Commit = qa3Commits.get(0);
        final List<ChangesetFileDetail> qa3Details = qa3Commit.getFileDetails();
        assert qa3Details != null;
        assertThat(qa3Details).hasSize(1);
        final ChangesetFileDetail qa3Detail = qa3Details.get(0);
        assertThat(qa3Detail.getAdditions()).isEqualTo(1);
        assertThat(qa3Detail.getDeletions()).isEqualTo(0);

        // QA-4
        final List<Changeset> qa4Commits = getCommitsForIssue("QA-4");
        assertThat(qa4Commits).hasSize(1);
        final Changeset qa4Commit = qa4Commits.get(0);
        final List<ChangesetFileDetail> qa4Details = qa4Commit.getFileDetails();
        assert qa4Details != null;
        assertThat(qa4Details).hasSize(1);
        final ChangesetFileDetail qa4Detail = qa4Details.get(0);
        assertThat(qa4Detail.getFileAction()).isEqualTo(ADDED);
    }

    private OAuthCredentials getOAuthCredentials() {
        return new OAuthCredentials(oAuth.key, oAuth.secret);
    }

    @Override
    @Test
    public void testPostCommitHookAddedAndRemoved() {
        AccountType gheAccountType = getGHEAccountType(GITHUB_ENTERPRISE_URL);
        testPostCommitHookAddedAndRemoved(JIRA_BB_CONNECTOR_ACCOUNT, gheAccountType, REPOSITORY_NAME, JIRA, getOAuthCredentials());
    }

    @Override
    protected boolean postCommitHookExists(final String accountName, final String jiraCallbackUrl) {
        List<String> actualHookUrls = GithubTestHelper.getHookUrls(accountName, GITHUB_ENTERPRISE_URL, REPOSITORY_NAME);
        return actualHookUrls.contains(jiraCallbackUrl);
    }

    @Test
    public void linkingRepositoryWithoutAdminPermission() {
        final AccountType accountType = getGHEAccountType(GITHUB_ENTERPRISE_URL);
        addOrganization(accountType, DVCS_CONNECTOR_TEST_ACCOUNT, getOAuthCredentials(), false);

        final DvcsAccountsPage accountsPage = JIRA.visit(DvcsAccountsPage.class);
        final Account account = accountsPage.getAccount(GIT_HUB_ENTERPRISE, DVCS_CONNECTOR_TEST_ACCOUNT);
        final AccountRepository repository = account.enableRepository("testemptyrepo", true);

        // check that repository is enabled
        waitUntilTrue(repository.isEnabled());
        waitUntilTrue(repository.hasWarning());
    }

    @Test
    public void linkingRepositoryWithAdminPermission() {
        final AccountType accountType = getGHEAccountType(GITHUB_ENTERPRISE_URL);
        addOrganization(accountType, ACCOUNT_NAME, getOAuthCredentials(), false);

        final DvcsAccountsPage accountsPage = JIRA.visit(DvcsAccountsPage.class);
        final Account account = accountsPage.getAccount(GIT_HUB_ENTERPRISE, ACCOUNT_NAME);
        final AccountRepository repository = account.enableRepository(REPOSITORY_NAME, false);

        // check that repository is enabled
        waitUntilTrue(repository.isEnabled());
        waitUntilFalse(repository.hasWarning());
    }

    @Test
    public void autoLinkingRepositoryWithoutAdminPermission() {
        final AccountType accountType = getGHEAccountType(GITHUB_ENTERPRISE_URL);
        addOrganization(accountType, DVCS_CONNECTOR_TEST_ACCOUNT, getOAuthCredentials(), false);

        final DvcsAccountsPage accountsPage = JIRA.visit(DvcsAccountsPage.class);
        final Account account = accountsPage.getAccount(GIT_HUB_ENTERPRISE, DVCS_CONNECTOR_TEST_ACCOUNT);

        final List<AccountRepository> repos = account.enableAllRepos();
        assertThat(repos.size()).isGreaterThan(0);

        for (final AccountRepository repository : repos) {
            waitUntilTrue(repository.isEnabled());
            waitUntilTrue(repository.hasWarning());
        }
    }

    @Test
    public void autoLinkingRepositoryWithAdminPermission() {
        final AccountType accountType = getGHEAccountType(GITHUB_ENTERPRISE_URL);
        addOrganization(accountType, ACCOUNT_NAME, getOAuthCredentials(), false);

        final DvcsAccountsPage accountsPage = JIRA.visit(DvcsAccountsPage.class);
        final Account account = accountsPage.getAccount(GIT_HUB_ENTERPRISE, ACCOUNT_NAME);

        final List<AccountRepository> repos = account.enableAllRepos();
        assertThat(repos.size()).isGreaterThan(0);

        for (final AccountRepository repository : repos) {
            waitUntilTrue(repository.isEnabled());
            waitUntilFalse(repository.hasWarning());
        }
    }
}
