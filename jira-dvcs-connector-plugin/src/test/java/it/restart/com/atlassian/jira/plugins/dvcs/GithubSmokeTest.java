package it.restart.com.atlassian.jira.plugins.dvcs;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.dvcs.github.api.model.GitHubRateLimit;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.MagicVisitor;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.OAuth;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.AccountType;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.OAuthCredentials;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.Account;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.AccountRepository;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.DvcsAccountsPage;
import com.atlassian.jira.plugins.dvcs.pageobjects.remoterestpoint.ChangesetLocalRestpoint;
import com.atlassian.pageobjects.TestedProductFactory;
import it.restart.com.atlassian.jira.plugins.dvcs.github.GithubLoginPage;
import it.restart.com.atlassian.jira.plugins.dvcs.test.DVCSTestHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;

import static it.restart.com.atlassian.jira.plugins.dvcs.test.GithubTestHelper.getRateLimit;
import static it.util.TestAccounts.JIRA_BB_CONNECTOR_ACCOUNT;
import static it.util.TestAccounts.JIRA_BB_CONNECTOR_GH_KEY;
import static it.util.TestAccounts.JIRA_BB_CONNECTOR_GH_SECRET;
import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.testng.FileAssert.fail;

/**
 * This test uses approved GH key & secret with base url localhost:2990 so in order to run this test locally you should set the following env variables
 * -Dbaseurl.jira=http://localhost:2990/jira -Dcontext.jira.path=jira    -Dhttp.jira.port=2990
 */
public class GithubSmokeTest {

    private static final String REPOSITORY = "test-project";
    private static final String EXPECTED_SYNC_MESSAGE = "Mon Feb 06 2012";

    private static final JiraTestedProduct JIRA = TestedProductFactory.create(JiraTestedProduct.class);
    private final static DVCSTestHelper helper = new DVCSTestHelper(JIRA.environmentData());
    private static final int TOTAL_GITHUB_REQUESTS_MADE_BY_THIS_TEST = 25;
    private static final Logger logger = LoggerFactory.getLogger(GithubSmokeTest.class);

    @BeforeClass
    public static void setup() {
        checkRateLimit();

        JIRA.quickLoginAsAdmin();
        new MagicVisitor(JIRA).visit(GithubLoginPage.class).doLogin();

        helper.clearAllOrganizations();
    }

    @AfterClass
    public static void cleanup() {
        helper.clearAllOrganizations();
    }

    @Test
    public void addOrganizationWaitForSync() {
        final DvcsAccountsPage accountsPage = JIRA.visit(DvcsAccountsPage.class);
        accountsPage.addGithubOrganisation(AccountType.GITHUB.type, JIRA_BB_CONNECTOR_ACCOUNT, AccountType.GITHUB.hostUrl, getOAuthCredentials(), false);

        final Optional<Account> mayBeAccount = accountsPage.getAccounts()
                .stream()
                .filter(account -> account.getName().equalsIgnoreCase(JIRA_BB_CONNECTOR_ACCOUNT))
                .findFirst();
        assertThat("Account " + JIRA_BB_CONNECTOR_ACCOUNT + " should exist", mayBeAccount.isPresent(), is(true));


        final Account account = mayBeAccount.get();
        final AccountRepository repository = account.enableRepository(REPOSITORY);

        assertThat("Repository " + REPOSITORY + " should exist", repository, notNullValue());
        repository.synchronize();

        assertThat(repository.getMessage().byDefaultTimeout(), equalToIgnoringCase(EXPECTED_SYNC_MESSAGE));

        final ChangesetLocalRestpoint changesetLocalRestpoint = new ChangesetLocalRestpoint();

        final List<String> commitsForQA2 = changesetLocalRestpoint.getCommitMessages("QA-2", 6);
        assertThat(commitsForQA2, hasItem("BB modified 1 file to QA-2 and QA-3 from TestRepo-QA"));

        final List<String> commitsForQA3 = changesetLocalRestpoint.getCommitMessages("QA-3", 1);
        assertThat(commitsForQA3, hasItem("BB modified 1 file to QA-2 and QA-3 from TestRepo-QA"));
    }

    private static void checkRateLimit() {
        final GitHubRateLimit rateLimit = getRateLimit(JIRA_BB_CONNECTOR_ACCOUNT);
        if (rateLimit.getRequestsLeft() < TOTAL_GITHUB_REQUESTS_MADE_BY_THIS_TEST) {
            final long minutesToWait = (rateLimit.getSecondsLeft() / 60) + 1;
            fail(format("This test will exceed our GitHub rate limit. Try again in %d minute(s)", minutesToWait));
        }
        logger.debug("Before class: {}", rateLimit);
    }

    private OAuthCredentials getOAuthCredentials() {
        final OAuth oAuth = new OAuth(JIRA_BB_CONNECTOR_GH_KEY, JIRA_BB_CONNECTOR_GH_SECRET, JIRA.getProductInstance().getBaseUrl());
        return new OAuthCredentials(oAuth.key, oAuth.secret);
    }
}
