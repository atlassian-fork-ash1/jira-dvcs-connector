package com.atlassian.jira.plugins.dvcs.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationMapping;
import com.atlassian.jira.plugins.dvcs.crypto.Encryptor;
import com.atlassian.jira.plugins.dvcs.dao.OrganizationAOFacade;
import com.atlassian.jira.plugins.dvcs.model.Group;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.BasicAuthCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.Credential;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialVisitor;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.ThreeLeggedOAuthCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.TwoLeggedOAuthCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.UnauthenticatedCredential;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import net.java.ao.Query;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Named("organizationAOFacade")
public class OrganizationAOFacadeImpl implements OrganizationAOFacade {
    public static final Logger log = LoggerFactory.getLogger(OrganizationAOFacadeImpl.class);

    private final ActiveObjects activeObjects;
    private final Encryptor encryptor;

    @Inject
    public OrganizationAOFacadeImpl(
            @ComponentImport @Nonnull final ActiveObjects activeObjects,
            @Nonnull final Encryptor encryptor) {
        this.activeObjects = checkNotNull(activeObjects);
        this.encryptor = checkNotNull(encryptor);
    }

    /**
     * Map a retrieved {@link OrganizationMapping} into the domain model {@link Organization}
     *
     * @param mapping the organization mapping
     * @return the organization
     */
    @VisibleForTesting
    Organization transform(@Nullable final OrganizationMapping mapping) {
        if (mapping == null) {
            return null;
        }

        final Credential credential = CredentialFactory.buildCredential()
                .setKey(mapping.getOauthKey())
                .setSecret(mapping.getOauthSecret())
                .setToken(mapping.getAccessToken())
                .setUsername(mapping.getAdminUsername())
                .setPassword(mapping.getAdminPassword())
                .setPrincipalId(mapping.getPrincipalId())
                .build();

        final Organization result = new Organization(
                mapping.getID(),
                mapping.getHostUrl(),
                mapping.getName(),
                mapping.getDvcsType(),
                mapping.isAutolinkNewRepos(),
                credential,
                createOrganizationUrl(mapping),
                mapping.isSmartcommitsForNewRepos(),
                deserializeDefaultGroups(mapping.getDefaultGroupsSlugs()));

        if (isNotBlank(mapping.getApprovalState())) {
            result.setApprovalState(Organization.ApprovalState.valueOf(mapping.getApprovalState()));
        }

        return result;
    }

    protected Set<Group> deserializeDefaultGroups(final String defaultGroupsSlugs) {
        final Set<Group> slugs = new HashSet<>();
        if (isNotBlank(defaultGroupsSlugs)) {
            Iterable<String> groupsSlugs = Splitter.on(Organization.GROUP_SLUGS_SEPARATOR).split(defaultGroupsSlugs);
            for (String slug : groupsSlugs) {
                if (isNotBlank(slug)) {
                    slugs.add(new Group(StringUtils.trim(slug)));
                }
            }
        }
        return slugs;
    }

    protected String serializeDefaultGroups(final Set<Group> groups) {
        if (CollectionUtils.isEmpty(groups)) {
            return "";
        }
        return Joiner.on(Organization.GROUP_SLUGS_SEPARATOR).join(groups);
    }

    private String createOrganizationUrl(final OrganizationMapping organizationMapping) {
        String hostUrl = organizationMapping.getHostUrl();
        // normalize
        if (hostUrl != null && hostUrl.endsWith("/")) {
            hostUrl = hostUrl.substring(0, hostUrl.length() - 1);
        }
        return hostUrl + "/" + organizationMapping.getName();
    }

    @Override
    public List<Organization> fetch() {
        final List<OrganizationMapping> organizationMappings = activeObjects.executeInTransaction(
                () -> newArrayList(activeObjects.find(OrganizationMapping.class, Query.select().order(OrganizationMapping.NAME))));

        return transformCollection(organizationMappings);
    }

    /**
     * Transform collection.
     *
     * @param organizationMappings the organization mappings
     * @return the list< organization>
     */
    @SuppressWarnings("unchecked")
    private List<Organization> transformCollection(final List<OrganizationMapping> organizationMappings) {
        return (List<Organization>) CollectionUtils.collect(organizationMappings, input -> {
            OrganizationMapping organizationMapping = (OrganizationMapping) input;

            return OrganizationAOFacadeImpl.this.transform(organizationMapping);
        });
    }

    @Override
    public Organization save(final Organization organization) {
        final OrganizationMapping organizationMapping = activeObjects.executeInTransaction(() ->
        {
            OrganizationMapping om = null;

            if (organization.getId() == 0) {
                // we need to remove null characters '\u0000' because PostgreSQL cannot store String values
                // with such characters
                final Map<String, Object> map = new MapRemovingNullCharacterFromStringValues();
                map.put(OrganizationMapping.HOST_URL, organization.getHostUrl());
                map.put(OrganizationMapping.NAME, organization.getName());
                map.put(OrganizationMapping.DVCS_TYPE, organization.getDvcsType());
                map.put(OrganizationMapping.AUTOLINK_NEW_REPOS, organization.isAutolinkNewRepos());
                map.put(OrganizationMapping.SMARTCOMMITS_FOR_NEW_REPOS, organization.isSmartcommitsOnNewRepos());
                map.put(OrganizationMapping.DEFAULT_GROUPS_SLUGS, serializeDefaultGroups(organization.getDefaultGroups()));
                map.put(OrganizationMapping.APPROVAL_STATE, organization.getApprovalState().name());
                map.putAll(organization.getCredential().accept(new CredentialDBFieldVisitor(organization, encryptor)));

                om = activeObjects.create(OrganizationMapping.class, map);
                om = activeObjects.find(OrganizationMapping.class, "ID = ?", om.getID())[0];
            } else {
                om = activeObjects.get(OrganizationMapping.class, organization.getId());

                om.setHostUrl(organization.getHostUrl());
                om.setName(organization.getName());
                om.setDvcsType(organization.getDvcsType());
                om.setAutolinkNewRepos(organization.isAutolinkNewRepos());
                om.setSmartcommitsForNewRepos(organization.isSmartcommitsOnNewRepos());
                om.setDefaultGroupsSlugs(serializeDefaultGroups(organization.getDefaultGroups()));
                om.setApprovalState(organization.getApprovalState().name());

                organization.getCredential().accept(new OrganizationPopulatorVisitor(om));

                om.save();
            }

            return om;
        });

        return transform(organizationMapping);
    }

    @Override
    public void updateDefaultGroupsSlugs(final int orgId, final Collection<String> groupsSlugs) {
        String serializedGroups = null;
        if (CollectionUtils.isNotEmpty(groupsSlugs)) {
            serializedGroups = Joiner.on(Organization.GROUP_SLUGS_SEPARATOR).join(groupsSlugs);
        }

        final OrganizationMapping organization = activeObjects.get(OrganizationMapping.class, orgId);
        organization.setDefaultGroupsSlugs(serializedGroups);
        activeObjects.executeInTransaction(() -> {
            organization.save();
            return null;
        });
    }

    private static class CredentialDBFieldVisitor implements CredentialVisitor<Map<String, Object>> {
        private final Organization organization;
        private final Map<String, Object> result;
        private final Encryptor encryptor;

        private CredentialDBFieldVisitor(final Organization organization, final Encryptor encryptor) {
            this.organization = organization;
            this.result = new HashMap<>();
            this.encryptor = encryptor;
        }

        @Override
        public Map<String, Object> visit(final BasicAuthCredential credential) {
            final String encryptedPassword = encryptor.encrypt(credential.getPassword(), organization.getName(), organization.getHostUrl());
            result.put(OrganizationMapping.ADMIN_PASSWORD, encryptedPassword);
            result.put(OrganizationMapping.ADMIN_USERNAME, credential.getUsername());
            return result;
        }

        @Override
        public Map<String, Object> visit(final TwoLeggedOAuthCredential credential) {
            result.put(OrganizationMapping.OAUTH_KEY, credential.getKey());
            result.put(OrganizationMapping.OAUTH_SECRET, credential.getSecret());
            return result;
        }

        @Override
        public Map<String, Object> visit(final ThreeLeggedOAuthCredential credential) {
            result.put(OrganizationMapping.OAUTH_KEY, credential.getKey());
            result.put(OrganizationMapping.OAUTH_SECRET, credential.getSecret());
            result.put(OrganizationMapping.ACCESS_TOKEN, credential.getAccessToken());
            return result;
        }

        @Override
        public Map<String, Object> visit(final PrincipalIDCredential credential) {
            result.put(OrganizationMapping.PRINCIPAL_ID, credential.getPrincipalId());
            return result;
        }

        @Override
        public Map<String, Object> visit(final UnauthenticatedCredential credential) {
            result.put(OrganizationMapping.ADMIN_PASSWORD, null);
            result.put(OrganizationMapping.ADMIN_USERNAME, null);
            result.put(OrganizationMapping.OAUTH_KEY, null);
            result.put(OrganizationMapping.OAUTH_SECRET, null);
            result.put(OrganizationMapping.ACCESS_TOKEN, null);
            result.put(OrganizationMapping.PRINCIPAL_ID, null);
            return result;
        }
    }

    private static class OrganizationPopulatorVisitor implements CredentialVisitor<Unit> {
        private final OrganizationMapping organizationMapping;

        private OrganizationPopulatorVisitor(final OrganizationMapping organizationMapping) {
            this.organizationMapping = organizationMapping;
            organizationMapping.setAdminPassword(null);
            organizationMapping.setAdminUsername(null);
            organizationMapping.setOauthKey(null);
            organizationMapping.setOauthSecret(null);
            organizationMapping.setAccessToken(null);
            organizationMapping.setPrincipalId(null);
        }

        @Override
        public Unit visit(final BasicAuthCredential credential) {
            organizationMapping.setAdminPassword(credential.getPassword());
            organizationMapping.setAdminUsername(credential.getUsername());
            return Unit.VALUE;
        }

        @Override
        public Unit visit(final PrincipalIDCredential credential) {
            organizationMapping.setPrincipalId(credential.getPrincipalId());
            return Unit.VALUE;
        }

        @Override
        public Unit visit(final TwoLeggedOAuthCredential credential) {
            organizationMapping.setOauthKey(credential.getKey());
            organizationMapping.setOauthSecret(credential.getSecret());
            return Unit.VALUE;
        }

        @Override
        public Unit visit(final ThreeLeggedOAuthCredential credential) {
            organizationMapping.setOauthKey(credential.getKey());
            organizationMapping.setOauthSecret(credential.getSecret());
            organizationMapping.setAccessToken(credential.getAccessToken());
            return Unit.VALUE;
        }

        @Override
        public Unit visit(final UnauthenticatedCredential credential) {
            return Unit.VALUE;
        }
    }
}
