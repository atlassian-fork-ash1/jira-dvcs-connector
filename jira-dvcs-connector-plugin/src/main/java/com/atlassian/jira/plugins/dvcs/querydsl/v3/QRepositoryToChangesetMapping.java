package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;

public class QRepositoryToChangesetMapping extends EnhancedRelationalPathBase<QRepositoryToChangesetMapping> {

    private static final String AO_TABLE_NAME = "AO_E8B6CC_REPO_TO_CHANGESET";
    private static final long serialVersionUID = 7302451420232752022L;

    public final NumberPath<Integer> ID = createNumber("ID", Integer.class);
    public final NumberPath<Integer> CHANGESET_ID = createNumber("CHANGESET_ID", Integer.class);
    public final NumberPath<Integer> REPOSITORY_ID = createNumber("REPOSITORY_ID", Integer.class);
    public final com.querydsl.sql.PrimaryKey<QRepositoryToChangesetMapping> REPOTOCHANGESET_PK = createPrimaryKey(ID);

    public QRepositoryToChangesetMapping() {
        super(QRepositoryToChangesetMapping.class, AO_TABLE_NAME);
    }
}