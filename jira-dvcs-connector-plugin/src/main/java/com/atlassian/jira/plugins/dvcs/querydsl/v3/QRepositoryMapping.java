package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

public class QRepositoryMapping extends EnhancedRelationalPathBase<QRepositoryMapping> {

    private static final String AO_TABLE_NAME = "AO_E8B6CC_REPOSITORY_MAPPING";
    private static final long serialVersionUID = -8689212089126346592L;

    public final StringPath FORK_OF_NAME = createString("FORK_OF_NAME");
    public final StringPath FORK_OF_OWNER = createString("FORK_OF_OWNER");
    public final StringPath FORK_OF_SLUG = createString("FORK_OF_SLUG");
    public final NumberPath<Integer> ID = createInteger("ID");
    public final StringPath LAST_CHANGESET_NODE = createString("LAST_CHANGESET_NODE");
    public final StringPath LOGO = createString("LOGO");
    public final StringPath NAME = createString("NAME");
    public final NumberPath<Integer> ORGANIZATION_ID = createInteger("ORGANIZATION_ID");
    public final StringPath SLUG = createString("SLUG");
    public final BooleanPath DELETED = createBoolean("DELETED");
    public final BooleanPath LINKED = createBoolean("LINKED");

    public final com.querydsl.sql.PrimaryKey<QRepositoryMapping> REPOSITORYMAPPING_PK = createPrimaryKey(ID);

    public QRepositoryMapping() {
        super(QRepositoryMapping.class, AO_TABLE_NAME);
    }

}