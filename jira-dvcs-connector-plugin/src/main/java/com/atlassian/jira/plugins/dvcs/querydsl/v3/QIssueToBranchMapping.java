package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.querydsl.core.types.dsl.StringPath;

public class QIssueToBranchMapping extends EnhancedRelationalPathBase<QIssueToBranchMapping> implements IssueKeyedMapping {
    private static final long serialVersionUID = 5669083607445739498L;

    private static final String AO_TABLE_NAME = "AO_E8B6CC_ISSUE_TO_BRANCH";

    public final NumberPath<Integer> ID = createInteger("ID");
    public final StringPath ISSUE_KEY = createString("ISSUE_KEY");
    public final NumberPath<Integer> BRANCH_ID = createInteger("BRANCH_ID");
    public final com.querydsl.sql.PrimaryKey<QIssueToBranchMapping> ISSUETOBRANCH_PK = createPrimaryKey(ID);

    public QIssueToBranchMapping() {
        super(QIssueToBranchMapping.class, AO_TABLE_NAME);
    }

    @Override
    public SimpleExpression getIssueKeyExpression() {
        return ISSUE_KEY;
    }
}