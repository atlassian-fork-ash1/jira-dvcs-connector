package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.dao.BranchDao;
import com.atlassian.jira.plugins.dvcs.dao.impl.BranchDaoImpl;
import com.atlassian.jira.plugins.dvcs.dao.impl.QueryDslFeatureHelper;
import com.atlassian.jira.plugins.dvcs.model.Branch;
import com.atlassian.jira.plugins.dvcs.model.BranchHead;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QBranchMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QIssueToBranchMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QOrganizationMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QRepositoryMapping;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Path;
import com.querydsl.sql.SQLExpressions;
import io.atlassian.fugue.Unit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.jira.plugins.dvcs.dao.impl.DAOConstants.MAXIMUM_ENTITIES_PER_ISSUE_KEY;
import static com.atlassian.jira.plugins.dvcs.dao.impl.querydsl.DvcsTypeBooleanConditionFactory.getOrgDvcsTypeCondition;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.querydsl.sql.SQLExpressions.countDistinct;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

/**
 * An implementation of {@link com.atlassian.jira.plugins.dvcs.dao.BranchDao} that has a delegate {@link
 * com.atlassian.jira.plugins.dvcs.dao.impl.BranchDaoImpl} and uses Query DSL for #getBranchesForIssue if the dark
 * feature is set.
 */
@SuppressWarnings("SpringJavaAutowiringInspection")
@Component("branchDaoQueryDsl")
public class BranchDaoQueryDsl implements BranchDao {

    private static final String ISSUE_COUNT = "issue_count";
    private static final String ISSUE_KEY = "ISSUE_KEY";
    private static Path<String> ISSUE_KEY_PATH = ExpressionUtils.path(String.class, ISSUE_KEY);
    private static Path<Long> ISSUE_COUNT_PATH = ExpressionUtils.path(Long.class, ISSUE_COUNT);
    private static final int PAGE_SIZE = 1000;
    private final BranchDaoImpl branchDao;
    private final QueryDslFeatureHelper queryDslFeatureHelper;
    private final DatabaseAccessor databaseAccessor;

    @Autowired
    public BranchDaoQueryDsl(@Nonnull final BranchDaoImpl branchDao,
                             @Nonnull DatabaseAccessor databaseAccessor,
                             @Nonnull final QueryDslFeatureHelper queryDslFeatureHelper) {
        this.databaseAccessor = checkNotNull(databaseAccessor);
        this.branchDao = checkNotNull(branchDao);
        this.queryDslFeatureHelper = checkNotNull(queryDslFeatureHelper);
    }

    @Override
    public void createBranchHead(final int repositoryId, final BranchHead branchHead) {
        branchDao.createBranchHead(repositoryId, branchHead);
    }

    @Override
    public List<BranchHead> getBranchHeads(final int repositoryId) {
        return branchDao.getBranchHeads(repositoryId);
    }

    @Override
    public void removeBranchHead(final int repositoryId, final BranchHead branchHead) {
        branchDao.removeBranchHead(repositoryId, branchHead);
    }

    @Override
    public void removeAllBranchHeadsInRepository(final int repositoryId) {
        branchDao.removeAllBranchHeadsInRepository(repositoryId);
    }

    @Override
    public List<Branch> getBranchesForIssue(final Iterable<String> issueKeys) {
        return branchDao.getBranchesForIssue(issueKeys);
    }

    @Override
    public List<Branch> getBranches(final int repositoryId) {
        return branchDao.getBranches(repositoryId);
    }

    @Override
    public void createBranch(final int repositoryId, final Branch branch, final Set<String> issueKeys) {
        branchDao.createBranch(repositoryId, branch, issueKeys);
    }

    @Override
    public void removeBranch(final int repositoryId, final Branch branch) {
        databaseAccessor.runInTransaction(connection -> {
            QBranchMapping branchMapping = new QBranchMapping();
            QIssueToBranchMapping issueMapping = new QIssueToBranchMapping();

            connection.delete(issueMapping)
                    .where(SQLExpressions.select(branchMapping.ID).from(branchMapping).where(issueMapping.BRANCH_ID.eq(branchMapping.ID)
                            .and(branchMapping.REPOSITORY_ID.eq(repositoryId))
                            .and(branchMapping.NAME.eq(branch.getName()))).exists())
                    .execute();

            return connection.delete(branchMapping)
                    .where(branchMapping.REPOSITORY_ID.eq(repositoryId)
                            .and(branchMapping.NAME.eq(branch.getName())))
                    .execute();
        });
    }

    @Override
    public void removeAllBranchesInRepository(final int repositoryId) {
        databaseAccessor.runInTransaction(connection -> {
            QIssueToBranchMapping issueMapping = new QIssueToBranchMapping();
            QBranchMapping branchMapping = new QBranchMapping();

            connection.delete(issueMapping)
                    .where(SQLExpressions.select(branchMapping.ID).from(branchMapping)
                            .where(issueMapping.BRANCH_ID.eq(branchMapping.ID)
                                    .and(branchMapping.REPOSITORY_ID.eq(repositoryId)))
                            .exists())
                    .execute();

            return connection.delete(branchMapping)
                    .where(branchMapping.REPOSITORY_ID.eq(repositoryId))
                    .execute();
        });
    }

    @Override
    public List<Branch> getBranchesForRepository(final int repositoryId) {
        return branchDao.getBranchesForRepository(repositoryId);
    }

    /**
     * Retrieve branches associated with the supplied issue keys and dvcsType, if the Query DSL dark feature is enabled
     * it /* will use Query DSL otherwise it will use the delegate.
     *
     * @param issueKeys The issue keys to query by
     * @param dvcsType  The optional dvcsType to restrict to
     * @return Branches associated with the supplied issue keys and dvcsType
     */
    @Nonnull
    @Override
    public List<Branch> getBranchesForIssue(@Nonnull final Iterable<String> issueKeys, @Nullable final String dvcsType) {
        if (Iterables.isEmpty(issueKeys)) {
            return Collections.emptyList();
        }

        if (queryDslFeatureHelper.isRetrievalUsingQueryDslDisabled()) {
            return branchDao.getBranchesForIssue(issueKeys, dvcsType);
        }

        final Tables tables = fetchAndInitTables();

        final List<Tuple> branchTuples = getBranches(issueKeys, dvcsType, tables);

        final Map<Integer, Branch> branches = branchTuples.stream().map(tuple ->
                new Branch(tuple.get(tables.getBranchMapping().ID),
                        tuple.get(tables.getBranchMapping().NAME),
                        tuple.get(tables.getBranchMapping().REPOSITORY_ID),
                        ImmutableList.of(tuple.get(ISSUE_KEY_PATH))))
                .collect(toMap(Branch::getId, identity()));

        final List<Integer> branchIdsWithMoreCommits = branchTuples.stream()
                .filter(t -> t.get(ISSUE_COUNT_PATH) != 1)
                .map(t -> t.get(tables.getBranchMapping().ID))
                .collect(Collectors.toList());

        if (branchIdsWithMoreCommits.size() == 0) {
            return ImmutableList.copyOf(branches.values());
        } else {
            final Map<Integer, List<String>> issueKeysForID = getAdditionalIssueKeys(branchIdsWithMoreCommits, tables)
                    .stream()
                    .collect(groupingBy(tuple -> tuple.get(tables.getIssueMapping().BRANCH_ID),
                            mapping(tuple -> tuple.get(tables.getIssueMapping().ISSUE_KEY), toList())));

            return addIssuesToBranches(branches, issueKeysForID);
        }
    }

    private Tables fetchAndInitTables() {
        Tables tables = new Tables();
        databaseAccessor.run(connection -> {
                    tables.initialize(connection);
                    return Unit.Unit();
                }
        );
        return tables;
    }

    private List<Branch> addIssuesToBranches(Map<Integer, Branch> branchesById, Map<Integer, List<String>> issuesForBranches) {
        return branchesById.keySet().stream()
                .map(branchId -> {
                    Set<String> issueKeys = new HashSet<>();
                    issueKeys.addAll(branchesById.get(branchId).getIssueKeys());
                    issueKeys.addAll(issuesForBranches.get(branchId));
                    branchesById.get(branchId).setIssueKeys(ImmutableList.copyOf(issueKeys));
                    return branchesById.get(branchId);
                }).collect(Collectors.toList());
    }

    private List<Tuple> getBranches(final Iterable<String> issueKeys, final String dvcsType, final Tables tables) {
        return databaseAccessor.run(connection -> connection.select(
                tables.getBranchMapping().ID,
                tables.getBranchMapping().NAME,
                tables.getBranchMapping().REPOSITORY_ID,
                tables.getIssueMapping().ISSUE_KEY.max().as(ISSUE_KEY_PATH),
                countDistinct((tables.getIssueMapping().ISSUE_KEY)).as(ISSUE_COUNT_PATH))
                .from(tables.getBranchMapping())
                .join(tables.getIssueMapping()).on(tables.getBranchMapping().ID.eq(tables.getIssueMapping().BRANCH_ID))
                .join(tables.getRepositoryMapping()).on(tables.getRepositoryMapping().ID.eq(tables.getBranchMapping().REPOSITORY_ID))
                .join(tables.getOrgMapping()).on(tables.getOrgMapping().ID.eq(tables.getRepositoryMapping().ORGANIZATION_ID))
                .where(tables.getRepositoryMapping().DELETED.eq(false)
                        .and(tables.getRepositoryMapping().LINKED.eq(true))
                        .and(IssueKeyPredicateFactory.buildIssueKeyPredicate(issueKeys, tables.getIssueMapping()))
                        .and(getOrgDvcsTypeCondition(dvcsType)))
                .groupBy(tables.getBranchMapping().ID, tables.getBranchMapping().NAME, tables.getBranchMapping().REPOSITORY_ID)
                .orderBy(tables.getBranchMapping().NAME.asc())
                .limit(MAXIMUM_ENTITIES_PER_ISSUE_KEY)
                .fetch());
    }

    private List<Tuple> getAdditionalIssueKeys(final List<Integer> branchIds, final Tables tables) {
        return databaseAccessor.run(connection -> connection.select(
                tables.getIssueMapping().BRANCH_ID,
                tables.getIssueMapping().ISSUE_KEY)
                .from(tables.getIssueMapping()).where(tables.getIssueMapping().BRANCH_ID.in(branchIds))
                .orderBy(tables.getIssueMapping().BRANCH_ID.asc())
                .fetch());
    }

    private class Tables {
        private QBranchMapping branchMapping;
        private QIssueToBranchMapping issueMapping;
        private QRepositoryMapping repositoryMapping;
        private QOrganizationMapping orgMapping;
        private boolean initialized = false;

        public void initialize(DatabaseConnection connection) {
            branchMapping = new QBranchMapping();
            issueMapping = new QIssueToBranchMapping();
            repositoryMapping = new QRepositoryMapping();
            orgMapping = new QOrganizationMapping();
            initialized = true;
        }

        public QBranchMapping getBranchMapping() {
            checkIsInitialized();
            return branchMapping;
        }

        public QIssueToBranchMapping getIssueMapping() {
            checkIsInitialized();
            return issueMapping;
        }

        public QRepositoryMapping getRepositoryMapping() {
            checkIsInitialized();
            return repositoryMapping;
        }

        public QOrganizationMapping getOrgMapping() {
            checkIsInitialized();
            return orgMapping;
        }

        private void checkIsInitialized() {
            if (!initialized) {
                throw new IllegalStateException("A call to initialize must be made before " +
                        "attempting to access the qBeans!");
            }
        }
    }

}
