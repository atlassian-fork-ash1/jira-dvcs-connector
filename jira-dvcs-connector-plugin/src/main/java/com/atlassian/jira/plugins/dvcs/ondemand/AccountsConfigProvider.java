package com.atlassian.jira.plugins.dvcs.ondemand;

import javax.annotation.Nullable;

public interface AccountsConfigProvider {
    /**
     * Provides the accounts configuration.
     *
     * @return <code>null</code> if the configuration cannot be obtained
     */
    @Nullable
    AccountsConfig provideConfiguration();

    /**
     * Indicates whether this provider supports integrated accounts.
     *
     * @return see above
     */
    boolean supportsIntegratedAccounts();
}

