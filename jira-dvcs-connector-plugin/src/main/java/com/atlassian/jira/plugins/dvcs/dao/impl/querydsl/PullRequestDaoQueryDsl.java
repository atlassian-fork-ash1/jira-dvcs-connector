package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.dao.impl.transform.PullRequestTransformer;
import com.atlassian.jira.plugins.dvcs.model.Participant;
import com.atlassian.jira.plugins.dvcs.model.PullRequest;
import com.atlassian.jira.plugins.dvcs.model.PullRequestRef;
import com.atlassian.jira.plugins.dvcs.model.PullRequestStatus;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QOrganizationMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QPullRequestParticipantMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QRepositoryMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QRepositoryPullRequestIssueKeyMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QRepositoryPullRequestMapping;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.google.common.collect.Iterables;
import com.querydsl.core.Tuple;
import com.querydsl.sql.SQLExpressions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static com.atlassian.jira.plugins.dvcs.dao.impl.DAOConstants.MAXIMUM_ENTITIES_PER_ISSUE_KEY;
import static com.atlassian.jira.plugins.dvcs.dao.impl.querydsl.DvcsTypeBooleanConditionFactory.getOrgDvcsTypeCondition;
import static com.atlassian.jira.plugins.dvcs.dao.impl.transform.PullRequestTransformer.createRepositoryLabel;
import static com.atlassian.jira.plugins.dvcs.dao.impl.transform.RepositoryTransformer.createRepositoryUrl;
import static com.google.common.base.Preconditions.checkNotNull;
import static io.atlassian.fugue.Unit.Unit;
import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

/**
 * Retrieves Pull Requests using Query DSL and then maps them directly to API entities, does not implement {@link
 * com.atlassian.jira.plugins.dvcs.activity.RepositoryPullRequestDao} because we need to return the entity used in
 * {@link com.atlassian.jira.plugins.dvcs.service.PullRequestService#getByIssueKeys(Iterable, String)}.
 */
@SuppressWarnings("SpringJavaAutowiringInspection")
@Component
public class PullRequestDaoQueryDsl {

    private final DatabaseAccessor databaseAccessor;

    @Autowired
    public PullRequestDaoQueryDsl(final DatabaseAccessor databaseAccessor) {
        this.databaseAccessor = databaseAccessor;
    }

    /**
     * Retrieve up to {@link com.atlassian.jira.plugins.dvcs.dao.impl.DAOConstants#MAXIMUM_ENTITIES_PER_ISSUE_KEY} Pull
     * Requests based on the supplied issue keys
     *
     * @param issueKeys The issue keys that are associated with the pull request
     * @param dvcsType  The (optional) dvcs type that we are restricting to
     * @return Pull requests associated with the issue key
     */
    @Nonnull
    public List<PullRequest> getByIssueKeys(@Nonnull final Iterable<String> issueKeys, @Nullable final String dvcsType) {
        checkNotNull(issueKeys);
        if (Iterables.isEmpty(issueKeys)) {
            return emptyList();
        }

        final Tables tables = getAndInitTables();

        final List<Tuple> rawPullRequests = getPullRequests(issueKeys, dvcsType, tables);
        final List<PullRequest> pullRequests = transformToPullRequest(rawPullRequests, tables);

        if (!pullRequests.isEmpty()) {
            final List<Tuple> pullRequestsData = getPullRequestsData(issueKeys, dvcsType, tables,
                    pullRequests.get(pullRequests.size() - 1).getCreatedOn(),
                    pullRequests.get(0).getCreatedOn());

            return populateAndFilterPullrequests(tables, pullRequests, pullRequestsData);
        }

        return pullRequests;
    }

    private Tables getAndInitTables() {
        final Tables tables = new Tables();
        databaseAccessor.runInTransaction(connection -> {
            tables.inititalize();
            return Unit();
        });
        return tables;
    }

    private List<Tuple> getPullRequests(@Nonnull final Iterable<String> issueKeys,
                                        @Nullable final String dvcsType,
                                        @Nonnull final Tables tables) {

        return databaseAccessor.<List<Tuple>>runInTransaction(connection -> {
            final QRepositoryPullRequestMapping pullRequest = tables.getPrMapping();
            final QRepositoryPullRequestMapping innerPullRequest = new QRepositoryPullRequestMapping();

            final QRepositoryMapping repository = tables.getRepositoryMapping();
            final QRepositoryMapping innerRepository = new QRepositoryMapping();

            final QOrganizationMapping org = tables.getOrgMapping();
            final QOrganizationMapping innerOrg = new QOrganizationMapping();

            return connection.select(
                    pullRequest.ID,
                    pullRequest.TO_REPOSITORY_ID,
                    pullRequest.REMOTE_ID,
                    pullRequest.EXECUTED_BY,
                    pullRequest.NAME,
                    pullRequest.URL,
                    pullRequest.LAST_STATUS,
                    pullRequest.CREATED_ON,
                    pullRequest.UPDATED_ON,
                    pullRequest.AUTHOR,
                    pullRequest.COMMENT_COUNT,
                    pullRequest.SOURCE_BRANCH,
                    pullRequest.SOURCE_REPO,
                    org.HOST_URL,
                    pullRequest.DESTINATION_BRANCH,
                    org.NAME,
                    repository.SLUG)
                    .from(pullRequest)
                    .join(repository).on(repository.ID.eq(pullRequest.TO_REPOSITORY_ID))
                    .join(org).on(org.ID.eq(repository.ORGANIZATION_ID))
                    .where(pullRequest.ID.in(SQLExpressions.select(innerPullRequest.ID)
                            .from(innerPullRequest)
                            .join(tables.getIssueMapping()).on(innerPullRequest.ID.eq(tables.getIssueMapping().PULL_REQUEST_ID))
                            .join(innerRepository).on(innerRepository.ID.eq(innerPullRequest.TO_REPOSITORY_ID))
                            .join(innerOrg).on(innerOrg.ID.eq(innerRepository.ORGANIZATION_ID))
                            .where(innerRepository.DELETED.eq(false)
                                    .and(innerRepository.LINKED.eq(true))
                                    .and(IssueKeyPredicateFactory.buildIssueKeyPredicate(issueKeys, tables.getIssueMapping()))
                                    .and(getOrgDvcsTypeCondition(dvcsType))
                            ))
                    ).orderBy(pullRequest.CREATED_ON.desc())
                    .limit(MAXIMUM_ENTITIES_PER_ISSUE_KEY)
                    .fetch();
        });
    }

    private List<Tuple> getPullRequestsData(@Nonnull final Iterable<String> issueKeys,
                                            @Nullable final String dvcsType,
                                            @Nonnull final Tables tables,
                                            @Nonnull final Date minDate,
                                            @Nonnull final Date maxDate) {

        return databaseAccessor.<List<Tuple>>runInTransaction(connection -> {
            final QRepositoryPullRequestMapping pullRequest = tables.getPrMapping();
            final QRepositoryMapping repository = tables.getRepositoryMapping();
            final QOrganizationMapping org = tables.getOrgMapping();
            final QRepositoryPullRequestIssueKeyMapping issueMapping = tables.getIssueMapping();
            final QPullRequestParticipantMapping participant = tables.getParticipantMapping();

            return connection.select(
                    pullRequest.ID,
                    participant.USERNAME,
                    participant.APPROVED,
                    participant.ROLE,
                    issueMapping.ISSUE_KEY)
                    .from(pullRequest)
                    .join(issueMapping).on(pullRequest.ID.eq(issueMapping.PULL_REQUEST_ID))
                    .join(repository).on(repository.ID.eq(pullRequest.TO_REPOSITORY_ID))
                    .join(org).on(org.ID.eq(repository.ORGANIZATION_ID))
                    .leftJoin(participant).on(pullRequest.ID.eq(participant.PULL_REQUEST_ID))
                    .where(pullRequest.CREATED_ON.between(minDate, maxDate)
                            .and(repository.DELETED.eq(false))
                            .and(repository.LINKED.eq(true))
                            .and(IssueKeyPredicateFactory.buildIssueKeyPredicate(issueKeys, issueMapping))
                            .and(getOrgDvcsTypeCondition(dvcsType))
                    ).orderBy(pullRequest.ID.asc(), participant.USERNAME.asc())
                    .fetch();
        });
    }

    private List<PullRequest> transformToPullRequest(final Collection<Tuple> rawPullRequests, final Tables tables) {
        return rawPullRequests.stream().map(t -> buildPullRequest(t, tables)).collect(toList());
    }

    private PullRequest buildPullRequest(@Nonnull final Tuple tuple, @Nonnull final Tables tables) {
        return PullRequest.newBuilder()
                .withId(tuple.get(tables.getPrMapping().ID))
                .withRemoteId(ofNullable(tuple.get(tables.getPrMapping().REMOTE_ID)).orElse(-1L))
                .withRepositoryId(ofNullable(tuple.get(tables.getPrMapping().TO_REPOSITORY_ID)).orElse(-1))
                .withName(tuple.get(tables.getPrMapping().NAME))
                .withUrl(tuple.get(tables.getPrMapping().URL))
                .withStatus(ofNullable(tuple.get(tables.getPrMapping().LAST_STATUS)).map(PullRequestStatus::fromRepositoryPullRequestMapping))
                .withCreatedOn(tuple.get(tables.getPrMapping().CREATED_ON))
                .withUpdatedOn(tuple.get(tables.getPrMapping().UPDATED_ON))
                .withAuthor(tuple.get(tables.getPrMapping().AUTHOR))
                .withCommentCount(ofNullable(tuple.get(tables.getPrMapping().COMMENT_COUNT)).orElse(0))
                .withExecutedBy(tuple.get(tables.getPrMapping().EXECUTED_BY))
                .withSource(getSourcePullRequestRef(tuple, tables))
                .withDestination(getDestinationPullRequestRef(tuple, tables))
                .build();
    }

    private List<PullRequest> populateAndFilterPullrequests(Tables tables, List<PullRequest> pullRequests,
                                                            List<Tuple> pullRequestsData) {
        pullRequests.forEach(pullrequest -> {
            pullRequestsData.stream()
                    .filter(data -> data.get(tables.getPrMapping().ID).equals(pullrequest.getId()))
                    .forEach(data -> {
                        addParticipant(pullrequest, data, tables);
                        pullrequest.addIssueKeyIfAbsent(data.get(tables.getIssueMapping().ISSUE_KEY));
                    });
        });

        return pullRequests.stream().filter(pr -> !pr.getIssueKeys().isEmpty()).collect(toList());
    }

    private void addParticipant(@Nonnull PullRequest pullRequest, @Nonnull final Tuple tuple, @Nonnull final Tables tables) {
        final String username = tuple.get(tables.getParticipantMapping().USERNAME);
        final Boolean approved = tuple.get(tables.getParticipantMapping().APPROVED);
        final String role = tuple.get(tables.getParticipantMapping().ROLE);

        // We are left joining so only include the participant if it is available
        if (username != null || approved != null || role != null) {
            Participant participant = new Participant(username,
                    approved == null ? false : approved, role);

            if (pullRequest.getParticipants() == null) {
                pullRequest.setParticipants(new ArrayList<>());
            }

            if (!pullRequest.getParticipants().contains(participant)) {
                pullRequest.getParticipants().add(participant);
            }
        }
    }

    private PullRequestRef getSourcePullRequestRef(final Tuple tuple, final Tables tables) {
        final String sourceBranch = tuple.get(tables.getPrMapping().SOURCE_BRANCH);
        final String sourceRepo = tuple.get(tables.getPrMapping().SOURCE_REPO);
        final String orgHostUrl = tuple.get(tables.getOrgMapping().HOST_URL);

        return new PullRequestRef(sourceBranch, sourceRepo,
                PullRequestTransformer.createRepositoryUrl(orgHostUrl, sourceRepo));
    }

    private PullRequestRef getDestinationPullRequestRef(final Tuple tuple, final Tables tables) {
        final String destinationBranch = tuple.get(tables.getPrMapping().DESTINATION_BRANCH);
        final String orgName = tuple.get(tables.getOrgMapping().NAME);
        final String slug = tuple.get(tables.getRepositoryMapping().SLUG);
        final String orgHostUrl = tuple.get(tables.getOrgMapping().HOST_URL);
        final String repositoryLabel = createRepositoryLabel(orgName, slug);
        final String repositoryUrl = createRepositoryUrl(orgHostUrl, orgName, slug);

        return new PullRequestRef(destinationBranch, repositoryLabel, repositoryUrl);
    }

    private class Tables {
        private QRepositoryPullRequestMapping prMapping;
        private QRepositoryPullRequestIssueKeyMapping issueMapping;
        private QPullRequestParticipantMapping participantMapping;
        private QRepositoryMapping repositoryMapping;
        private QOrganizationMapping orgMapping;
        private boolean initialized = false;

        public void inititalize() {
            prMapping = new QRepositoryPullRequestMapping();
            issueMapping = new QRepositoryPullRequestIssueKeyMapping();
            participantMapping = new QPullRequestParticipantMapping();
            repositoryMapping = new QRepositoryMapping();
            orgMapping = new QOrganizationMapping();
            initialized = true;
        }

        public QRepositoryPullRequestMapping getPrMapping() {
            checkIsInitialed();
            return prMapping;
        }

        public QRepositoryPullRequestIssueKeyMapping getIssueMapping() {
            checkIsInitialed();
            return issueMapping;
        }

        public QPullRequestParticipantMapping getParticipantMapping() {
            checkIsInitialed();
            return participantMapping;
        }

        public QRepositoryMapping getRepositoryMapping() {
            checkIsInitialed();
            return repositoryMapping;
        }

        public QOrganizationMapping getOrgMapping() {
            checkIsInitialed();
            return orgMapping;
        }

        private void checkIsInitialed() {
            if (!initialized) {
                throw new IllegalStateException("A call to initialize must be made before " +
                        "attempting to access the qBeans!");
            }
        }
    }
}
