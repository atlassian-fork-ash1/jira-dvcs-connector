package com.atlassian.jira.plugins.dvcs.rest.external.v1;

import com.atlassian.fusion.aci.api.model.Installation;
import com.atlassian.fusion.aci.api.service.ACIJwtService;
import com.atlassian.jira.plugins.dvcs.exception.SourceControlException;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.RepositoryList;
import com.atlassian.jira.plugins.dvcs.rest.security.AdminOnly;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.service.RepositoryService;
import com.atlassian.jira.plugins.dvcs.service.optional.aci.ACIJwtServiceAccessor;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants;
import com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag;
import com.atlassian.jira.plugins.dvcs.util.ExceptionLogger;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URL;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.atlassian.jira.plugins.dvcs.rest.ResourceHelper.buildErrorResponse;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * REST resource for working with repositories
 */
@Path("/repository")
@Consumes(MediaType.APPLICATION_JSON)
public class RepositoryResource {

    private static final Logger log = ExceptionLogger.getLogger(RepositoryResource.class);

    private static final Map<String, SynchronizationFlag> WEBHOOK_EVENT_MAP = ImmutableMap.of(
            "repo:push", SynchronizationFlag.SYNC_CHANGESETS,
            "pullrequest:created", SynchronizationFlag.SYNC_PULL_REQUESTS
    );

    private final OrganizationService organizationService;
    private final RepositoryService repositoryService;
    private final ACIJwtServiceAccessor jwtServiceAccessor;

    public RepositoryResource(
            @Nonnull final ACIJwtServiceAccessor jwtServiceAccessor,
            @Nonnull final OrganizationService organizationService,
            @Nonnull final RepositoryService repositoryService) {
        this.organizationService = checkNotNull(organizationService);
        this.repositoryService = checkNotNull(repositoryService);
        this.jwtServiceAccessor = checkNotNull(jwtServiceAccessor);
    }

    /**
     * List all repositories.
     *
     * @return HTTP.200 with all current repositories
     * @response.representation.200.doc Returned with all current repositories
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/")
    @AdminOnly
    public Response getAllRepositories() {
        List<Repository> activeRepositories = repositoryService.getAllRepositories();
        return Response.ok(new RepositoryList(activeRepositories)).build();
    }

    /**
     * Get the repository.
     *
     * @param id the id of the repository to retrieve
     * @return HTTP.200 with the repository details if the repository was found;
     * HTTP.204 if no repository was found for the given ID.
     * @response.representation.200.doc Returned with the repository details if a repository was found for the given ID
     * @response.representation.204.doc If no repository was found with the given ID
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{id}")
    @AdminOnly
    public Response getRepository(@PathParam("id") int id) {
        Repository repository = repositoryService.get(id);
        if (repository != null) {
            return Response.ok(repository).build();
        } else {
            return Response.noContent().build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/find")
    @AdminOnly
    public Response getRepositoriesByOrganization(@QueryParam("orgId") final int orgId) {
        final List<Repository> repositories = repositoryService.getAllByOrganization(orgId);
        return Response.ok(repositories).build();
    }

    /**
     * Disables all enabled repositories.
     *
     * @return the number of repositories whose state changed to disabled
     */
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/disable")
    @AdminOnly
    public Response disableAllRepositories() {
        final int reposDisabled = repositoryService.disableAllRepositories();
        return Response.ok(String.valueOf(reposDisabled)).build();
    }

    /**
     * Disable (unlink) the given repository.
     *
     * @param repoId The ID of the repository to unlink
     * @return HTTP.400 if no repository ID is given; HTTP.204 if disabling was successful.
     * @response.representation.400.doc Returned if no ID was provided in the request
     * @response.representation.204.doc If disabling the repository was successful
     */
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{id}/disable")
    @AdminOnly
    public Response disableRepository(@PathParam("id") Integer repoId) {
        if (repoId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        repositoryService.enableRepository(repoId, false);
        return Response.noContent().build();
    }

    /**
     * Enable (link) the given repository.
     *
     * @param repoId The ID of the repository to link
     * @return HTTP.400 if no repository ID is given; HTTP.204 if enabling was successful.
     * @response.representation.204.doc If there was no error while enabling repository
     */
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{id}/enable")
    @AdminOnly
    public Response enableRepository(@PathParam("id") Integer repoId) {
        repositoryService.enableRepository(repoId, true);
        return Response.noContent().build();
    }

    /**
     * Start repository pull requests full sync.
     *
     * @param id the id of the repository to sync
     * @return HTTP.303 (Redirect) with the URL of the repository details if sync was successful;
     * HTTP.503 (Service unavilable) if something went wrong.
     * @response.representation.303.doc Returned with the URL of the repository details if the sync was successful
     * @response.representation.503.doc Returned if an error occurred during sync
     */
    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{id}/fullSyncPullRequests")
    @AdminOnly
    public Response startRepositoryPullRequestsSynchronization(@PathParam("id") int id, @Context final UriInfo uriInfo) {
        log.debug("Rest request to pull request fullsync repository [{}] ", id);

        try {
            repositoryService.sync(id, EnumSet.of(SynchronizationFlag.SYNC_PULL_REQUESTS));

            // ...
            // redirect to Repository resource - that will contain sync
            // message/status
            UriBuilder ub = uriInfo.getBaseUriBuilder();
            URI uri = ub.path("/repository/{id}").build(id);

            return Response.seeOther(uri).build();
        } catch (SourceControlException.SynchronizationDisabled e) {
            return buildErrorResponse(Response.Status.SERVICE_UNAVAILABLE, e.getMessage());
        }
    }

    /**
     * Start repository changeset full sync.
     *
     * @param id the id of the repository to sync
     * @return HTTP.303 (Redirect) with the URL of the repository details if sync was successful;
     * HTTP.503 (Service unavilable) if something went wrong.
     * @response.representation.303.doc Returned with the URL of the repository details if the sync was successful
     * @response.representation.503.doc Returned if an error occurred during sync
     */
    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{id}/fullSyncChangesets")
    @AdminOnly
    public Response startRepositoryChangesetsSynchronization(@PathParam("id") int id, @Context final UriInfo uriInfo) {
        log.debug("Rest request to changesets fullsync repository [{}] ", id);

        try {
            repositoryService.sync(id, EnumSet.of(SynchronizationFlag.SYNC_CHANGESETS));

            // ...
            // redirect to Repository resource - that will contain sync
            // message/status
            UriBuilder ub = uriInfo.getBaseUriBuilder();
            URI uri = ub.path("/repository/{id}").build(id);

            return Response.seeOther(uri).build();
        } catch (SourceControlException.SynchronizationDisabled e) {
            return buildErrorResponse(Response.Status.SERVICE_UNAVAILABLE, e.getMessage());
        }
    }

    /**
     * Start repository full sync.
     *
     * @param id the id of the repository to sync
     * @return HTTP.303 (Redirect) with the URL of the repository details if sync was successful;
     * HTTP.503 (Service unavilable) if something went wrong.
     * @response.representation.303.doc Returned with the URL of the repository details if the sync was successful
     * @response.representation.503.doc Returned if an error occurred during sync
     */
    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{id}/fullsync")
    @AdminOnly
    public Response startRepositoryFullSync(@PathParam("id") int id, @Context final UriInfo uriInfo) {
        log.debug("Rest request to fullsync repository [{}] ", id);

        try {
            repositoryService.sync(id, EnumSet.of(SynchronizationFlag.SYNC_CHANGESETS, SynchronizationFlag.SYNC_PULL_REQUESTS));

            // ...
            // redirect to Repository resource - that will contain sync
            // message/status
            UriBuilder ub = uriInfo.getBaseUriBuilder();
            URI uri = ub.path("/repository/{id}").build(id);

            return Response.seeOther(uri).build();
        } catch (SourceControlException.SynchronizationDisabled e) {
            return buildErrorResponse(Response.Status.SERVICE_UNAVAILABLE, e.getMessage());
        }
    }

    /**
     * Start repository soft sync.
     *
     * @param id the id of the repository to sync
     * @return HTTP.303 (Redirect) with the URL of the repository details if sync was successful;
     * HTTP.503 (Service unavilable) if something went wrong.
     * @response.representation.303.doc Returned with the URL of the repository details if the sync was successful
     * @response.representation.503.doc Returned if an error occurred during sync
     */
    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{id}/softsync")
    @AdminOnly
    public Response startRepositorySoftSync(@PathParam("id") int id, @Context final UriInfo uriInfo) {
        log.debug("Rest request to softsync repository [{}] ", id);

        try {
            repositoryService.sync(id,
                    EnumSet.of(SynchronizationFlag.SOFT_SYNC, SynchronizationFlag.SYNC_CHANGESETS, SynchronizationFlag.SYNC_PULL_REQUESTS));
            // ...
            // redirect to Repository resource - that will contain sync
            // message/status
            UriBuilder ub = uriInfo.getBaseUriBuilder();
            URI uri = ub.path("/repository/{id}").build(id);

            return Response.seeOther(uri).build();
        } catch (SourceControlException.SynchronizationDisabled e) {
            return buildErrorResponse(Response.Status.SERVICE_UNAVAILABLE, e.getMessage());
        }
    }

    /**
     * Start repository sync.
     * <p>
     * Triggered by remote webhooks.
     *
     * @param id     the id of the repository to sync
     * @param source the payload received from the remote
     * @return HTTP.200 if sync was successful;
     * HTTP.503 (Service unavilable) if something went wrong.
     * @response.representation.200.doc Returned if the sync was successful
     * @response.representation.503.doc Returned if an error occurred during sync
     */
    @AnonymousAllowed
    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{id}/sync")
    public Response startRepositoryPRSync(@PathParam("id") int id, String source) {
        log.info("Pull Request Postcommit hook started synchronization for repository [{}].", id);
        String key = null;

        try {
            JSONObject jsoned = new JSONObject(source);
            Iterator<String> keys = jsoned.keys();
            if (keys.hasNext()) {
                key = keys.next();
            }
        } catch (JSONException e) {
            log.info("Could not parse json request.", e);
        }

        log.debug("Rest request to soft sync pull requests for repository [{}] with type [{}]", id, key);

        try {
            repositoryService.sync(id,
                    EnumSet.of(SynchronizationFlag.SOFT_SYNC, SynchronizationFlag.SYNC_PULL_REQUESTS, SynchronizationFlag.WEBHOOK_SYNC));

            return Response.ok().build();
        } catch (SourceControlException.SynchronizationDisabled e) {
            return buildErrorResponse(Response.Status.SERVICE_UNAVAILABLE, e.getMessage());
        }
    }

    /**
     * Start repository sync.
     * <p>
     * Triggered by remote webhooks.
     *
     * @param id      the id of the repository to sync
     * @param payload The payload received from the remote
     * @return HTTP.200 if sync was successful;
     * HTTP.503 (Service unavailable) if something went wrong.
     * @response.representation.200.doc Returned if the sync was successful
     * @response.representation.503.doc Returned if an error occurred during sync
     */
    @AnonymousAllowed
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{id}/sync")
    public Response startRepositorySync(@PathParam("id") int id, @FormParam("payload") String payload) {
        log.info("Postcommit hook started synchronization for repository [{}].", id);
        log.debug("Rest request to soft sync repository [{}] with payload [{}]", id, payload);

        try {
            repositoryService.sync(id,
                    EnumSet.of(SynchronizationFlag.SOFT_SYNC, SynchronizationFlag.SYNC_CHANGESETS, SynchronizationFlag.WEBHOOK_SYNC));

            return Response.ok().build();
        } catch (SourceControlException.SynchronizationDisabled e) {
            return buildErrorResponse(Response.Status.SERVICE_UNAVAILABLE, e.getMessage());
        }
    }

    /**
     * Endpoint for receiving webhooks to trigger syncing of dev information.
     * <p>
     * Currently supports webhook payloads received from Bitbucket for the <code>repo:push</code>
     * and <code>pullrequest:created</code> events.
     *
     * @param payloadString The payload received from the remote
     * @param request       The request received
     * @see <a href="https://developer.atlassian.com/bitbucket/modules/webhooks.html">Bitbucket Connect Webhooks</a>
     * @see <a href="https://confluence.atlassian.com/bitbucket/event-payloads-740262817.html">Bitbucket Event payloads</a>
     */
    @AnonymousAllowed
    @POST
    @Path("/webhook")
    public void receiveWebhook(final String payloadString, @Context final HttpServletRequest request) {
        log.trace("Received webhook payload: {}", payloadString);
        log.debug("Received webhook. Processing...");
        try {
            final JSONObject payloadJson = new JSONObject(payloadString);

            final String eventName = payloadJson.getString("event");
            if (!WEBHOOK_EVENT_MAP.containsKey(eventName)) {
                log.debug("Webhook event '{}' is unsupported. Ignoring webhook.", eventName);
                return;
            }

            final JSONObject repositoryJson = payloadJson.getJSONObject("data").getJSONObject("repository");
            final String repoName = repositoryJson.getString("name");
            final String principalUuid = repositoryJson.getJSONObject("owner").getString("uuid");
            log.debug("Webhook has event '{}' for repo '{}' owned by '{}'.",
                    new Object[]{eventName, repoName, principalUuid});

            if (!verifyJwtHeader(principalUuid, request)) {
                log.debug("Webhook failed JWT authentication. Ignoring webhook.");
                return;
            }

            final Organization organization = organizationService.getByPrincipalId(principalUuid);
            if (organization == null) {
                log.debug("No organization found for principal UUID '{}'. Ignoring webhook.", principalUuid);
                return;
            }

            final Repository repository = repositoryService.getByNameForOrganization(organization.getId(), repoName);
            if (repository == null) {
                log.debug("No repository found with name '{}' for organization '{}'. Ignoring webhook.",
                        repoName, organization.getName());
                return;
            }
            if (!repository.isLinked()) {
                log.debug("Repository '{}' is not linked. Ignoring webhook.", repoName);
                return;
            }

            log.info("Valid webhook received for '{}'. Triggering sync.", repositoryJson.getString("full_name"));

            final EnumSet<SynchronizationFlag> flags = EnumSet.of(
                    SynchronizationFlag.SOFT_SYNC,
                    SynchronizationFlag.WEBHOOK_SYNC,
                    WEBHOOK_EVENT_MAP.get(eventName));
            repositoryService.sync(repository.getId(), flags);
        } catch (JSONException e) {
            log.debug("Received a malformed JSON payload. Ignoring webhook.", e);
        } catch (Exception e) {
            log.debug("Exception occurred while processing webhook.", e);
        }
    }

    private boolean verifyJwtHeader(final String principalId, final HttpServletRequest request) {
        final String authorizationHeader = request.getHeader("Authorization");

        final Optional<ACIJwtService> maybeJwtService = jwtServiceAccessor.get();
        if (!maybeJwtService.isPresent()) {
            log.trace("JWT service is not available. Failing verification.");
            return false;
        }

        if (isEmpty(authorizationHeader) || !authorizationHeader.startsWith("JWT")) {
            log.trace("Authentication header is not a JWT token. Failing verification");
            return false;
        }

        log.trace("Verifying '{}' with header '{}'.", request.getRequestURL(), authorizationHeader);

        try {
            final Installation installation = maybeJwtService.get().verifyJwtToken(
                    BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID,
                    principalId,
                    request.getMethod(),
                    new URL(request.getRequestURL().toString()),
                    authorizationHeader.replace("JWT", "").trim());

            return installation != null;
        } catch (Exception e) {
            log.trace("Exception while verifying JWT. Failing verification.", e);
            return false;
        }
    }
}
