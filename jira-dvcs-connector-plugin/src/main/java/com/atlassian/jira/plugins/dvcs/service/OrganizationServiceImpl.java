package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.fusion.aci.api.model.Installation;
import com.atlassian.fusion.aci.api.service.ACIInstallationService;
import com.atlassian.fusion.aci.api.service.exception.ConnectApplicationNotFoundException;
import com.atlassian.fusion.aci.api.service.exception.InstallationNotFoundException;
import com.atlassian.fusion.aci.api.service.exception.RemoteApplicationClientException;
import com.atlassian.jira.exception.NotFoundException;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsType;
import com.atlassian.jira.plugins.dvcs.analytics.event.Source;
import com.atlassian.jira.plugins.dvcs.analytics.smartcommits.SmartCommitsAnalyticsService;
import com.atlassian.jira.plugins.dvcs.dao.OrganizationDao;
import com.atlassian.jira.plugins.dvcs.model.AccountInfo;
import com.atlassian.jira.plugins.dvcs.model.DvcsUser;
import com.atlassian.jira.plugins.dvcs.model.Group;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.credential.Credential;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.plugins.dvcs.model.credential.OAuthCredentialVisitor;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.jira.plugins.dvcs.service.optional.aci.AciInstallationServiceAccessor;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicator;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicatorProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.BitbucketCommunicator;
import com.atlassian.jira.plugins.dvcs.util.ExceptionLogger;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.RejectedExecutionException;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static io.atlassian.fugue.Option.fromOptional;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Component
public class OrganizationServiceImpl implements OrganizationService {
    @VisibleForTesting
    static final String WRONG_CREDENTIAL_TYPE_MESSAGE_KEY =
            "com.atlassian.jira.plugins.dvcs.service.oauth.wrong-credential-type";
    private static final Logger log = ExceptionLogger.getLogger(OrganizationServiceImpl.class);
    @Inject
    private AciInstallationServiceAccessor aciInstallationServiceAccessor;

    @Inject
    private AnalyticsService analyticsService;

    @Inject
    private DvcsCommunicatorProvider dvcsCommunicatorProvider;

    @Inject
    private I18nHelper i18nHelper;

    @Inject
    private OrganizationDao organizationDao;

    @Inject
    private RepositoryService repositoryService;

    @Inject
    private SmartCommitsAnalyticsService smartCommitsAnalyticsService;

    @Inject
    private LinkerService linkerService;

    @Override
    public AccountInfo getAccountInfo(final String hostUrl, final String accountName) {
        return getAccountInfo(hostUrl, accountName, null);
    }

    @Override
    public AccountInfo getAccountInfo(final String hostUrl, final String accountName,
                                      final String dvcsType) {
        return dvcsCommunicatorProvider.getAccountInfo(hostUrl, accountName, dvcsType);
    }

    @Override
    public List<Organization> getAll(final boolean loadRepositories) {
        final List<Organization> organizations = organizationDao.getAll();
        if (loadRepositories) {
            for (Organization organization : organizations) {
                retrieveRepositories(organization);
            }
        }
        return organizations;
    }

    @Override
    public List<Organization> getAll(final boolean loadRepositories, final String type) {
        final List<Organization> organizations = organizationDao.getAllByType(type);
        if (loadRepositories) {
            for (final Organization organization : organizations) {
                retrieveRepositories(organization);
            }
        }
        return organizations;
    }

    @Override
    public int getAllCount() {
        return organizationDao.getAllCount();
    }

    @Override
    public Organization get(final int organizationId, final boolean loadRepositories) {
        Organization organization = organizationDao.get(organizationId);

        if (loadRepositories && organization != null) {
            retrieveRepositories(organization);
        }

        return organization;
    }

    /**
     * Retrieve an Organization that must exist without retrieving its repositories, throwing an Exception if it does
     * not exist
     *
     * @param id The id of the Organization
     * @return The Organization that matches the id
     * @throws NotFoundException If the organization could not be found
     */
    private Organization getOrganizationThatMustExist(final int id) throws NotFoundException {
        final Organization organization = get(id, false);
        if (organization == null) {
            throw new NotFoundException("Could not find Organization with id: " + id);
        }

        return organization;
    }

    @Override
    public Organization getByPrincipalId(final String principalId) {
        return organizationDao.getByPrincipalId(principalId);
    }

    @Override
    public Organization save(@Nonnull final Organization organization) {
        checkNotNull(organization, "An organization is required");

        final Organization org = organizationDao.getByHostAndName(organization.getHostUrl(), organization.getName());
        if (org != null) {
            // nop;
            // we've already have this organization, don't save another one
            return org;
        }

        if (organization.getCredential() == null) {
            organization.setCredential(CredentialFactory.createUnauthenticatedCredential());
        }

        //
        // it's brand new organization. save it.
        //
        final Organization orgSaveResult = organizationDao.save(organization);

        // sync repository list
        repositoryService.syncRepositoryList(orgSaveResult, false);

        return orgSaveResult;
    }

    @Override
    public void remove(final int organizationId) {
        final Organization organization = get(organizationId, false);
        if (organization == null) {
            return;
        }

        final long startTime = System.currentTimeMillis();
        log.debug("Deleting Organization {}...", organizationId);
        final List<Repository> repositoriesToDelete = repositoryService.getAllByOrganization(
                organizationId, true);
        repositoryService.removeRepositories(repositoriesToDelete);
        organizationDao.remove(organizationId);
        repositoryService.removeOrphanRepositories(repositoriesToDelete);
        log.debug("Organization {} was deleted in {} ms", organizationId, System.currentTimeMillis() - startTime);

        // We should only trigger the remote uninstall of the application corresponding to an ACI-managed organisation
        // once we have removed the linkers and commit hooks from Bitbucket, since otherwise our JWTs will stop working
        // and we won't be able to remove the links and hooks.
        triggerAciRemoteUninstallIfNecessary(organization);
    }

    private void triggerAciRemoteUninstallIfNecessary(final Organization organization) {
        fromOptional(organization.getCredential().accept(PrincipalIDCredential.visitor()))
                .forEach(principalCredential ->
                {
                    analyticsService.publishConnectOrganisationRemoved(organization);
                    fromOptional(aciInstallationServiceAccessor.get()).forEach(aciInstallationService ->
                            tryAciUninstall(aciInstallationService, principalCredential, organization));
                });
    }

    private void tryAciUninstall(
            @Nonnull final ACIInstallationService aciInstallationService,
            @Nonnull final PrincipalIDCredential principalCredential,
            @Nonnull final Organization organization) {
        try {
            log.debug("Triggering uninstall for installation with principal ID {}.", principalCredential.getPrincipalId());
            aciInstallationService.uninstall(BITBUCKET_CONNECTOR_APPLICATION_ID,
                    principalCredential.getPrincipalId(), Optional.empty());
            log.debug("Uninstall of installation with principal ID {} successful.", principalCredential.getPrincipalId());
        } catch (InstallationNotFoundException | ConnectApplicationNotFoundException e) {
            // we've lost details of the installation in ACI, but we swallow these exceptions so that we don't show
            // an ugly error in the UI
            log.warn("Tried to tell ACI to uninstall the installation for org with id '{}', "
                            + "but the installation with application id '{}' and principalUuid '{}' doesn't exist",
                    new Object[]{organization.getId(), BITBUCKET_CONNECTOR_APPLICATION_ID, principalCredential.getPrincipalId()},
                    e);
        } catch (RemoteApplicationClientException e) {
            // Failed to remove on the remote side, we do not report the exception as we can still successfully remove
            // in JIRA
            String errorMessage = String.format("ACI reports that remote uninstallation has failed for org with id '%s', "
                            + "and application id '%s' and principalUuid '%s'",
                    organization.getId(), BITBUCKET_CONNECTOR_APPLICATION_ID, principalCredential.getPrincipalId());
            log.warn(errorMessage, e);
        }
    }

    @Override
    public Organization updateCredentials(final int organizationId, final Credential credential) {
        final Organization organization = organizationDao.get(organizationId);
        if (organization == null) {
            return null;
        }
        organization.setCredential(credential);
        return organizationDao.save(organization);
    }

    @Override
    public Organization updateOAuthCredentials(final int organizationId, final String key, final String secret) {
        final Organization organization = organizationDao.get(organizationId);
        if (organization == null) {
            return null;
        }

        if (!OAuthCredentialVisitor.isAOAuthCredential(organization.getCredential())) {
            throw new IllegalStateException(
                    i18nHelper.getText(WRONG_CREDENTIAL_TYPE_MESSAGE_KEY, organization.getName()));
        }

        organization.setCredential(CredentialFactory.updateOAuthCredential(organization.getCredential(), key, secret));
        return organizationDao.save(organization);
    }

    @Override
    public void updateCredentialsAccessToken(final int organizationId, final String accessToken) {
        final Organization organization = organizationDao.get(organizationId);
        if (organization != null) {
            organization.setCredential(CredentialFactory
                    .buildCredential()
                    .copyFrom(organization.getCredential())
                    .setToken(accessToken)
                    .build());
            organizationDao.save(organization);
        }
    }

    @Override
    public void enableAutolinkNewRepos(final int orgId, final boolean autolink) {
        final Organization organization = organizationDao.get(orgId);
        if (organization != null) {
            organization.setAutolinkNewRepos(autolink);
            organizationDao.save(organization);
        }
    }

    @Override
    public void enableSmartcommitsOnNewRepos(final int id, final boolean enabled) {
        final Organization organization = organizationDao.get(id);
        if (organization != null) {
            organization.setSmartcommitsOnNewRepos(enabled);
            organizationDao.save(organization);
            smartCommitsAnalyticsService.fireSmartCommitAutoEnabledConfigChange(id, enabled);
        }
    }

    @Override
    public void setAutolinkAndSmartcommits(final int organizationId,
                                           final boolean enableAutolink, final boolean enableSmartCommits) {
        final Organization organization = organizationDao.get(organizationId);
        if (organization != null) {
            organization.setAutolinkNewRepos(enableAutolink);
            organization.setSmartcommitsOnNewRepos(enableSmartCommits);
            organizationDao.save(organization);
            smartCommitsAnalyticsService.fireSmartCommitAutoEnabledConfigChange(organizationId, enableSmartCommits);
            setSmartCommitStatusOnAllNonLinkedRepos(organization.getId(), enableSmartCommits);
        }
    }

    private void setSmartCommitStatusOnAllNonLinkedRepos(int organizationId, boolean enableSmartCommits) {
        //organization.getRepositories returns null so we have to use the repositoryService
        repositoryService.getAllByOrganization(organizationId)
                .stream()
                .filter(repo -> !repo.isLinked())
                .forEach(repo -> {
                    log.debug("setting smart commits for repo " + repo.getId());
                    repo.setSmartcommitsEnabled(enableSmartCommits);
                    repositoryService.save(repo);
                });
    }

    @Override
    public List<Organization> getAllByIds(final Collection<Integer> ids) {
        if (CollectionUtils.isNotEmpty(ids)) {
            return organizationDao.getAllByIds(ids);
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public void setDefaultGroupsSlugs(final int orgId, final Collection<String> groupsSlugs) {
        organizationDao.setDefaultGroupsSlugs(orgId, groupsSlugs);
    }

    @Override
    public Organization findIntegratedAccount() {
        return organizationDao.findIntegratedAccount();
    }

    @Override
    public Organization getByHostAndName(final String hostUrl, final String name) {
        return organizationDao.getByHostAndName(hostUrl, name);
    }

    @Override
    public DvcsUser getTokenOwner(final int organizationId) {
        final Organization organization = get(organizationId, false);
        final DvcsCommunicator communicator = dvcsCommunicatorProvider
                .getCommunicator(organization.getDvcsType());
        return communicator.getTokenOwner(organization);
    }

    @Override
    public List<Group> getGroupsForOrganization(final Organization organization) {
        return dvcsCommunicatorProvider.getCommunicator(organization.getDvcsType())
                .getGroupsForOrganization(organization);
    }

    @Override
    public boolean existsOrganizationWithType(final String... types) {
        return organizationDao.existsOrganizationWithType(types);
    }

    @Override
    public Organization changeOrganizationApprovalState(final int id, final Organization.ApprovalState approvalState)
            throws NotFoundException {

        final Organization organization = getOrganizationThatMustExist(id);
        log.trace("Setting approval state of Organisation id: {} to {}", id, approvalState);
        organization.setApprovalState(approvalState);
        organizationDao.save(organization);
        return organization;
    }

    @Override
    public void createNewOrgBasedOnAciInstallation(final Installation installation) {
        log.debug("Creating new DVCS organisation for {}", installation.getPrincipalUuid());

        // Published for consistency with other org add flows
        analyticsService.publishOrganisationAddStarted(Source.DEVTOOLS, DvcsType.BITBUCKET);

        final Credential credential = CredentialFactory.createPrincipalCredential(installation.getPrincipalUuid());

        final Organization org = new Organization();
        org.setHostUrl(installation.getBaseUrl());
        org.setName(installation.getPrincipalUsername());
        org.setDvcsType(BitbucketCommunicator.BITBUCKET);
        org.setAutolinkNewRepos(false);
        org.setSmartcommitsOnNewRepos(false);
        org.setCredential(credential);

        // When Org is created via ACI, it has to be in PENDING_APPROVAL state until it is explicitly
        // approved by a JIRA Admin.
        org.setApprovalState(Organization.ApprovalState.PENDING_APPROVAL);

        save(org);

        log.debug("Created new DVCS organisation for {}", installation.getPrincipalUuid());
        analyticsService.publishConnectOrganisationAdded(org);
    }

    @Override
    public void migrateExistingOrgToPrincipalCredentialOrg(final Organization org, final String principalUuid) {
        checkNotNull(org);
        checkArgument(isNotBlank(principalUuid), "principalUuid cannot be blank");

        log.debug("Organisation already exists for {}. Figuring out what to do....", principalUuid);

        final Credential credential = CredentialFactory.createPrincipalCredential(principalUuid);
        final Optional<PrincipalIDCredential> oldPrincipalIdCredential = org.getCredential()
                .accept(PrincipalIDCredential.visitor());

        final Organization updatedOrg = updateCredentials(org.getId(), credential);
        if (oldPrincipalIdCredential.isPresent()) {
            log.debug("Organisation already exists for {}. Updated credentials.", principalUuid);
            analyticsService.publishConnectOrganisationUpdated(updatedOrg);
        } else {
            log.debug("Organisation already exists for {}. Migrated credentials and removing custom BB linkers and webhooks.",
                    principalUuid);
            try {
                linkerService.removeLinkersAsync(org);
            } catch (RejectedExecutionException e) {
                log.debug("Failed to schedule task to update linker values for Organization {}", org.getId());
            }
            try {
                repositoryService.removePostcommitHooksAsync(org, true);
            } catch (RejectedExecutionException e) {
                log.debug("Failed to schedule task to remove webhooks for Organization {}", org.getId());
            }
            analyticsService.publishConnectOrganisationMigrated(updatedOrg);
        }
    }

    private void retrieveRepositories(final Organization org) {
        final List<Repository> repositories = repositoryService.getAllByOrganization(org.getId());
        org.setRepositories(repositories);
    }

    @Override
    public void removeAll() {
        getAll(false).stream().forEach(org -> remove(org.getId()));
    }
}
