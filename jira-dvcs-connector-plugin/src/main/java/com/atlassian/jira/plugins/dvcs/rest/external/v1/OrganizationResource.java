package com.atlassian.jira.plugins.dvcs.rest.external.v1;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.exception.NotFoundException;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService.OrganizationApprovalLocation;
import com.atlassian.jira.plugins.dvcs.exception.SourceControlException;
import com.atlassian.jira.plugins.dvcs.model.DvcsUser;
import com.atlassian.jira.plugins.dvcs.model.Group;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.Credential;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.plugins.dvcs.rest.json.CredentialJson;
import com.atlassian.jira.plugins.dvcs.rest.json.OrganizationAutoSettingsJson;
import com.atlassian.jira.plugins.dvcs.rest.security.AdminOnly;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.service.RepositoryService;
import com.atlassian.jira.plugins.dvcs.util.ExceptionLogger;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService.OrganizationApprovalFailedReason.ORGANIZATION_ALREADY_REMOVED;
import static com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService.OrganizationApprovalFailedReason.UNKNOWN_ERROR;
import static com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService.OrganizationApprovalLocation.UNKNOWN;
import static com.atlassian.jira.plugins.dvcs.model.Organization.ApprovalState.APPROVED;
import static com.atlassian.jira.plugins.dvcs.model.Organization.ApprovalState.PENDING_APPROVAL;
import static com.atlassian.jira.plugins.dvcs.rest.ResourceHelper.buildErrorResponse;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@ParametersAreNonnullByDefault
@ExperimentalApi
@Path("/organization")
public class OrganizationResource {
    static final String NO_SUCH_ORG_MESSAGE_KEY = "com.atlassian.jira.plugins.dvcs.service.no-such-organization";
    private static final Logger log = ExceptionLogger.getLogger(OrganizationResource.class);
    private final AnalyticsService analyticsService;
    private final OrganizationService organizationService;
    private final RepositoryService repositoryService;
    private final I18nHelper i18nHelper;

    public OrganizationResource(
            @ComponentImport final I18nHelper i18nHelper,
            final AnalyticsService analyticsService,
            final OrganizationService organizationService,
            final RepositoryService repositoryService) {
        this.analyticsService = checkNotNull(analyticsService);
        this.organizationService = checkNotNull(organizationService);
        this.repositoryService = checkNotNull(repositoryService);
        this.i18nHelper = checkNotNull(i18nHelper);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{id}/tokenOwner")
    @AdminOnly
    public Response getTokenOwner(@PathParam("id") String organizationId) {
        if (organizationId == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        DvcsUser currentUser;
        try {
            currentUser = organizationService.getTokenOwner(Integer.parseInt(organizationId));
            return Response.ok(currentUser).build();
        } catch (Exception e) {
            log.info("Error retrieving token owner", e);
        }

        return Response.status(NOT_FOUND).build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{id}/syncRepoList")
    @AdminOnly
    public Response syncRepoList(@PathParam("id") int id) {
        final Organization organization = organizationService.get(id, false);
        try {
            repositoryService.syncRepositoryList(organization);
        } catch (SourceControlException e) {
            log.info("Could not refresh repository list", e);
        }
        return Response.noContent().build();
    }

    @GET
    @Path("/{id}/defaultgroups")
    @AdminOnly
    @Produces({MediaType.APPLICATION_JSON})
    public Response getDefaultGroups(@PathParam("id") int orgId) {
        final Map<String, Object> result = new HashMap<>();

        Organization organization = organizationService.get(orgId, false);
        try {
            // organization
            final Map<String, Object> organizationResult = new HashMap<>();
            result.put("organization", organizationResult);
            organizationResult.put("id", organization.getId());
            organizationResult.put("name", organization.getName());

            // groups
            final List<Map<String, Object>> groupsResult = new LinkedList<>();
            result.put("groups", groupsResult);
            for (Group group : organizationService.getGroupsForOrganization(organization)) {
                final Map<String, Object> groupView = new HashMap<>();
                groupView.put("slug", group.getSlug());
                groupView.put("niceName", group.getNiceName());
                groupView.put("selected", organization.getDefaultGroups().contains(group));
                groupsResult.add(groupView);
            }

            return Response.ok(result).build();

        } catch (SourceControlException.Forbidden_403 e) {
            return buildErrorResponse(Response.Status.FORBIDDEN, "Unable to access Bitbucket");

        } catch (SourceControlException e) {
            return buildErrorResponse(Response.Status.INTERNAL_SERVER_ERROR,
                    "Error retrieving list of groups for " + organization.getOrganizationUrl()
                            + ". Please check JIRA logs for details.");
        }
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @AdminOnly
    public Organization createOrganization(Organization org) {
        return organizationService.save(org);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @AdminOnly
    public List<Organization> getOrganizations() {
        return organizationService.getAll(true);
    }

    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)
    @AdminOnly
    public Response findOrganization(@QueryParam("name") final String name, @QueryParam("type") final String type) {
        return organizationService.getAll(false, type).stream()
                .filter(org -> org.getName().equals(name))
                .findFirst()
                .map(org -> Response.ok(org).build())
                .orElseGet(() -> Response.status(NOT_FOUND).build());
    }

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    @AdminOnly
    public Organization getOrganization(@PathParam("id") final int id) {
        return organizationService.get(id, true);
    }

    @POST
    @Path("/{id}/credential")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @AdminOnly
    public Response setOrganizationCredential(@PathParam("id") int id, final CredentialJson newCredentialRaw) {
        Credential newCredential = CredentialFactory.buildCredential()
                .setKey(newCredentialRaw.getKey())
                .setPassword(newCredentialRaw.getPassword())
                .setPrincipalId(newCredentialRaw.getPrincipalId())
                .setSecret(newCredentialRaw.getSecret())
                .setToken(newCredentialRaw.getToken())
                .setUsername(newCredentialRaw.getUsername())
                .build();

        Organization organization = organizationService.updateCredentials(id, newCredential);
        if (organization == null) {
            return Response
                    .status(NOT_FOUND)
                    .entity(i18nHelper.getText(NO_SUCH_ORG_MESSAGE_KEY, id))
                    .build();
        }
        return Response.ok().build();
    }

    @POST
    @Path("/{id}/autosettings")
    @Consumes({MediaType.APPLICATION_JSON})
    @AdminOnly
    public Response updateAutolinkAndSmartCommits(@PathParam("id") int id, final OrganizationAutoSettingsJson settings) {
        organizationService.setAutolinkAndSmartcommits(id, settings.isEnableAutolink(), settings.isEnableSmartCommits());
        return Response.noContent().build();
    }

    @DELETE
    @Path("/{id}")
    @AdminOnly
    public Response deleteOrganization(@PathParam("id") int id,
                                       @QueryParam("allowIntegratedAccountRemoval") @DefaultValue("false") boolean allowIntegratedAccountRemoval) {
        if (!allowIntegratedAccountRemoval) {
            Organization integratedAccount = organizationService.findIntegratedAccount();
            if (integratedAccount != null && id == integratedAccount.getId()) {
                log.debug("Organization {} is an integrated account. Not deleting.", id);
                return buildErrorResponse(Response.Status.INTERNAL_SERVER_ERROR, "Failed to delete integrated account.");
            }
        }

        if (organizationService.get(id, false) == null) {
            log.debug("No Organization found with ID {}.", id);
            return Response.status(NOT_FOUND).build();
        }

        try {
            organizationService.remove(id);
        } catch (Exception e) {
            log.error("Failed to remove account with id " + id, e);
            return buildErrorResponse(Response.Status.INTERNAL_SERVER_ERROR, "Failed to delete account.");
        }

        log.debug("Successfully removed Organization {}.", id);
        return Response.noContent().build();
    }

    @DELETE
    @AdminOnly
    public Response deleteAllOrganisations() {
        organizationService.removeAll();
        return Response.noContent().build();
    }

    /**
     * Changes Organization's ApprovalState to APPROVED. This endpoint is usually used when JIRA Admin
     * wants to approve an Organization that was previously in PENDING_APPROVAL state.
     * This will also trigger "Repository List Sync" on target Organization.
     *
     * @param id of Organization to approve
     * @see com.atlassian.jira.plugins.dvcs.model.Organization.ApprovalState
     * <p>
     * response.representation.204.doc
     * Returned if Organization approval succeeded.
     * <p>
     * response.representation.404.doc
     * Returned when there is no Organization with provided id
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{id}/approve")
    @AdminOnly
    public Response approveOrganization(@PathParam("id") int id,
                                        @QueryParam("approvalLocation") String approvalLocationText) {

        final OrganizationApprovalLocation approvalLocation = parseApprovalLocation(approvalLocationText);

        try {
            final Organization organization = organizationService.changeOrganizationApprovalState(id, APPROVED);
            analyticsService.publishOrganizationApproved(organization, approvalLocation);
        } catch (NotFoundException e) {
            analyticsService.publishOrganizationApprovalFailed(id, approvalLocation,
                    ORGANIZATION_ALREADY_REMOVED);
            throw e;
        } catch (RuntimeException e) {
            analyticsService.publishOrganizationApprovalFailed(id, approvalLocation,
                    UNKNOWN_ERROR);
            throw e;
        }
        return Response.noContent().build();
    }

    private OrganizationApprovalLocation parseApprovalLocation(final String approvalLocationText) {
        if (StringUtils.isBlank(approvalLocationText)) {
            return UNKNOWN;
        }

        try {
            return OrganizationApprovalLocation.valueOf(approvalLocationText);
        } catch (IllegalArgumentException e) {
            return UNKNOWN;
        }
    }

    /**
     * Changes an Organization's ApprovalState to PENDING. This endpoint is provided for testing purposes.
     *
     * @param id The id of the organization to approve
     * @see #approveOrganization(int, String)
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/{id}/pending")
    @AdminOnly
    public Response pendOrganization(@PathParam("id") int id) {
        organizationService.changeOrganizationApprovalState(id, PENDING_APPROVAL);
        return Response.noContent().build();
    }

    /**
     * Changes all Organizations ApprovalState to PENDING. This endpoint is provided for testing purposes.
     *
     * @see #approveOrganization(int, String)
     */
    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/pending")
    @AdminOnly
    public Response pendAllOrganization() {
        final List<Organization> orgs = organizationService.getAll(false);

        for (Organization org : orgs) {
            organizationService.changeOrganizationApprovalState(org.getId(), PENDING_APPROVAL);
        }
        return Response.noContent().build();
    }
}
