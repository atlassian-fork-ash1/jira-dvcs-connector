package com.atlassian.jira.plugins.dvcs.rest.internal.v1;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.plugins.dvcs.rest.common.StringElement;
import com.atlassian.jira.plugins.dvcs.rest.security.AdminOnly;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Resource that allows admin users to
 */
@AdminOnly
@Internal
@Path("/configuration")
@Consumes(APPLICATION_JSON)
@ParametersAreNonnullByDefault
public class ConfigurationResource {

    @GET
    @Produces(APPLICATION_JSON)
    @Path("/bitbucket/hostUrl")
    public Response whereIsBitbucket() {
        Response response = Response.ok(StringElement.of(BitbucketDetails.getHostUrl())).build();
        return response;
    }

    @PUT
    @Produces(APPLICATION_JSON)
    @Path("/bitbucket/hostUrl")
    public Response hereIsBitbucket(final StringElement hostUrl) {
        BitbucketDetails.setHostUrl(hostUrl.getValue());
        return Response.noContent().build();
    }

    @DELETE
    @Produces(APPLICATION_JSON)
    @Path("/bitbucket/hostUrl")
    public Response clearBitbucketConfiguration() {
        BitbucketDetails.resetHostUrl();
        return Response.noContent().build();
    }
}
