package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.querydsl.v3.QIssueToChangesetMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QOrganizationMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QOrganizationToProjectKeyMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QRepositoryMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QRepositoryToChangesetMapping;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementation of some of the DAO logic from {@link com.atlassian.jira.plugins.dvcs.dao.OrganizationDao} which
 * the main DAO can delegate to
 */
@Named
public class OrganizationQueryDslFacade {

    private final DatabaseAccessor databaseAccessor;

    @Inject
    public OrganizationQueryDslFacade(final DatabaseAccessor databaseAccessor) {
        this.databaseAccessor = checkNotNull(databaseAccessor);
    }

    public Collection<String> getAllProjectKeysFromChangesetsInOrganization(final int orgId) {
        return databaseAccessor.runInTransaction(connection -> {
            final QIssueToChangesetMapping issueToChangesetMapping = new QIssueToChangesetMapping();
            final QRepositoryToChangesetMapping repositoryToChangesetMapping = new QRepositoryToChangesetMapping();
            final QRepositoryMapping repositoryMapping = new QRepositoryMapping();
            final QOrganizationMapping organizationMapping = new QOrganizationMapping();

            return connection.select(issueToChangesetMapping.PROJECT_KEY)
                    .from(issueToChangesetMapping)
                    .join(repositoryToChangesetMapping).on(issueToChangesetMapping.CHANGESET_ID.eq(repositoryToChangesetMapping.CHANGESET_ID))
                    .join(repositoryMapping).on(repositoryToChangesetMapping.REPOSITORY_ID.eq(repositoryMapping.ID))
                    .join(organizationMapping).on(repositoryMapping.ORGANIZATION_ID.eq(organizationMapping.ID))
                    .where(organizationMapping.ID.eq(orgId)
                            .and(repositoryMapping.LINKED.eq(true)))
                    .distinct()
                    .fetch();
        });

    }

    public Iterable<String> getCurrentlyLinkedProjects(final int orgId) {
        return databaseAccessor.runInTransaction(connection -> {
            final QOrganizationToProjectKeyMapping organizationToProjectKeyMapping = new QOrganizationToProjectKeyMapping();
            final QOrganizationMapping organizationMapping = new QOrganizationMapping();

            return connection.select(organizationToProjectKeyMapping.PROJECT_KEY)
                    .from(organizationToProjectKeyMapping)
                    .join(organizationMapping)
                    .on(organizationToProjectKeyMapping.ORGANIZATION_ID.eq(organizationMapping.ID))
                    .where(organizationMapping.ID.eq(orgId))
                    .distinct().fetch();
        });
    }

    public long linkProject(final int orgId, final String projectKey) {
        return databaseAccessor.runInTransaction(connection -> {
            final QOrganizationToProjectKeyMapping organizationToProjectKeyMapping
                    = new QOrganizationToProjectKeyMapping();

            return connection.insert(organizationToProjectKeyMapping)
                    .set(organizationToProjectKeyMapping.ORGANIZATION_ID, orgId)
                    .set(organizationToProjectKeyMapping.PROJECT_KEY, projectKey)
                    .execute();
        });
    }

    public void updateLinkedProjects(final int orgId, final Iterable<String> newProjectKeys) {
        databaseAccessor.runInTransaction(connection -> {
            final QOrganizationToProjectKeyMapping organizationToProjectKeyMapping = new QOrganizationToProjectKeyMapping();
            return connection.delete(organizationToProjectKeyMapping)
                    .where(organizationToProjectKeyMapping.ORGANIZATION_ID.eq(orgId))
                    .execute();
        });

        for (String newProjectKey : newProjectKeys) {
            linkProject(orgId, newProjectKey);
        }
    }

    public void remove(final int organizationId) {
        databaseAccessor.runInTransaction(connection -> {
            final QOrganizationToProjectKeyMapping organizationToProjectKeyMapping = new QOrganizationToProjectKeyMapping();
            final QOrganizationMapping organizationMapping = new QOrganizationMapping();

            connection.delete(organizationToProjectKeyMapping)
                    .where(organizationToProjectKeyMapping.ORGANIZATION_ID.eq(organizationId))
                    .execute();

            return connection.delete(organizationMapping)
                    .where(organizationMapping.ID.eq(organizationId))
                    .execute();
        });
    }
}
