package com.atlassian.jira.plugins.dvcs.service.api;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.plugins.dvcs.model.PullRequest;
import com.atlassian.jira.plugins.dvcs.model.Repository;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Set;

/**
 * Gets the pull requests for one or more issue keys or repository from connected dvcs account
 */
@PublicApi
public interface DvcsPullRequestService {
    /**
     * Retrieves keys of issues associated with the pull request. If either {@code repositoryId} or
     * {@code pullRequestId} point to non-existing entities, an empty set will be returned.
     *
     * @param repositoryId  id of the repository to query
     * @param pullRequestId id of the pull request to query
     * @return keys of issues associated with the pull request, or an empty set in case there were no matching issue
     * keys found.
     * @since v2.1.2
     */
    @Nonnull
    Set<String> getIssueKeys(int repositoryId, int pullRequestId);

    /**
     * Find all pullRequests by one or more issue keys
     *
     * @param issueKeys the list of issue keys to find
     * @return list of (@link PullRequest}
     */
    @Nonnull
    List<PullRequest> getPullRequests(Iterable<String> issueKeys);

    @Deprecated
    String getCreatePullRequestUrl(Repository repository, String sourceSlug, String sourceBranch, String destinationSlug, String destinationBranch, String eventSource);

    /**
     * Returns the location for the create pull request page for the specified repository, based on the specified
     * branch.
     *
     * @param repository   The repository where the pull request will be opened.
     * @param sourceBranch The branch which is the source of the pull request
     * @return URL to the create pull request page
     */
    String getCreatePullRequestUrl(@Nonnull Repository repository, @Nonnull final String sourceBranch);

    List<PullRequest> getPullRequests(Iterable<String> issueKeys, String dvcsType);
}
