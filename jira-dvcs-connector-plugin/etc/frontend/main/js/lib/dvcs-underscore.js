// Wrapper around the atlassian-jslibs provided underscore lib to hide versioning etc.
define('jira-dvcs-connector/lib/underscore', [
    'atlassian/libs/underscore-1.5.2'
], function (_) {
    return _;
});