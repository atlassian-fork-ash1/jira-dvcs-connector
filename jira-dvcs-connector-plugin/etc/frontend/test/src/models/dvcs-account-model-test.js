define([
    'fusion/test/qunit',
    'fusion/test/hamjest',
    'fusion/test/backbone'
], function (QUnit,
             __,
             Backbone) {

    QUnit.module('jira-dvcs-connector/bitbucket/models/dvcs-account-model', {
        require: {
            main: 'jira-dvcs-connector/bitbucket/models/dvcs-account-model',
            backbone: 'fusion/test/backbone'
        },
        mocks: {
            window: QUnit.moduleMock('jira-dvcs-connector/util/window', function () {
                return {
                    BASE_URL: 'test_base_url'
                }
            }),

            backbone: QUnit.moduleMock('jira-dvcs-connector/lib/backbone', function () {
                return Backbone;
            })
        }
    });

    QUnit.test('should wrap org-data correctly', function (assert, DVCSOrgModel) {
        var orgId = 'my-org-id';
        var orgName = 'my-org-name';
        var smartCommits = true;
        var reposDefaultEnabled = false;
        var model = new DVCSOrgModel({
            id: orgId,
            name: orgName,
            smartCommitsDefault: smartCommits,
            reposDefaultEnabled: reposDefaultEnabled
        });

        assert.assertThat("get id should return correct id", model.getId(), __.equalTo(orgId));
        assert.assertThat("get url should return correct url", model.url(), __.equalTo(this.mocks.window.BASE_URL + '/rest/bitbucket/1.0/organization/' + orgId));
        assert.assertThat("get name return return correct name", model.getName(), __.equalTo(orgName));

        assert.assertThat("isSmartCommitsDefaultEnabled returns correct value", model.isSmartCommitsDefaultEnabled(), __.equalTo(smartCommits));
        assert.assertThat("isReposDefaultEnabled returns correct value", model.isReposDefaultEnabled(), __.equalTo(reposDefaultEnabled));
    });
});