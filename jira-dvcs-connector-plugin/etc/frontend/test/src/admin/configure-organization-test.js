define([
    'fusion/test/qunit',
    'fusion/test/hamjest',
    'jquery',
    'fusion/test/mock-declaration',
    'fusion/test/mock-view'
], function (QUnit,
             __,
             $,
             MockDeclaration,
             MockView
){

    var orgDivsSnippet = `
        <div id="organization-list">
            <div id="dvcs-orgdata-container-1"
                class="dvcs-orgdata-container bottom "
                data-org-principal-id="{111-111-111}"
                data-approval-state="APPROVED"
                data-org-name="org1"
                data-org-id="1">
            </div>
            <div id="dvcs-orgdata-container-2"
                class="dvcs-orgdata-container bottom "
                data-org-principal-id="{222-222-222}"
                data-approval-state="APPROVED"
                data-org-name="org2"
                data-org-id="2">
            </div>
        </div>`;

    QUnit.module('jira-dvcs-connector/admin/configure-organization', {
        require: {
            main: 'jira-dvcs-connector/admin/configure-organization'
        },
        mocks: {
            navigate: QUnit.moduleMock('jira-dvcs-connector/util/navigate',
                new MockDeclaration([
                    'getIdToScrollToPostRefreshIfExists',
                    'setIdToScrollPostRefresh',
                    'reload',
                    'getQueryParams'
                ])
            ),
            auiMessages: QUnit.moduleMock('jira-dvcs-connector/aui/messages',
                new MockDeclaration([
                    'error',
                    'warning'
                ])
            ),
            jquery: QUnit.moduleMock('jquery', function () {
                return $;
            }),
            console: QUnit.moduleMock('jira-dvcs-connector/util/console',
                new MockDeclaration(['log'])
            ),
            contextPath: QUnit.moduleMock('wrm/context-path', function () {
                return function() {
                    return 'context-path'
                };
            }),
            refreshAccountDialog: QUnit.moduleMock('jira-dvcs-connector/admin/view/refresh-account-dialog',
                new MockDeclaration([], new MockView()).withConstructor()
            ),
            connectionSuccessfulDialog: QUnit.moduleMock('jira-dvcs-connector/bitbucket/views/connection-successful',
                new MockDeclaration([], new MockView()).withConstructor()
            ),
            featureDiscovery: QUnit.moduleMock('jira-dvcs-connector/feature-discovery/feature-discovery-view',
                new MockDeclaration([
                    'getCurrentPageIndex'
                ], new MockView()).withConstructor()
            ),
            dvcsConnectorRestClient: QUnit.moduleMock('jira-dvcs-connector/rest/dvcs-connector-rest-client',
                new MockDeclaration([
                    'organization.refreshRepositoryList',
                    'internal.user.setHasUserSeenFeatureDiscovery'
                ]).withConstructor()
            ),
            analyticsClient: QUnit.moduleMock('jira-dvcs-connector/analytics/analytics-client',
                new MockDeclaration([
                    'fireConnectOrganizationNavigatedByPrincipal',
                    'fireFeatureDiscoveryStarted',
                    'fireFeatureDiscoveryCompleted',
                    'fireFeatureDiscoveryAborted'
                ]).withConstructor()
            ),
            notification: QUnit.moduleMock('jira-dvcs-connector/admin/dvcs-notifications',
                new MockDeclaration([
                'showError',
                'showWarning',
                'showAuiMessage'
                ])
            ),
            localSettings: QUnit.moduleMock('jira-dvcs-connector/util/settings',
                new MockDeclaration([
                    'shouldShowFeatureDiscovery',
                    'setFeatureDiscoveryShown'
                ]).withConstructor()
            )
        },
        beforeEach: function() {
            _setShowFeatureDiscovery(this, true);
            _setRefreshRepositoryListSucceed(this);

            this.fixture.append($(orgDivsSnippet));
            this.mocks.dvcsConnectorRestClient.answer.internal.user.setHasUserSeenFeatureDiscovery.returns($.Deferred().resolve());
        }
    });

    // ======================
    // Tests
    // ======================

    QUnit.test('Priority given to navigate params if available',
        function (assert, configureOrganization) {
            // setup
            this.mocks.navigate.getIdToScrollToPostRefreshIfExists.returns('dvcs-orgdata-container-1');

            // execute
            var result = configureOrganization.getOrganizationElementToScrollTo();

            // verify
            assert.assertThat('ID to scroll to should be', result.attr('id'), __.is('dvcs-orgdata-container-1'));
        }
    );

    QUnit.test('Data element is used if available',
        function (assert, configureOrganization) {
            // setup
            this.mocks.navigate.getQueryParams.returns({principalUuid: '{222-222-222}'});

            // execute
            var result = configureOrganization.getOrganizationElementToScrollTo();

            // verify
            assert.assertThat('ID to scroll to should be', result.attr('id'), __.equalTo('dvcs-orgdata-container-2'));
            _assertAnalyticsFired(assert, this.mocks, 'fireConnectOrganizationNavigatedByPrincipal');
        }
    );

    QUnit.test('No parameter or data attribute returns undefined',
        function (assert, configureOrganization) {
            // setup
            this.mocks.navigate.getIdToScrollToPostRefreshIfExists.returns(undefined);
            this.mocks.navigate.getQueryParams.returns({});

            // execute
            var result = configureOrganization.getOrganizationElementToScrollTo();

            // verify
            assert.assertThat('id to scroll to should be', result, __.undefined());
        }
    );

    QUnit.test('Show feature discovery deferred resolves when feature discovery shouldnt show',
        function (assert, configureOrganization) {
            // setup
            _setShowFeatureDiscovery(this, false);

            // execute
            var d = configureOrganization.showFeatureDiscovery();

            // verify
            _assertDeferredResolved(assert, d);
            _assertFeatureDiscoveryDialogShown(assert, this.mocks, false);
            _assertAnalyticsNotFired(assert, this.mocks, 'fireFeatureDiscoveryStarted');
            _assertAnalyticsNotFired(assert, this.mocks, 'fireFeatureDiscoveryCompleted');
            _assertAnalyticsNotFired(assert, this.mocks, 'fireFeatureDiscoveryAborted');
        }
    );

    QUnit.test('Show feature discovery deferred resolves when feature discovery closed',
        function (assert, configureOrganization) {
            // execute
            var d = configureOrganization.showFeatureDiscovery();

            _triggerFeatureDiscoveryEvent(this, 'closed');

            // verify
            assert.assertThat('Local settings should have been updated',
                this.mocks.localSettings.answer.setFeatureDiscoveryShown, __.calledOnce());

            assert.assertThat('Remote flag should have been updated',
                this.mocks.dvcsConnectorRestClient.answer.internal.user.setHasUserSeenFeatureDiscovery, __.calledOnce());

            _assertDeferredResolved(assert, d);
            _assertFeatureDiscoveryDialogShown(assert, this.mocks, true);
            _assertAnalyticsFired(assert, this.mocks, 'fireFeatureDiscoveryStarted');
        }
    );

    QUnit.test('Show feature discovery emits correct analytics when tour finished',
        function (assert, configureOrganization) {
            // execute
            configureOrganization.showFeatureDiscovery();

            _triggerFeatureDiscoveryEvent(this, 'tourFinished');

            // verify
            _assertAnalyticsFired(assert, this.mocks, 'fireFeatureDiscoveryStarted');
            _assertAnalyticsFired(assert, this.mocks, 'fireFeatureDiscoveryCompleted');
            _assertAnalyticsNotFired(assert, this.mocks, 'fireFeatureDiscoveryAborted');
        }
    );

    QUnit.test('Show feature discovery emits correct analytics when tour aborted',
        function (assert, configureOrganization) {
            // execute
            configureOrganization.showFeatureDiscovery();

            _triggerFeatureDiscoveryEvent(this, 'tourAborted');

            // verify
            _assertAnalyticsFired(assert, this.mocks, 'fireFeatureDiscoveryStarted');
            _assertAnalyticsFired(assert, this.mocks, 'fireFeatureDiscoveryAborted');
            _assertAnalyticsNotFired(assert, this.mocks, 'fireFeatureDiscoveryCompleted');
        }
    );

    QUnit.test('Get organization name returns name when org is on page',
        function (assert, configureOrganization) {
            // execute
            var name = configureOrganization._findOrganizationName(1);

            // verify
            assert.assertThat('Found the wrong name', name, __.is('org1'));
        }
    );

    QUnit.test('Get organization name returns null when org is not on page',
        function (assert, configureOrganization) {
            // execute
            var name = configureOrganization._findOrganizationName(100);

            // verify
            assert.assertThat('Found a name when it shouldnt have', name, __.is(null));
        }
    );

    QUnit.test('Is valid organization ID returns true when org is on page',
        function (assert, configureOrganization) {
            // execute
            var valid = configureOrganization._isValidOrganizationId(1);

            // verify
            assert.assertThat(valid, __.is(true));
        }
    );

    QUnit.test('Is valid organization ID returns false when org is not on page',
        function (assert, configureOrganization) {
            // execute
            var valid = configureOrganization._isValidOrganizationId(100);

            // verify
            assert.assertThat(valid, __.is(false));
        }
    );

    QUnit.test('Finish flow does nothing if not a valid org ID',
        function (assert, configureOrganization) {
            // execute
            configureOrganization.doFinishConnectionFlow(100);

            // verify
            _assertConnectionSuccessfulDialogShown(assert, this.mocks, false);
            _assertRefreshRepositoryListTriggered(assert, this.mocks, false);
            _assertFeatureDiscoveryDialogShown(assert, this.mocks, false);
            _assertRefreshRepositoryDialogShown(assert, this.mocks, false);
            _assertPageReloadTriggered(assert, this.mocks, false);
            _assertErrorMessageShown(assert, this.mocks, false);
        }
    );

    QUnit.test('Finish flow shows connection success if valid org ID',
        function (assert, configureOrganization) {
            // execute
            configureOrganization.doFinishConnectionFlow(1);

            // verify
            _assertConnectionSuccessfulDialogShown(assert, this.mocks, true);
        }
    );

    QUnit.test('Finish flow shows feature discovery on dismissal of connection success',
        function (assert, configureOrganization) {
            // execute
            configureOrganization.doFinishConnectionFlow(1);

            _dismissConnectionSuccessDialogAndSucceed(this);

            // verify
            _assertConnectionSuccessfulDialogShown(assert, this.mocks, true);
            _assertRefreshRepositoryListTriggered(assert, this.mocks, true);
            _assertFeatureDiscoveryDialogShown(assert, this.mocks, true);
        }
    );

    QUnit.test('Finish flow does not show feature discovery if auto settings update failed',
        function (assert, configureOrganization) {
            // execute
            configureOrganization.doFinishConnectionFlow(1);

            _dismissConnectionSuccessDialogAndFail(this);

            // verify
            _assertConnectionSuccessfulDialogShown(assert, this.mocks, true);
            _assertRefreshRepositoryListTriggered(assert, this.mocks, false);
            _assertFeatureDiscoveryDialogShown(assert, this.mocks, false);
            _assertRefreshRepositoryDialogShown(assert, this.mocks, false);
            _assertPageReloadTriggered(assert, this.mocks, false);
            _assertErrorMessageShown(assert, this.mocks, true);
        }
    );

    QUnit.test('Finish flow shows refresh dialog and refreshes when feature discovery closed',
        function (assert, configureOrganization) {
            // execute
            configureOrganization.doFinishConnectionFlow(1);

            _dismissConnectionSuccessDialogAndSucceed(this);
            _triggerFeatureDiscoveryEvent(this, 'closed');

            // verify
            _assertConnectionSuccessfulDialogShown(assert, this.mocks, true);
            _assertRefreshRepositoryListTriggered(assert, this.mocks, true);
            _assertFeatureDiscoveryDialogShown(assert, this.mocks, true);
            _assertRefreshRepositoryDialogShown(assert, this.mocks, true);
            _assertPageReloadTriggered(assert, this.mocks, true);
        }
    );

    QUnit.test('Finish flow does not refresh page if repo refresh failed',
        function (assert, configureOrganization) {
            // execute
            configureOrganization.doFinishConnectionFlow(1);

            _setRefreshRepositoryListFail(this);
            _dismissConnectionSuccessDialogAndSucceed(this);
            _triggerFeatureDiscoveryEvent(this, 'closed');

            // verify
            _assertConnectionSuccessfulDialogShown(assert, this.mocks, true);
            _assertRefreshRepositoryListTriggered(assert, this.mocks, true);
            _assertFeatureDiscoveryDialogShown(assert, this.mocks, true);
            _assertRefreshRepositoryDialogShown(assert, this.mocks, false);
            _assertPageReloadTriggered(assert, this.mocks, false);
            _assertErrorMessageShown(assert, this.mocks, true);
        }
    );

    // ======================
    // Helpers and assertions
    // ======================

    function _setRefreshRepositoryListSucceed(self) {
        self.mocks.dvcsConnectorRestClient.answer.organization.refreshRepositoryList.returns($.Deferred().resolve());
    }

    function _setRefreshRepositoryListFail(self) {
        self.mocks.dvcsConnectorRestClient.answer.organization.refreshRepositoryList.returns($.Deferred().reject());
    }

    function _setShowFeatureDiscovery(self, show) {
        self.mocks.localSettings.answer.shouldShowFeatureDiscovery.returns(show);
    }

    function _dismissConnectionSuccessDialogAndSucceed(self) {
        self.mocks.connectionSuccessfulDialog.getConstructorArgs()[0].onSuccess();
    }

    function _dismissConnectionSuccessDialogAndFail(self) {
        self.mocks.connectionSuccessfulDialog.getConstructorArgs()[0].onError();
    }

    function _triggerFeatureDiscoveryEvent(self, event) {
        self.mocks.featureDiscovery.answer.trigger(event);
    }

    function _assertErrorMessageShown(assert, mocks, shown) {
        assert.assertThat('Error msg should' + (shown ? '' : ' not') + ' have been shown',
            mocks.notification.showError, __.calledTimes(shown ? 1 : 0));
    }

    function _assertPageReloadTriggered(assert, mocks, triggered) {
        assert.assertThat('Page reload should' + (triggered ? '' : ' not') + ' have been triggered',
            mocks.navigate.reload, __.calledTimes(triggered ? 1 : 0));
    }

    function _assertRefreshRepositoryListTriggered(assert, mocks, triggered) {
        assert.assertThat('Refresh repository list should' + (triggered ? '' : ' not') + ' have been triggered',
            mocks.dvcsConnectorRestClient.answer.organization.refreshRepositoryList, __.calledTimes(triggered ? 1 : 0));
    }

    function _assertRefreshRepositoryDialogShown(assert, mocks, shown) {
        assert.assertThat('Refresh repository list dialog should' + (shown ? '' : ' not') + ' have been shown',
            mocks.refreshAccountDialog.answer.wasShown(), __.is(shown));
    }

    function _assertFeatureDiscoveryDialogShown(assert, mocks, shown) {
        assert.assertThat('Feature Discovery dialog should' + (shown ? '' : ' not') + ' have been shown',
            mocks.featureDiscovery.answer.wasShown(), __.is(shown));
    }

    function _assertConnectionSuccessfulDialogShown(assert, mocks, shown) {
        assert.assertThat('Connection Successful dialog should' + (shown ? '' : ' not') + ' have been shown',
            mocks.connectionSuccessfulDialog.answer.wasShown(), __.is(shown));
    }

    function _assertAnalyticsFired(assert, mocks, event) {
        assert.assertThat('Analytics event "' + event + '" should have fired',
            mocks.analyticsClient.answer[event], __.calledOnce());
    }

    function _assertAnalyticsNotFired(assert, mocks, event) {
        assert.assertThat('Analytics event "' + event + '" should not have been fired',
            mocks.analyticsClient.answer[event], __.notCalled());
    }

    function _assertDeferredResolved(assert, d) {
        assert.assertThat('Deferred should be resolved', d.state(), __.equalTo('resolved'));
    }
});