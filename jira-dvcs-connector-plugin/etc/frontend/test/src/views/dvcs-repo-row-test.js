define([
    'fusion/test/qunit',
    'fusion/test/jquery',
    'fusion/test/hamjest',
    'fusion/test/backbone'
], function (QUnit,
             $,
             __,
             Backbone) {

    const RepoModelMock = Backbone.Model.extend({
        disable(){},
        isBusy: () => false,
        hasPermissionError: () => false,
        getLastError: () => {},
        getWebhookCallbackUrl: () => {},
        isEnabled(){}
    });

    const templateSpy = sinon.spy(() => '<div class="aui-inline-dialog-contents">content</div>');
    const linkingErrorTemplateSpy = sinon.spy(() => '<h2>Error</h2><div id="error-message">error</div>');

    QUnit.module('jira-dvcs-connector/bitbucket/views/dvcs-repo-row-view', {
        require: {
            main: 'jira-dvcs-connector/bitbucket/views/dvcs-repo-row-view',
            backbone: 'fusion/test/backbone'
        },
        templates: {
            'dvcs.connector.plugin.soy.adminPermisionWarning': templateSpy,
            'dvcs.connector.plugin.soy.linkingUnlinkingError': linkingErrorTemplateSpy
        },
        mocks: {
            backbone: QUnit.moduleMock('jira-dvcs-connector/lib/backbone', function () {
                return Backbone;
            }),
            inline2: QUnit.moduleMock('aui/inline-dialog2', function () {
                return;
            })

        },
        beforeEach: function (assert, RowView) {
            this.fixture.append('<tr class="dvcs-repo-row">' +
                '<td class="dvcs-repo-close-col"><span class="repo-close"></span><div class="sync-spinner"></div></td>' +
                '<td><span class="repository-error-icon" /></td></tr>');

            this.view = new RowView({el: this.fixture, model: new RepoModelMock()});
        },

        afterEach: function () {
            templateSpy.reset();
            linkingErrorTemplateSpy.reset();
        }
    });


    QUnit.test('should hover repo properly', function (assert) {
        var $closeButton = this.fixture.find(this.view.closeButtonSelector);

        sinon.stub(RepoModelMock.prototype, 'isBusy', function () {
            return false;
        });
        this.view.showSettings({type: 'mouseenter'});
        assert.assertThat('close repo button should be visible', $closeButton.css('display'), __.not(__.equalTo('none')));

        this.view.showSettings({type: 'mouseleave'});
        assert.assertThat('close repo button should be hidden', $closeButton.css('display'), __.equalTo('none'));

        RepoModelMock.prototype.isBusy.restore();
        sinon.stub(RepoModelMock.prototype, 'isBusy', function () {
            return true;
        });
        this.view.showSettings({type: 'mouseenter'});
        assert.assertThat('close repo button should be hidden', $closeButton.css('display'), __.equalTo('none'));
    });

    QUnit.test('should close repo properly', function (assert) {
        $.fn.spin = sinon.spy();
        const repoModelDisableSpy = sinon.spy(RepoModelMock.prototype, 'disable');
        var $closeButton = this.fixture.find(this.view.closeButtonSelector);

        $closeButton.trigger('click');
        assert.assertThat('close repo button should be hidden', $closeButton.css('display'), __.equalTo('none'));
        assert.assertThat('should call call disable in the model', repoModelDisableSpy.callCount, __.equalTo(1));
        assert.assertThat('should show spinner', $.fn.spin.callCount, __.equalTo(1));
        $.fn.spin.reset();
    });

    QUnit.test('should add inline dialogue to repos with permission warning', function (assert, RowView) {

        const url = "dummyUrl";
        const repoId = 1234;

        var model = new RepoModelMock({id: repoId});
        sinon.stub(RepoModelMock.prototype, 'hasPermissionError', () => true);
        sinon.stub(RepoModelMock.prototype, 'getWebhookCallbackUrl', () => url);

        this.view = new RowView({el: this.fixture, model: model});

        assert.assertThat('inline dialogue template should be called', templateSpy.callCount, __.equalTo(1));
        assert.assertThat('template should be called with expected url and Id', templateSpy.firstCall.args[0], __.equalTo({
            url: url,
            id: repoId
        }));

        assert.assertThat('page should contain inline dialogue div', this.fixture.find('.aui-inline-dialog-contents').first().is(':visible'), __.truthy());
    });

    QUnit.test('should add inline dialogue to repos with linking error', function (assert, RowView) {

        const message = "error message";
        const repoId = 1234;

        var model = new RepoModelMock({id: repoId});
        sinon.stub(RepoModelMock.prototype, 'getLastError', () => new Object({responseText: JSON.stringify({message: message})}));
        sinon.stub(RepoModelMock.prototype, 'isEnabled', () => true);

        const inlineDialgShowSpy = sinon.spy();
        AJS.InlineDialog = () => {};
        sinon.stub(AJS, 'InlineDialog', () => new Object({show: inlineDialgShowSpy}));

        this.view = new RowView({el: this.fixture, model: model});

        assert.assertThat('inline Dialog show be called', inlineDialgShowSpy.callCount, __.equalTo(1));
        assert.assertThat('inline dialogue link error template should be called', linkingErrorTemplateSpy.callCount, __.equalTo(1));
        assert.assertThat('link error template should be called with expected Id, and islinking values', linkingErrorTemplateSpy.firstCall.args[0], __.equalTo({
            isLinking: true,
            errorMessage: message
        }));
    });
});
