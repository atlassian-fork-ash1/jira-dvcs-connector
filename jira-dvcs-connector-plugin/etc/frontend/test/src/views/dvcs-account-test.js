define([
    'fusion/test/qunit',
    'fusion/test/jquery',
    'fusion/test/hamjest',
    'fusion/test/backbone',
    'underscore'
], function (QUnit,
             $,
             __,
             Backbone,
             _) {
    "use strict";

    const templateSpy = sinon.stub();
    const confirmationDialogTemplateSpy = sinon.stub();

    const ReposTableMock = Backbone.View.extend({
        initialize: sinon.spy()
    });

    const AccountModelMock = Backbone.View.extend({
        hasLinkedRepositories: () => {},
        getName: () => {},
        isApproved: () => {},
        getId: () => {},
        setHasLinkedRepositories: sinon.spy()
    });

    const AdminPageOrgContainerMock = Backbone.View.extend({
        initialize: sinon.spy()
    });

    const mockedContextPath = sinon.spy();

    const RepoMultiSelectorMock = Backbone.View.extend({
        initialize: sinon.spy(),
        spin: sinon.spy(),
        spinStop: sinon.spy()
    });

    const RepoCollectionMock = Backbone.Collection.extend({
        initialize: sinon.spy(),
        hasEnabledRepos: () => {}
    });

    const RestClientMock = sinon.stub();
    const ConfirmationDialogMock = Backbone.View.extend({
        initialize: sinon.spy(),
        render: sinon.spy(),
        remove: sinon.spy()
    });

    const webhooksDialogMock = {
        showWebhooksDialog: sinon.spy()
    };

    const DVCSaccountSettingsMock = Backbone.View.extend({
        initialize: sinon.spy()
    });

    QUnit.module('jira-dvcs-connector/bitbucket/views/dvcs-account-view', {
        require: {
            main: 'jira-dvcs-connector/bitbucket/views/dvcs-account-view',
            jquery: 'fusion/test/jquery',
            backbone: 'fusion/test/backbone'
        },

        mocks: {
            jquery: QUnit.moduleMock('jquery', () => $),

            backbone: QUnit.moduleMock('jira-dvcs-connector/lib/backbone', () => Backbone),

            underscore: QUnit.moduleMock('unserscore', () => _),

            ReposTable: QUnit.moduleMock('jira-dvcs-connector/bitbucket/views/dvcs-repos-table-view', () => ReposTableMock),

            WebhooksDialog: QUnit.moduleMock('jira-dvcs-connector/bitbucket/views/webhooks-dialog', () => webhooksDialogMock),

            AdminPageOrgContainer: QUnit.moduleMock('jira-dvcs-connector/bitbucket/views/bitbucket-admin-page-org-container-view',
                    () => AdminPageOrgContainerMock),

            contextPath: QUnit.moduleMock('wrm/context-path', () => mockedContextPath),

            RepoMultiSelectorMock: QUnit.moduleMock('jira-dvcs-connector/bitbucket/views/dvcs-repo-multi-selector', () => RepoMultiSelectorMock),

            RepoCollection: QUnit.moduleMock('jira-dvcs-connector/bitbucket/models/dvcs-repo-collection', () => RepoCollectionMock),

            RestClient: QUnit.moduleMock('jira-dvcs-connector/rest/dvcs-connector-rest-client', () => RestClientMock),

            confirmationDialog: QUnit.moduleMock('jira-dvcs-connector/bitbucket/views/confirmation-dialog', () => ConfirmationDialogMock),

            Constants: QUnit.moduleMock('jira-dvcs-connector/util/constants', () => new Object({
                REPOSITORIES_CHUNK_SIZE : 100,
                REPOSITORIES_RENDERING_INTERVAL: 1
            })),

            DvcsAccountSettings: QUnit.moduleMock('jira-dvcs-connector/bitbucket/views/dvcs-account-settings', () => DVCSaccountSettingsMock),
        },

        templates: {
            'dvcs.connector.plugin.soy.dvcs.accounts.emptyAccount': templateSpy,
            'dvcs.connector.plugin.soy.confirmDelete': confirmationDialogTemplateSpy
        },

        setupDefaultModelMocks() {
            this.modelHasEnabledReposStub = sinon.stub(AccountModelMock.prototype, 'hasLinkedRepositories');
            this.modelIsApproved = sinon.stub(AccountModelMock.prototype, 'isApproved');
            this.modelGetName = sinon.stub(AccountModelMock.prototype, 'getName');
            this.modelGetId = sinon.stub(AccountModelMock.prototype, 'getId');

            this.modelHasEnabledReposStub.returns(false);
            this.modelIsApproved.returns(true);
        },

        setupRestClientMock() {
            this.ajaxResultStub = {
                done: () => new Object({fail: () => {}})
            };

            RestClientMock.prototype.organization = {
                getRepositories: sinon.stub(),
                remove: sinon.stub()
            };

            RestClientMock.prototype.organization.getRepositories.returns(this.ajaxResultStub);
            RestClientMock.prototype.organization.remove.returns(this.ajaxResultStub);
        },

        setupRepoCollectionMock() {
            this.repoCollectionHasEnabledReposStub = sinon.stub(RepoCollectionMock.prototype, 'hasEnabledRepos');
            this.repoCollectionHasEnabledReposStub.returns(true);
        },

        setupFixture() {
            this.$el = $('<div class="the-view-el"></div>');
            this.fixture.append(this.$el);
            this.$el.append('<div class="dvcs-header-container expandable"><h4></h4><a class="dvcs-control-delete-org"></a></div>');
            this.$el.append('<div id="dvcs-account-repos-N" data-sync-disabled="false"><form id="fake-form">' +
                '<table class="dvcs-repos-table"></table></form></div>');
            this.$el.append('<div class="no-enabled-repos"></div>');
            this.$el.append('<select><options></options></select>');
            this.$el.append('<button class="addDvcsRepoButton"></button>');
        },

        resetConfirmationDialogMocks() {
            ConfirmationDialogMock.prototype.initialize.reset();
            ConfirmationDialogMock.prototype.render.reset();
            ConfirmationDialogMock.prototype.remove.reset();
        },

        restoreModelMocks() {
            this.modelHasEnabledReposStub.restore();
            this.modelIsApproved.restore();
            this.modelGetName.restore();
            this.modelGetId.restore();
        },

        resetTemplatesMocks() {
            templateSpy.reset();
            confirmationDialogTemplateSpy.reset();
        },

        resetDVCSAccountViewMock() {
            DVCSaccountSettingsMock.prototype.initialize.reset();
        },

        beforeEach: function () {
            templateSpy.returns('<div id="empty-div"></div>');

            this.setupFixture();
            this.setupDefaultModelMocks();
            this.setupRestClientMock();
            this.setupRepoCollectionMock();

            $.fn.spin = sinon.spy();
            $.fn.spinStop = sinon.spy();
        },

        afterEach: function () {

            this.restoreModelMocks();
            this.resetConfirmationDialogMocks();
            this.resetTemplatesMocks();
            this.resetDVCSAccountViewMock();

            this.repoCollectionHasEnabledReposStub.restore();
            $.fn.spin.reset();
            $.fn.spinStop.reset();

            // reset initialize spies
            RepoMultiSelectorMock.prototype.initialize.reset();
            RepoCollectionMock.prototype.initialize.reset();
            ReposTableMock.prototype.initialize.reset();
            AdminPageOrgContainerMock.prototype.initialize.reset();
            mockedContextPath.reset();
            RestClientMock.reset();
            webhooksDialogMock.showWebhooksDialog.reset();
        }

    });

    QUnit.test('should create all required views and models', function (assert, DVCSAccountView) {

        const model = new AccountModelMock();

        const view = new DVCSAccountView({
            el: this.$el,
            model: model
        });
        view.render();

        assert.assertThat('should call create instance of repo collection', RepoCollectionMock.prototype.initialize.callCount, __.equalTo(1));

        const collectionInstance = RepoCollectionMock.prototype.initialize.thisValues[0];

        assert.assertThat('should call create instance of repo multi selector view', RepoMultiSelectorMock.prototype.initialize.callCount, __.equalTo(1));
        assert.assertThat('should pass repo collection and correct dom selector to mutli selector view',
            RepoMultiSelectorMock.prototype.initialize.firstCall.args[0], __.hasProperties({
                el: this.$el.find('form'),
                collection: collectionInstance
            }));

        assert.assertThat('should call create instance of repo table view', ReposTableMock.prototype.initialize.callCount, __.equalTo(1));
        assert.assertThat('shoud pass repo collection and correct dom selector to repo table view',
            ReposTableMock.prototype.initialize.firstCall.args[0], __.hasProperties({
                el: __.containsString('.dvcs-repos-table'),
                collection: collectionInstance,
                syncDisabled: this.$el.find('#dvcs-account-repos-N').data('sync-disabled'),
                accountModel: model
            }));

        assert.assertThat('should call create instance of admin page org container view', AdminPageOrgContainerMock.prototype.initialize.callCount, __.equalTo(1));
        assert.assertThat('should call create instance of dvcs account settings', DVCSaccountSettingsMock.prototype.initialize.callCount, __.equalTo(1))

        assert.assertThat('should call create instance of rest client', RestClientMock.calledWithNew(), __.equalTo(true));
    });

    QUnit.test('should render empty state when no repo is enabled', function (assert, DVCSAccountView) {
        const model = new AccountModelMock();

        const view = new DVCSAccountView({
            el: this.$el,
            model: model
        });
        view.render();

        assert.assertThat('should call hasLinkedRepositories on passed model', this.modelHasEnabledReposStub.calledOn(model), __.equalTo(true));
        assert.assertThat('should render empty state when account is approved and has no linked repos', templateSpy.callCount, __.equalTo(1));
        assert.assertThat('should render empty state when account is approved and has no linked repos',
            this.$el.find('.no-enabled-repos').find('div#empty-div').length, __.equalTo(1));
    });

    QUnit.test('should not render empty state when no repo is enabled but account is not approved', function (assert, DVCSAccountView) {
        const model = new AccountModelMock();

        const view = new DVCSAccountView({
            el: this.$el,
            model: model
        });

        this.modelIsApproved.returns(false);
        view.render();

        assert.assertThat('should call hasLinkedRepositories on passed model', this.modelHasEnabledReposStub.calledOn(model), __.equalTo(true));
        assert.assertThat('should not render empty state when account is not approved and has no linked repos', templateSpy.callCount, __.equalTo(0));
        assert.assertThat('should not render empty state when account is not approved and has no linked repos',
            this.$el.find('.no-enabled-repos').find('div#empty-div').length, __.equalTo(0));
    });

    QUnit.test('should show spinner without empty state when account has enabled repos until repos async load is done', function (assert, DVCSAccountView) {
        const model = new AccountModelMock();

        const stub = sinon.stub(this.ajaxResultStub, 'done');
        stub.returns({fail: sinon.spy()});

        const view = new DVCSAccountView({
            el: this.$el,
            model: model
        });

        this.modelHasEnabledReposStub.returns(true);
        this.modelIsApproved.returns(true);
        view.render();

        assert.assertThat('should call hasLinkedRepositories on passed model', this.modelHasEnabledReposStub.calledOn(model), __.equalTo(true));
        assert.assertThat('should not render empty state when account has enabled repos', templateSpy.callCount, __.equalTo(0));
        assert.assertThat('should show spinner until repo async load is done', $.fn.spin.callCount, __.equalTo(1));
        assert.assertThat('should not stop spinning until async call returned', $.fn.spinStop.callCount, __.equalTo(0));

        stub.callArgWith(0, [{id: 1, name: 'repo1'}]);
        assert.assertThat('should stop spinning on ajax return', $.fn.spinStop.callCount, __.equalTo(1));
        assert.assertThat('should not render empty state when account has enabled repos',
            this.$el.find('.no-enabled-repos').find('div#empty-div').length, __.equalTo(0));
    });

    QUnit.test('should add repos to repo collection after async call', function (assert, DVCSAccountView) {
        const model = new AccountModelMock();

        const addSpy = sinon.spy(RepoCollectionMock.prototype, 'add');

        const stub = sinon.stub(this.ajaxResultStub, 'done');
        stub.returns({fail: sinon.spy()});

        const view = new DVCSAccountView({
            el: this.$el,
            model: model
        });
        view.render();

        assert.assertThat('should not add any repo to collection', addSpy.callCount, __.equalTo(0));

        const repoArray = [{id: 1, name: 'repo1', linked:true}, {id: 2, name:'repo2', linked:true}];
        stub.callArgWith(0, repoArray);

        assert.assertThat('should call add repo to collection', addSpy.called, __.equalTo(true));
        const allAddedRepos = [];
        addSpy.args.map((args) => args[0]).forEach((arr => allAddedRepos.push(...arr)));
        assert.assertThat('should call add repo to collection with returned repos', allAddedRepos, __.equalTo(repoArray));
    });


    QUnit.test('should render empty state when last connected repo is deleted', function(assert, DVCSAccountView) {
        const model = new AccountModelMock();

        const stub = sinon.stub(this.ajaxResultStub, 'done');
        stub.returns({fail: sinon.spy()});

        const view = new DVCSAccountView({
            el: this.$el,
            model: model
        });

        this.modelHasEnabledReposStub.returns(true);
        this.modelIsApproved.returns(true);

        view.render();

        const repoArray = [{id: 1, name: 'repo1'}, {id: 2, name:'repo2'}];
        stub.callArgWith(0, repoArray);

        assert.assertThat('empty state should not be called when there is at least one connected repo', templateSpy.callCount, __.equalTo(0));
    });

    QUnit.test('should show webhooks warning dialog', function(assert, DVCSAccountView) {
        const model = new AccountModelMock();
        const view = new DVCSAccountView({
            el: this.$el,
            model: model
        });
        view.render();

        const multiSelector = RepoMultiSelectorMock.prototype.initialize.thisValues[0];

        const repoWithWarning = {
            id: 101,
            getName: () => 'warnRepo',
            getLastRegistration: () => new Object({callBackUrlInstalled: false, callBackUrl: 'http://example.com'})

        };
        const repoWithoutWarning = {
            id: 202,
            getName: () => 'repo',
            getLastRegistration: () => new Object({callBackUrlInstalled: true})
        };
        assert.assertThat('should not show webhook dialog until multi selector event is triggered', webhooksDialogMock.showWebhooksDialog.callCount, __.equalTo(0));

        multiSelector.trigger('jira-dvcs-connector:repos-batch-added', [repoWithWarning, repoWithoutWarning]);

        assert.assertThat('should show webhook dialog when multi selector event is triggered with repo with webhook warning',
            webhooksDialogMock.showWebhooksDialog.callCount, __.equalTo(1));

        assert.assertThat('should show webhook dialog with only the repo with webhook warning',
            webhooksDialogMock.showWebhooksDialog.firstCall.args[1], __.equalTo({
                101: {"repoName":"warnRepo","callBackUrl":"http://example.com"}
            }));

        webhooksDialogMock.showWebhooksDialog.reset();

        const repo3 = {
            id: 303,
            getName: () => 'repo3',
            getLastRegistration: () => new Object({callBackUrlInstalled: true})

        };
        multiSelector.trigger('jira-dvcs-connector:repos-batch-added', [repo3]);

        assert.assertThat('should not show webhook dialog no added repo has webhook warning', webhooksDialogMock.showWebhooksDialog.callCount, __.equalTo(0));

    });

    QUnit.test('should render account body and hide it after clicking on the header', function (assert, DVCSAccountView) {
        const model = new AccountModelMock();
        const view = new DVCSAccountView({
            el: this.$el,
            model: model
        });
        view.render();

        assert.assertThat('account body should be visible', this.$el.find('div[id^="dvcs-account-repos-"]').is(':visible'), __.truthy());
        assert.assertThat('account icon should be expanded', this.$el.find('.dvcs-header-container h4').hasClass('aui-iconfont-expanded'), __.truthy());

        this.$el.find('.dvcs-header-container').trigger('click');

        assert.assertThat('account body should not be visible', this.$el.find('div[id^="dvcs-account-repos-"]').is(':visible'), __.falsy());
        assert.assertThat('account icon should be collapsed', this.$el.find('.dvcs-header-container h4').hasClass('aui-iconfont-collapsed'), __.truthy());
    });


    QUnit.test('should always show account body', function (assert, DVCSAccountView) {
        this.$el.find('.dvcs-header-container').remove();
        this.$el.append('<div class="dvcs-header-container"><h4></h4></div>');

        const model = new AccountModelMock();
        const view = new DVCSAccountView({
            el: this.$el,
            model: model
        });
        view.render();

        this.$el.find('.dvcs-header-container').trigger('click');

        assert.assertThat('account body should be visible', this.$el.find('div[id^="dvcs-account-repos-"]').is(':visible'), __.truthy());
        assert.assertThat('account icon should not be expanded', this.$el.find('.dvcs-header-container h4').hasClass('aui-iconfont-expanded'), __.falsy());
        assert.assertThat('account icon should not be collapsed', this.$el.find('.dvcs-header-container h4').hasClass('aui-iconfont-collapsed'), __.falsy());
    });

    QUnit.test('should respond to pending org deleted', function (assert, DVCSAccountView) {
        const model = new AccountModelMock();
        AccountModelMock.prototype.toJSON = sinon.stub();
        AccountModelMock.prototype.toJSON.returns({json: 'json'});
        const view = new DVCSAccountView({
            el: this.$el,
            model: model
        });
        const listener = sinon.spy();
        view.on('jira-dvcs-connector:org-deleted', listener, this);
        view.render();

        const orgContainerInstance = AdminPageOrgContainerMock.prototype.initialize.thisValues[0];
        orgContainerInstance.trigger('jira-dvcs-connector:pending-org-deleted');

        assert.assertThat('el was removed', $.contains(document.documentElement, this.$el), __.falsy());
        assert.assertThat('We fired org deleted', listener.callCount, __.equalTo(1));
        assert.assertThat('We sent the write json payload with the event',
            listener.firstCall.args[0], __.equalTo({json: 'json'}));
    });

    QUnit.test('should respond to delete button click', function(assert, DVCSAccountView) {
        this.modelGetName.returns('repo1521');
        this.modelGetId.returns(1521);
        confirmationDialogTemplateSpy.returns('<div>body</div>');

        const model = new AccountModelMock();
        AccountModelMock.prototype.toJSON = sinon.stub();
        AccountModelMock.prototype.toJSON.returns({json: {id: 1521}});
        const view = new DVCSAccountView({
            el: this.$el,
            model: model
        });

        const listener = sinon.spy();
        view.on('jira-dvcs-connector:org-deleted', listener, this);
        view.render();

        this.$el.find('.dvcs-control-delete-org').trigger('click');

        assert.assertThat('should show confirmation dialog on delete', ConfirmationDialogMock.prototype.initialize.callCount, __.equalTo(1));
        assert.assertThat('should pass correct parameters to config dialog', ConfirmationDialogMock.prototype.initialize.firstCall.args[0],
            __.hasProperties({
                header: __.containsString('repo1521'),
                body: __.equalTo('<div>body</div>'),
                submitButtonLabel: __.truthy(),
                okAction: __.instanceOf(Function)
            }));
        assert.assertThat('should show confirmation dialog on delete', ConfirmationDialogMock.prototype.render.callCount, __.equalTo(1));

        assert.assertThat('should not delete account without confirmation', RestClientMock.prototype.organization.remove.callCount, __.equalTo(0));

        const stub = sinon.stub(this.ajaxResultStub, 'done');
        stub.returns({fail: sinon.spy()});

        const okCallback = ConfirmationDialogMock.prototype.initialize.firstCall.args[0].okAction;
        okCallback(ConfirmationDialogMock.prototype.initialize.thisValues[0]);

        assert.assertThat('should delete organization after confirmation', RestClientMock.prototype.organization.remove.callCount, __.equalTo(1));
        assert.assertThat('should pass correct organization id to delete fn', RestClientMock.prototype.organization.remove.firstCall.args, __.equalTo([1521]));

        stub.callArg(0);
        assert.assertThat('should close confirmation dialog after delete is done', ConfirmationDialogMock.prototype.remove.callCount, __.equalTo(1));
        assert.assertThat('We fired org deleted', listener.callCount, __.equalTo(1));
        assert.assertThat('We sent the write json payload with the event',
            listener.firstCall.args[0], __.equalTo({json: {id: 1521}}));
    });
});