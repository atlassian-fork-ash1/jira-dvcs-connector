package com.atlassian.jira.plugins.dvcs.pageobjects.page;

import com.atlassian.fusion.aci.api.feature.AciDarkFeatures;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.dvcs.pageobjects.component.BitBucketOrganization;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.Account;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.AccountRepository;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.ConnectionSuccessfulDialog;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.DvcsAccountsPage;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.FeatureDiscoveryDialog;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.plugins.dvcs.pageobjects.page.account.ConnectionSuccessfulDialog.CONNECTION_SUCCESSFUL_DIALOG_ID;
import static com.atlassian.jira.plugins.dvcs.pageobjects.page.account.FeatureDiscoveryDialog.FEATURE_DISCOVERY_DIALOG_ID;
import static com.atlassian.jira.plugins.dvcs.pageobjects.util.AciPageObjectUtils.oldFlowAddOrgButton;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.pageobjects.elements.timeout.TimeoutType.PAGE_LOAD;

/**
 * Represents the page to link repositories to projects
 */
public abstract class BaseConfigureOrganizationsPage implements Page {
    public static final String SYNC_FAILED_MESSAGE = "Sync Failed";
    protected JiraTestedProduct jiraTestedProduct;
    @Inject
    PageBinder pageBinder;
    @Inject
    PageElementFinder elementFinder;
    @Inject
    Backdoor backdoor;
    @ElementBy(id = "linkRepositoryButton")
    PageElement linkRepositoryButton;
    @ElementBy(id = "linkBitbucketAccountButton")
    PageElement linkBitbucketAccountButton;
    @ElementBy(id = "linkGithubAccountButton")
    PageElement linkGithubAccountButton;
    @ElementBy(className = "button-panel-submit-button")
    PageElement addOrgButton;
    @ElementBy(id = "aui-message-bar")
    PageElement messageBarDiv;
    @ElementBy(id = "organization")
    PageElement organization;
    @ElementBy(id = "organization-list", timeoutType = PAGE_LOAD)
    PageElement organizationsElement;
    @ElementBy(id = "autoLinking")
    PageElement autoLinkNewRepos;
    @ElementBy(id = "urlSelect")
    SelectElement dvcsTypeSelect;

    @Override
    public String getUrl() {
        return "/secure/admin/ConfigureDvcsOrganizations!default.jspa";
    }

    public List<BitBucketOrganization> getOrganizations() {
        final List<BitBucketOrganization> orgs = new ArrayList<>();

        for (final PageElement orgContainer : organizationsElement.findAll(By.className("dvcs-orgdata-container"))) {
            waitUntilTrue(orgContainer.find(By.className("dvcs-org-container")).timed().isVisible());

            orgs.add(pageBinder.bind(BitBucketOrganization.class, orgContainer));
        }

        return orgs;
    }

    public BaseConfigureOrganizationsPage deleteAllOrganizations() {
        List<BitBucketOrganization> orgs;
        while (!(orgs = getOrganizations()).isEmpty()) {
            orgs.get(0).delete();
        }

        return this;
    }

    protected void waitFormBecomeVisible() {
        waitUntilTrue(isFormOpen());
    }

    public abstract BaseConfigureOrganizationsPage addOrganizationSuccessfully(String organizationAccount,
                                                                               OAuthCredentials oAuthCredentials, boolean autosync, String username, String password);

    public void setJiraTestedProduct(JiraTestedProduct jiraTestedProduct) {
        this.jiraTestedProduct = jiraTestedProduct;
    }

    public String getRepositoryIdFromRepositoryName(String queriedRepositoryName) {
        for (PageElement repositoryRow : elementFinder.findAll(By.className("dvcs-repo-row"))) {
            String repositoryName = repositoryRow.find(By.className("dvcs-org-reponame")).find(By.tagName("a")).getText();

            if (repositoryName.equals(queriedRepositoryName)) {
                String id = repositoryRow.getAttribute("id");
                return id.replaceAll("dvcs-repo-row-", "");
            }
        }

        return null;
    }

    public TimedQuery<Boolean> isFormOpen() {
        return getForm().timed().isVisible();
    }

    public TimedQuery<String> getDvcsTypeSelectValue() {
        return dvcsTypeSelect.timed().getValue();
    }

    /**
     * Select the given DVCS type in the dropdown
     *
     * @param type One of "bitbucket", "github", "githube"
     */
    public void selectDvcsType(final String type) {
        dvcsTypeSelect.select(dvcsTypeSelect.getAllOptions()
                .stream()
                .filter(o -> o.value().equals(type))
                .findFirst()
                .orElse(null));
    }

    public TimedQuery<String> getFormAction() {
        return getForm().timed().getAttribute("action");
    }

    private PageElement getForm() {
        return elementFinder.find(By.id("repoEntry"));
    }

    /**
     * Enables and synchronizes given repository.
     *
     * @param accountType    type of account
     * @param accountName    account name
     * @param repositoryName repository name
     * @return the enabled repository
     */
    public AccountRepository enableAndSyncRepository(Account.AccountType accountType, String accountName, String repositoryName) {
        DvcsAccountsPage accountsPage = pageBinder.bind(DvcsAccountsPage.class);
        Account account = accountsPage.getAccount(accountType, accountName);

        final AccountRepository repository = account.enableRepository(repositoryName);
        repository.synchronize();

        return repository;
    }

    /**
     * Bind and return a ConnectionSuccessfulDialog to the current page
     *
     * @return A {@link ConnectionSuccessfulDialog} bound to the current page
     */
    public ConnectionSuccessfulDialog getConnectionSuccessfulDialog() {
        PageElement dialogElement = elementFinder.find(By.id(CONNECTION_SUCCESSFUL_DIALOG_ID), TimeoutType.DEFAULT);
        return pageBinder.bind(ConnectionSuccessfulDialog.class, dialogElement);
    }

    /**
     * Bind and return a FeatureDiscoveryDialog to the current page
     *
     * @return A {@link FeatureDiscoveryDialog} bound to the current page
     */
    public FeatureDiscoveryDialog getFeatureDiscoveryDialog(){
        PageElement dialogElement = elementFinder.find(By.id(FEATURE_DISCOVERY_DIALOG_ID), TimeoutType.DEFAULT);
        return pageBinder.bind(FeatureDiscoveryDialog.class, dialogElement);
    }

    /**
     * @return The button to use for adding a Github account
     */
    protected PageElement getLinkGithubAccountButton() {
        return isAciEnabled() ? linkGithubAccountButton : linkRepositoryButton;
    }

    /**
     * @return The button to use for adding a Bitbucket account
     */
    protected PageElement getLinkBitbucketAccountButton() {
        return oldFlowAddOrgButton(linkRepositoryButton, linkGithubAccountButton, isAciEnabled());
    }

    protected boolean isAciEnabled() {
        return backdoor.darkFeatures().isGlobalEnabled(AciDarkFeatures.ACI_ENABLED_FEATURE_KEY);
    }
}
