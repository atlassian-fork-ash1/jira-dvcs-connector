package com.atlassian.jira.plugins.dvcs.pageobjects.page;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.pageobjects.elements.timeout.TimeoutType.PAGE_LOAD;

public class BitbucketManageAddonsPage implements Page {
    private static final String EXPAND_TWIXIES_XPATH = "//ul[contains(@data-addons, 'JIRA')]//span[contains(@class, 'boxed-list--collapsed-icon')]";

    private static final String REMOVE_BUTTON_XPATH = "//ul[contains(@data-addons, 'JIRA')]//button[normalize-space(text()) = 'Remove']";

    private static final String VISIBLE_REMOVE_BUTTON_CONFIRMATION_XPATH = ".//*[@id='remove-addon']/footer/div/button[contains(@aria-hidden, false)]";

    private static final String JIRA_BITBUCKET_CONNECTOR_INSTALLATIONS = "//h3[contains(text(), 'JIRA')]";

    private final String accountName;

    @ElementBy(id = "install-from-url-button", timeoutType = PAGE_LOAD)
    private PageElement installAddonFromUrlButton;

    @ElementBy(id = "custom-descriptor-url")
    private PageElement customDescriptorUrl;

    @ElementBy(className = "dialog-install-button")
    private PageElement installButton;

    @ElementBy(className = "dialog-submit")
    private PageElement approveScopesButton;

    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementFinder elementFinder;

    public BitbucketManageAddonsPage(String accountName) {
        this.accountName = accountName;
    }

    @Override
    public String getUrl() {
        return "/account/user/" + accountName + "/addon-management";
    }

    @WaitUntil
    public void waitUntilLoaded() {
        waitUntilTrue("Manage Addons page failed to load", installAddonFromUrlButton.timed().isPresent());
    }

    public PageElement getInstallAddonFromUrlButton() {
        return installAddonFromUrlButton;
    }

    public PageBinder getPageBinder() {
        return pageBinder;
    }

    public String getAccountName() {
        return accountName;
    }

    public PageElementFinder getElementFinder() {
        return elementFinder;
    }

    public PageElement getCustomDescriptorUrl() {
        return customDescriptorUrl;
    }

    public PageElement getStartInstallButton() {
        return installButton;
    }

    public PageElement getApproveScopesButton() {
        return approveScopesButton;
    }

    public List<PageElement> retrieveAndExpandAllDVCSTwixies() {
        List<PageElement> twixies = elementFinder.findAll(By.xpath(EXPAND_TWIXIES_XPATH));
        for (PageElement twixy : twixies) {
            twixy.click();
        }

        return twixies;
    }

    public List<PageElement> getAllDvcsConnectorUnInstallButtons() {
        return elementFinder.findAll(By.xpath(REMOVE_BUTTON_XPATH));
    }

    public PageElement getVisibleRemoveButtonInDialog() {
        return elementFinder.find(By.xpath(VISIBLE_REMOVE_BUTTON_CONFIRMATION_XPATH));
    }

    public List<PageElement> getAllDvcsConnectorInstallations() {
        return elementFinder.findAll(By.xpath(JIRA_BITBUCKET_CONNECTOR_INSTALLATIONS));
    }

    /**
     * Install a custom connect addon using the provided descriptor URL
     *
     * @param descriptorURL The descriptor URL to enter into the UI
     */
    public void installConnectAddon(final String descriptorURL) {
        waitUntilTrue(installAddonFromUrlButton.timed().isVisible());
        installAddonFromUrlButton.click();

        waitUntilTrue(customDescriptorUrl.timed().isEnabled());
        customDescriptorUrl.type(descriptorURL);

        waitUntilTrue(installButton.timed().isEnabled());
        installButton.click();

        waitUntilTrue(approveScopesButton.timed().isEnabled());
        approveScopesButton.click();
    }
}