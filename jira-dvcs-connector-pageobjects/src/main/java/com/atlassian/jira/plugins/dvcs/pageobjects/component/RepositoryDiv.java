package com.atlassian.jira.plugins.dvcs.pageobjects.component;

import com.atlassian.jira.plugins.dvcs.pageobjects.page.RepositoriesPage;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.apache.commons.lang.math.NumberUtils;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.greaterThan;

/**
 * @deprecated see deprecation note on {@link RepositoriesPage}
 */
@Deprecated
public class RepositoryDiv {
    private final PageElement rootElement;
    @Inject
    protected Timeouts timeouts;

    public RepositoryDiv(final PageElement rootElement) {
        this.rootElement = rootElement;
    }

    public String getMessage() {
        return rootElement.find(By.cssSelector(".dvcs-sync-repo span.gh_messages")).getText();
    }

    public String getRepositoryName() {
        return rootElement.find(By.cssSelector("td.dvcs-org-reponame a")).getAttribute("text");
    }

    public PageElement getSyncIcon() {
        return rootElement.find(By.cssSelector(".dvcs-sync-repo a span"));
    }

    public String getElementId() {
        return rootElement.getAttribute("id");
    }

    public String getRepositoryId() {
        return parseRepositoryId(getElementId());
    }

    public String parseRepositoryId(String elementId) {
        return elementId.substring(elementId.lastIndexOf("-") + 1);
    }

    public TimedElement get() {
        return rootElement.timed();
    }

    public void sync() {
        rootElement.javascript().mouse().mouseover();
        final PageElement syncIcon = getSyncIcon();
        waitUntilTrue(syncIcon.timed().isPresent());
        TimedQuery<Long> lastSync = Queries.forSupplier(timeouts,
                () -> NumberUtils.toLong(syncIcon.getAttribute("data-last-sync")), TimeoutType.SLOW_PAGE_LOAD);
        final long lastSyncBefore = lastSync.now();

        syncIcon.javascript().mouse().click();
        waitUntil(lastSync, greaterThan(lastSyncBefore));
        waitUntilFalse(syncIcon.withTimeout(TimeoutType.SLOW_PAGE_LOAD).timed().hasClass("running"));
    }

}
