package com.atlassian.jira.plugins.dvcs.rest;

import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.sun.jersey.api.client.GenericType;

import javax.annotation.Nonnull;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * A test-time client for the /jira-dev resource.
 */
public class DevToolsClient extends DvcsRestClient<DevToolsClient> {
    public DevToolsClient(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    /**
     * Returns details of the commits stored by DVCS for the given issue.
     *
     * @param issueKey the issue key
     * @return a non-null list with the newest commits first
     */
    @Nonnull
    public List<Changeset> getCommitDetails(@Nonnull final String issueKey) {
        return createResource()
                .path("jira-dev")
                .path("commits")
                .queryParam("issue", issueKey)
                .accept(APPLICATION_JSON_TYPE)
                .get(new GenericType<List<Changeset>>() {
                });
    }
}
