package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request;

import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.BitbucketRemoteClient;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.auth.BasicAuthAuthProvider;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Provides convenient factory methods for creating {@link RemoteRequestor} instances
 */
public class RemoteRequestorFactory {
    private RemoteRequestorFactory() {
    }

    /**
     * Create a default requestor configured with basic auth and a non-browser user agent.
     */
    public static RemoteRequestor createDefaultRequestor(@Nonnull final String hostURL, @Nonnull final String user, @Nonnull final String password) {
        checkArgument(isNotBlank(user));
        checkArgument(isNotBlank(password));

        HttpClientProvider httpClientProvider = new HttpClientProvider();
        httpClientProvider.setUserAgent(BitbucketRemoteClient.TEST_USER_AGENT);
        AuthProvider basicAuthProvider = new BasicAuthAuthProvider(hostURL, user, password, httpClientProvider);

        return basicAuthProvider.provideRequestor();
    }
}
