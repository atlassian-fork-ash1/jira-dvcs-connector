package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.auth;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fusion.aci.api.service.ACIJwtService;
import com.atlassian.fusion.aci.api.service.ACIRegistrationService;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.HttpClientProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.RemoteRequestor;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Auth provider that uses the {@link ACIJwtService} for JWT-based authentication for organizations using ACI
 * principal-based credentials.
 */
public class ACIJwtAuthProvider extends AbstractAuthProvider {
    private final String principalId;
    private final ACIJwtService jwtService;
    private final ACIRegistrationService aciRegistrationService;
    private final EventPublisher eventPublisher;

    public ACIJwtAuthProvider(
            @Nonnull final String hostUrl,
            @Nonnull final String principalId,
            @Nonnull final ACIJwtService jwtService,
            @Nonnull final ACIRegistrationService aciRegistrationService,
            @Nonnull final HttpClientProvider httpClientProvider,
            @Nonnull final EventPublisher eventPublisher) {
        super(checkNotNull(hostUrl), checkNotNull(httpClientProvider));

        checkArgument(isNotBlank(hostUrl));
        checkArgument(isNotBlank(principalId));

        this.principalId = principalId;
        this.jwtService = checkNotNull(jwtService);
        this.aciRegistrationService = checkNotNull(aciRegistrationService);
        this.eventPublisher = checkNotNull(eventPublisher);
    }

    @Override
    public RemoteRequestor provideRequestor() {
        return new ACIJwtAuthRemoteRequestor(
                this, principalId, aciRegistrationService, jwtService, httpClientProvider, eventPublisher);
    }
}
