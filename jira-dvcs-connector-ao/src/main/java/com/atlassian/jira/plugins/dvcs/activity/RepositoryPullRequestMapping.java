package com.atlassian.jira.plugins.dvcs.activity;

import net.java.ao.ManyToMany;
import net.java.ao.OneToMany;
import net.java.ao.Preload;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.Table;

import java.util.Date;

@Preload
@Table("PULL_REQUEST")
public interface RepositoryPullRequestMapping extends RepositoryDomainMapping {
    String REMOTE_ID = "REMOTE_ID";
    String TO_REPO_ID = "TO_REPOSITORY_ID";
    String NAME = "NAME";
    String URL = "URL";

    String AUTHOR = "AUTHOR";
    String SOURCE_REPO = "SOURCE_REPO";
    String SOURCE_BRANCH = "SOURCE_BRANCH";
    String DESTINATION_BRANCH = "DESTINATION_BRANCH";
    String LAST_STATUS = "LAST_STATUS";
    String CREATED_ON = "CREATED_ON";
    String UPDATED_ON = "UPDATED_ON";
    String PARTICIPANTS = "PARTICIPANTS";
    String COMMENT_COUNT = "COMMENT_COUNT";
    String EXECUTED_BY = "EXECUTED_BY";

    /**
     * Unique per repo on Bitbucket and unique globally on GitHub.
     *
     * @return remote Id of this pull request
     */
    @Indexed
    Long getRemoteId();

    void setRemoteId(Long id);

    /**
     * @return local id of destination repository
     */
    @Indexed
    int getToRepositoryId();

    void setToRepositoryId(int repoId);

    String getName();

    void setName(String name);

    String getUrl();

    void setUrl(String url);

    String getSourceBranch();

    void setSourceBranch(String branch);

    String getDestinationBranch();

    void setDestinationBranch(String branch);

    String getLastStatus();

    void setLastStatus(String status);

    Date getCreatedOn();

    void setCreatedOn(Date date);

    Date getUpdatedOn();

    void setUpdatedOn(Date date);

    String getAuthor();

    void setAuthor(String author);

    @ManyToMany(reverse = "getRequest", through = "getCommit", value = RepositoryPullRequestToCommitMapping.class)
    RepositoryCommitMapping[] getCommits();

    String getSourceRepo();

    void setSourceRepo(String sourceRepo);

    @OneToMany(reverse = "getPullRequest")
    PullRequestParticipantMapping[] getParticipants();

    int getCommentCount();

    void setCommentCount(int commentCount);

    /**
     * Pull requests can be declined, merged or reopened by some user that is not the author.
     * In Github/Githube for PR declined and reopened the user will be null because it would be expensive to find out
     * the actor.
     *
     * @return actor who created, merged, declined or reopened the pull request.
     */
    String getExecutedBy();

    void setExecutedBy(String user);
}
