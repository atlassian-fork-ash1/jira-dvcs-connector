package com.atlassian.jira.plugins.dvcs.analytics.event;

import com.atlassian.analytics.api.annotations.EventName;

import java.util.Date;

@EventName("jira.dvcsconnector.sync.end")
public class DvcsSyncEndAnalyticsEvent {
    private final int syncId;
    private final Date finishedOn;
    private final long tookMillis;

    public DvcsSyncEndAnalyticsEvent(final int syncId, final Date finishedOn, long timeInMillis) {
        this.syncId = syncId;
        this.finishedOn = finishedOn;
        this.tookMillis = timeInMillis;
    }

    public int getSyncId() {
        return syncId;
    }

    public Date getFinishedOn() {
        return finishedOn;
    }

    public long getTookMillis() {
        return tookMillis;
    }

}
