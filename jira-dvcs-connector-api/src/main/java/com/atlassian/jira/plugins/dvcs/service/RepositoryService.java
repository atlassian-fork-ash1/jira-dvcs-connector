package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.jira.plugins.dvcs.model.DvcsUser;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Progress;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.RepositoryRegistration;
import com.atlassian.jira.plugins.dvcs.model.SyncProgress;
import com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag;

import javax.annotation.Nonnull;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;

/**
 * Returning type {@link Repository} is enriched with synchronization status by default.
 *
 * @see SyncProgress
 */
public interface RepositoryService {
    /**
     * Returns all repositories for given organization, excluding deleted ones.
     *
     * @param organizationId organizationId
     * @return repositories
     */
    List<Repository> getAllByOrganization(int organizationId);

    /**
     * returns all repositories for given organization
     *
     * @param organizationId organizationId
     * @param includeDeleted whether to include also deleted repositories
     * @return repositories
     */
    List<Repository> getAllByOrganization(int organizationId, boolean includeDeleted);

    /**
     * Find the Repository with the given name in the provided organization, if it exists.
     *
     * @param organizationId The ID of the organization to find the repository in
     * @param repositoryName The name of the repository to find (unique for a given organization)
     * @return The repository, or <code>null</code> if none is found
     */
    Repository getByNameForOrganization(int organizationId, String repositoryName);

    /**
     * Gets the all active (not deleted) repositories and their synchronization status.
     *
     * @return the all active repositories
     */
    List<Repository> getAllRepositories();

    /**
     * Same as {@link #getAllRepositories()}, but provides choice to include also deleted repositories.
     *
     * @return all repositories
     */
    List<Repository> getAllRepositories(boolean includeDeleted);

    /**
     * check if there is at least one linked repository
     *
     * @return true if there is at least one linked repository
     */
    boolean existsLinkedRepositories();

    /**
     * returns repository by ID
     *
     * @param repositoryId repositoryId
     * @return repository
     */
    Repository get(int repositoryId);

    /**
     * save Repository to storage. If it's new object (without ID) after this operation it will have it assigned.
     *
     * @param repository Repository
     * @return Repository
     */
    Repository save(Repository repository);

    /**
     * <p>
     * Synchronization of repository list in given organization Retrieves list of repositories for organization and
     * adds/removes local repositories accordingly. If autolinking is set to to true new repositories will be linked and
     * they will start synchronizing.
     * </p>
     * softsync is used by default
     *
     * @param organization organization
     */
    void syncRepositoryList(Organization organization);

    void syncRepositoryList(Organization organization, boolean soft);

    /**
     * synchronization of changesets in given repository
     *
     * @param repositoryId repositoryId The ID of the repository to sync
     * @param flags        The syncronisation flags to control the sync process
     * @return The progress instance representing the sync, or none if no sync was started.
     */
    Optional<Progress> sync(int repositoryId, EnumSet<SynchronizationFlag> flags);

    /**
     * Enables/links the repository to the jira projects. This will also (un)install postcommit hooks on repository and
     * configure Links on bitbucket repositories
     *
     * @param repoId the repo id
     * @param linked the parse boolean
     */
    RepositoryRegistration enableRepository(int repoId, boolean linked);

    /**
     * Enable repository smartcommits.
     *
     * @param repoId  the repo id
     * @param enabled the enabled
     */
    void enableRepositorySmartcommits(int repoId, boolean enabled);

    /**
     * Remove remote webhooks associated with the provided repository.
     * <p>
     * Unless the provided <code>ignoreCredentialType</code> param is true, will only perform
     * removal for non-principalId based repositories.
     *
     * @param repository           the repository to remove the post commit hook for
     * @param ignoreCredentialType whether to skip the credential-type check and force removal
     * @return whether the removal was successful
     */
    boolean removePostcommitHook(@Nonnull Repository repository, boolean ignoreCredentialType);

    /**
     * Remove all post commit hooks for repositories in the provided organization.
     * <p>
     * This method will remove all hooks synchronously on the current thread. This may be a long operation as it
     * likely involves remote web service calls. Use {@link #removePostcommitHooksAsync(Organization, boolean)} to
     * remove hooks asynchronously.
     * <p>
     * Unless the provided <code>ignoreCredentialType</code> param is true, will only perform
     * removal for non-principalId based organizations.
     *
     * @param organization         The organization to remove post commit hooks for
     * @param ignoreCredentialType whether to skip the credential-type check and force removal
     * @return The number of hooks successfully removed
     */
    int removePostcommitHooks(@Nonnull Organization organization, boolean ignoreCredentialType);

    /**
     * Remove all post commit hooks for repositories in the provided organization.
     * <p>
     * This method will remove all hooks asynchronously. See
     * {@link RepositoryService#removePostcommitHooks(Organization, boolean)} for a synchronous version.
     * <p>
     * Unless the provided <code>ignoreCredentialType</code> param is true, will only perform
     * removal for non-principalId based repositories.
     *
     * @param organization         The organization to remove post commit hooks for
     * @param ignoreCredentialType whether to skip the credential-type check and force removal
     * @return The future representing the removal task, which yields the number of
     * hooks successfully removed when complete.
     * @throws RejectedExecutionException if the task could not be scheduled
     */
    @Nonnull
    Future<Integer> removePostcommitHooksAsync(@Nonnull Organization organization, boolean ignoreCredentialType)
            throws RejectedExecutionException;

    /**
     * remove all the listed repositories
     *
     * @param repositories list of repositories to delete
     */
    void removeRepositories(List<Repository> repositories);

    /**
     * @param repository
     */
    void remove(Repository repository, boolean shouldRemoveLinkersAndCommitHooks);

    /**
     * @param repository
     */
    void remove(Repository repository);

    /**
     * Removes orphan repositories (asynchronously).
     *
     * @param orphanRepositories the repositories to remove
     */
    void removeOrphanRepositories(List<Repository> orphanRepositories);

    /**
     * Turn On or off linkers.
     *
     * @param onOffBoolean the on off boolean
     */
    void onOffLinkers(boolean onOffBoolean);

    DvcsUser getUser(Repository repository, String author, String raw_author);

    List<Repository> getAllRepositories(String dvcsType, boolean includeDeleted);

    void setPreviouslyLinkedProjects(Repository repository, Set<String> projects);

    List<String> getPreviouslyLinkedProjects(Repository repository);

    Set<String> getEmails(Repository repository, DvcsUser user);

    /**
     * Disables all repositories.
     *
     * @return the number of repositories disabled
     */
    int disableAllRepositories();
}
