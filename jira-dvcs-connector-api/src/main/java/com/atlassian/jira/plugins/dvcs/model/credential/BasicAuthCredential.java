package com.atlassian.jira.plugins.dvcs.model.credential;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * A username/password credential for basic auth
 */
public class BasicAuthCredential implements Credential {
    private final String username;
    private final String password;

    public BasicAuthCredential(final String username, final String password) {
        checkArgument(isNotBlank(username));
        checkArgument(isNotBlank(password));

        this.username = username;
        this.password = password;
    }

    public static CredentialVisitor<Optional<BasicAuthCredential>> visitor() {
        return new BasicAuthCredentialVisitor();
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    @Nonnull
    public CredentialType getType() {
        return CredentialType.BASIC;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BasicAuthCredential that = (BasicAuthCredential) o;
        return Objects.equals(username, that.username) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }

    @Override
    public <T> T accept(CredentialVisitor<T> visitor) {
        return visitor.visit(this);
    }

    private static class BasicAuthCredentialVisitor
            extends AbstractOptionalCredentialVisitor<BasicAuthCredential> {
        @Override
        public Optional<BasicAuthCredential> visit(final BasicAuthCredential credential) {
            return Optional.ofNullable(credential);
        }
    }
}
