package com.atlassian.jira.plugins.dvcs.model.credential;

/**
 * Supported credential types
 */
public enum CredentialType {
    /**
     * Principal ID credential used to lookup stored credential information for
     * generation of JWT tokens etc.
     */
    PRINCIPAL_ID,

    /**
     * OAuth 2LO credentials consisting of a key and secret
     */
    TWO_LEGGED_OAUTH,

    /**
     * OAuth 3LO credentials consisting of a key, secret and access token
     */
    THREE_LEGGED_OAUTH,

    /**
     * Basic username/password credentials
     */
    BASIC,

    /**
     * Anonymous unauthenticated credentials
     */
    UNAUTHENTICATED,

}
